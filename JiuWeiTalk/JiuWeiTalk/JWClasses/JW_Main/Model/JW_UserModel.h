//
//  JW_UserModel.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JW_UserModel : NSObject

@property (nonatomic, copy) NSString *userId; //用户id
@property (nonatomic, copy) NSString *userName; //昵称
@property (nonatomic, copy) NSString *mobile; //手机号
@property (nonatomic, copy) NSString *imgUrl; //头像
@property (nonatomic, copy) NSString *unionId;
@property (nonatomic, assign) BOOL redPoint;


+ (instancetype)sharedInstance;
+ (BOOL)isLogin;
+ (void)setupSharedInstance:(JW_UserModel *)model;
+ (void)saveDataToCache;
+ (void)cleanSharedInstance;
+ (void)cleanCache;


@end

NS_ASSUME_NONNULL_END
