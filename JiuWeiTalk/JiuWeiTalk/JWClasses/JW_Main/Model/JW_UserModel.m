//
//  JW_UserModel.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "JW_UserModel.h"


@implementation JW_UserModel

static dispatch_once_t onceToken1;

+ (instancetype)sharedInstance {
    static JW_UserModel *model;
    dispatch_once(&onceToken1, ^{
        model = [JW_UserModel new];
    });
    return model;
}
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self modelEncodeWithCoder:aCoder];
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    return [self modelInitWithCoder:aDecoder];
}
- (id)copyWithZone:(NSZone *)zone {
    return [self modelCopy];
}

+ (void)setupSharedInstance:(JW_UserModel *)model {
    [JW_UserModel sharedInstance].userId   = model.userId;
    [JW_UserModel sharedInstance].userName = model.userName;
    [JW_UserModel sharedInstance].mobile   = model.mobile;
    [JW_UserModel sharedInstance].imgUrl   = model.imgUrl;
    [JW_UserModel sharedInstance].unionId  = model.unionId;
    [JW_UserModel sharedInstance].redPoint = model.redPoint;
    
}
+ (void)saveDataToCache {
    JW_UserModel *model = [JW_UserModel sharedInstance].deepCopy;
    YYCache *cache = [YYCache cacheWithName:Cache];
    [cache setObject:(id)model forKey:Cache_JW_UserModel];
}
+ (void)cleanSharedInstance {
    onceToken1 = 0;
}
+ (void)cleanCache {
    YYCache *cache = [YYCache cacheWithName:Cache];
    [cache removeObjectForKey:Cache_JW_UserModel];
}
+ (BOOL)isLogin {
    BOOL status = NO;
    if ([JW_UserModel sharedInstance].userId) {
        status = YES;
    }
    return status;
}
- (void)dealloc {
    NSLog(@"%@_________dealloc",self);
}
@end
