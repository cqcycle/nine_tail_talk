//
//  BaseNavController.m
//  squirrelTravel
//
//  Created by Itanotomomi on 17/2/20.
//  Copyright © 2017年 chance. All rights reserved.
//

#import "BaseNavController.h"

@interface BaseNavController ()<UIGestureRecognizerDelegate, UINavigationControllerDelegate>

@property(nonatomic,weak) UIViewController *currentShowVC;

@end

@implementation BaseNavController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBar.tintColor = [UIColor blackColor];
//    self.view.backgroundColor = [UIColor clearColor];
    [self.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationBar setShadowImage:[[UIImage alloc]init]];
    
    NSDictionary *textAttributes = nil;
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        textAttributes = @{
                           NSFontAttributeName : [UIFont boldSystemFontOfSize:18],
                           NSForegroundColorAttributeName : [UIColor colorWithHexString:@"262626"],
                           };
    } else {
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
        textAttributes = @{
                           UITextAttributeTextColor : [UIColor colorWithHexString:@"262626"],
                           UITextAttributeTextShadowColor : [UIColor clearColor],
                           UITextAttributeTextShadowOffset : [NSValue valueWithUIOffset:UIOffsetZero],
                           };
#endif
    }
    
    [self.navigationBar setTitleTextAttributes:textAttributes];
    
    if (@available(iOS 13.0, *)) {
        
        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
        
    } else {
        // Fallback on earlier versions
    }
    
    
//    [self setUpNavigationBarAppearance];
}

- (void)back {

    [self popViewControllerAnimated:YES];
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    if (self.viewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
    
    if (self.viewControllers.count != 1) {
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_btn_back"] style:UIBarButtonItemStyleDone target:self action:@selector(back)];
    }else{
        viewController.navigationItem.leftBarButtonItem = nil;
    }
    
}

-(id)initWithRootViewController:(UIViewController *)rootViewController {
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        self.interactivePopGestureRecognizer.delegate = self;
        self.delegate = self;
    }
    return self;
}
-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
}
-(void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (navigationController.viewControllers.count == 1)
        self.currentShowVC = Nil;
    else
        self.currentShowVC = viewController;
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer == self.interactivePopGestureRecognizer) {
        return (self.currentShowVC == self.topViewController);
    }
    return YES;
}

//返回根控制器，必须显示底部的tabBar
- (NSArray *)popToRootViewControllerAnimated:(BOOL)animated {
    
    //由于是根控制器，必须显示tabBar
    [self rootViewController].hidesBottomBarWhenPushed = NO;
    NSArray *array = [super popToRootViewControllerAnimated:YES];
    return array;
}

- (UIViewController *) rootViewController
{
    return ((UIViewController *)self.viewControllers.firstObject);
}

//- (void)setUpNavigationBarAppearance {
//    UINavigationBar *navigationBar = [UINavigationBar appearance];
//    navigationBar.barTintColor = [UIColor whiteColor];
//    NSDictionary *textAttributes = nil;
//    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
//        textAttributes = @{
//                           NSFontAttributeName : [UIFont systemFontOfSize:18],
//                           NSForegroundColorAttributeName : [UIColor whiteColor],
//                           };
//    } else {
//#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
//        textAttributes = @{
//                           UITextAttributeTextColor : [UIColor whiteColor],
//                           UITextAttributeTextShadowColor : [UIColor clearColor],
//                           UITextAttributeTextShadowOffset : [NSValue valueWithUIOffset:UIOffsetZero],
//                           };
//#endif
//    }
//    [navigationBar setTitleTextAttributes:textAttributes];
//    UIImage *backButtonImage = [[UIImage imageNamed:@"nav_back_white"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    [navigationBar setBackIndicatorImage:backButtonImage];
//    [navigationBar setBackIndicatorTransitionMaskImage:backButtonImage];
//    //将返回按钮的文字position设置不在屏幕上显示
//    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment: UIOffsetMake(-2*SCREEN_WIDTH, 0) forBarMetrics:UIBarMetricsDefault];
//    //用系统的返回按钮的时候 会发现显示的是蓝色的，想要改变成白色的添加下面一句话
//    [navigationBar setTintColor:[UIColor blackColor]];
//    // 透明导航栏
//    [navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forBarMetrics:UIBarMetricsDefault];
//    [navigationBar setShadowImage:[[UIImage alloc]init]];
//}

@end
