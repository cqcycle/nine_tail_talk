//
//  CYLTabBarControllerConfig.m
//  CYLTabBarController
//
//  v1.10.0 Created by 微博@iOS程序犭袁 ( http://weibo.com/luohanchenyilong/ ) on 10/20/15.
//  Copyright © 2015 https://github.com/ChenYilong . All rights reserved.
//
#import "CYLTabBarControllerConfig.h"

@interface CYLBaseNavigationController : UINavigationController
@end

@implementation CYLBaseNavigationController

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (self.viewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
}

@end

#import <UIImage+GIF.h>
//View Controllers
#import "JW_HomeViewController.h"
#import "JW_MineViewController.h"

//#import "NT_SchoolViewController.h"

@interface CYLTabBarControllerConfig ()

//@property (nonatomic, readwrite, strong) CYLTabBarController *tabBarController;

@end

@implementation CYLTabBarControllerConfig

/**
 *  lazy load tabBarController
 *
 *  @return CYLTabBarController
 */
- (CYLTabBarController *)tabBarController {
    if (_tabBarController == nil) {
        CYLTabBarController *tabBarController = [CYLTabBarController tabBarControllerWithViewControllers:self.viewControllers
                                                                                   tabBarItemsAttributes:self.tabBarItemsAttributesForController];
        [self customizeTabBarAppearance:tabBarController];
        _tabBarController = tabBarController;
    }
    return _tabBarController;
}

- (NSArray *)viewControllers {

    
    JW_HomeViewController *homeViewController = [[JW_HomeViewController alloc] init];
    UIViewController *homeNavigationController = [[BaseNavController alloc]
                                                     initWithRootViewController:homeViewController];
    
   
    
    JW_MineViewController *mineRootViewController = [[JW_MineViewController alloc] init];
    UIViewController *mineRootNavigationController = [[BaseNavController alloc]
                                                     initWithRootViewController:mineRootViewController];

    NSArray *viewControllers = @[
                                 homeNavigationController,
                                 mineRootNavigationController
                                 ];
    return viewControllers;
}

- (NSArray *)tabBarItemsAttributesForController {
    NSDictionary *firstTabBarItemsAttributes = @{
                                                 CYLTabBarItemTitle : @"首页",
                                                 CYLTabBarItemImage : @"tab_home",  /* NSString and UIImage are supported*/
                                                 CYLTabBarItemSelectedImage : @"tab_home_pre", /* NSString and UIImage are supported*/
                                                 };
//    NSDictionary *secondTabBarItemsAttributes = @{
//                                                  CYLTabBarItemTitle : @"案例",
//                                                  CYLTabBarItemImage : @"tab_anli",
//                                                  CYLTabBarItemSelectedImage : @"tab_anli_pre",
//                                                  };
//
//    NSDictionary *thirdTabBarItemsAttributes = @{
//                                                  CYLTabBarItemTitle : @"发现",
//                                                  CYLTabBarItemImage : @"tab_faxian",
//                                                  CYLTabBarItemSelectedImage : @"tab_faxian_pre",
//                                                  };
    
    
    NSDictionary *fourthTabBarItemsAttributes = @{
                                                  CYLTabBarItemTitle : @"我的",
                                                  CYLTabBarItemImage : @"tab_my",
                                                  CYLTabBarItemSelectedImage : @"tab_my_pre"
                                                  };
    
//    kUserDef
//    NSInteger runMode = [ud integerForKey:@"runMode"];
//    if (runMode == 0) {
        NSArray *tabBarItemsAttributes = @[
                                           firstTabBarItemsAttributes,
//                                           secondTabBarItemsAttributes,
//                                           thirdTabBarItemsAttributes,
                                           fourthTabBarItemsAttributes
                                           ];
        return tabBarItemsAttributes;
//    }else{
//        NSArray *tabBarItemsAttributes = @[
//                                           first,
//                                           second,
//                                           third
//                                           ];
//        return tabBarItemsAttributes;
//    }
}

/**
 *  更多TabBar自定义设置：比如：tabBarItem 的选中和不选中文字和背景图片属性、tabbar 背景图片属性等等
 */
- (void)customizeTabBarAppearance:(CYLTabBarController *)tabBarController {
//#warning CUSTOMIZE YOUR TABBAR APPEARANCE
    // Customize UITabBar height
    // 自定义 TabBar 高度
//     tabBarController.tabBarHeight = 40.f;
    
    // set the text color for unselected state
    // 普通状态下的文字属性
    NSMutableDictionary *normalAttrs = [NSMutableDictionary dictionary];
    normalAttrs[NSForegroundColorAttributeName] = [UIColor colorWithHexString:@"#444444"];
    
    // set the text color for selected state
    // 选中状态下的文字属性
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSForegroundColorAttributeName] = [UIColor colorWithHexString:@"#FF8824"];
    
    // set the text Attributes
    // 设置文字属性
    UITabBarItem *tabBarItem = [UITabBarItem appearance];
    
    
    if (@available(iOS 13.0, *)){
        UITabBarAppearance *appearance = [UITabBarAppearance new];
        // 设置未被选中的颜色
        appearance.stackedLayoutAppearance.normal.titleTextAttributes = normalAttrs;
        appearance.stackedLayoutAppearance.normal.titlePositionAdjustment = UIOffsetMake(0, -2);
        // 设置被选中时的颜色
        appearance.stackedLayoutAppearance.selected.titleTextAttributes = selectedAttrs;
        appearance.stackedLayoutAppearance.selected.titlePositionAdjustment = UIOffsetMake(0, -2);
        
        appearance.backgroundColor = [UIColor whiteColor];
        appearance.backgroundImage = [UIImage new];
        appearance.shadowImage = [UIImage new];
        tabBarController.tabBar.standardAppearance = appearance;
    }else{
        [tabBarItem setTitleTextAttributes:normalAttrs forState:UIControlStateNormal];
        [tabBarItem setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
        tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -2);
    }
    
    // Set the dark color to selected tab (the dimmed background)
    // TabBarItem选中后的背景颜色
    // [self customizeTabBarSelectionIndicatorImage];
    
    // update TabBar when TabBarItem width did update
    // If your app need support UIDeviceOrientationLandscapeLeft or UIDeviceOrientationLandscapeRight，
    // remove the comment '//'
    // 如果你的App需要支持横竖屏，请使用该方法移除注释 '//'
    // [self updateTabBarCustomizationWhenTabBarItemWidthDidUpdate];
    
    // set the bar shadow image
    // This shadow image attribute is ignored if the tab bar does not also have a custom background image.So at least set somthing.
    [[UITabBar appearance] setBackgroundImage:[[UIImage alloc] init]];
    [[UITabBar appearance] setBackgroundColor:[UIColor whiteColor]];
//    [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
//    [[UITabBar appearance] setShadowImage:[UIImage imageNamed:@"yinying"]];
    
    // set the bar background image
    // 设置背景图片
    // UITabBar *tabBarAppearance = [UITabBar appearance];
    // [tabBarAppearance setBackgroundImage:[UIImage imageNamed:@"tabbar_background"]];
    
    // remove the bar system shadow image
    // 去除 TabBar 自带的顶部阴影
    [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
    [tabBarController.tabBar jw_setLayerShadow:KJWHexRGBA(0x000000, 0.5) offset:CGSizeMake(0, 2) radius:4];
    
    // 解决 iOS12.1 tab 图标跳动
    if (@available(iOS 12.1, *)){
        [UITabBar appearance].translucent = NO;
    }
}

- (void)updateTabBarCustomizationWhenTabBarItemWidthDidUpdate {
    void (^deviceOrientationDidChangeBlock)(NSNotification *) = ^(NSNotification *notification) {
        UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
        if ((orientation == UIDeviceOrientationLandscapeLeft) || (orientation == UIDeviceOrientationLandscapeRight)) {
            NSLog(@"Landscape Left or Right !");
        } else if (orientation == UIDeviceOrientationPortrait) {
            NSLog(@"Landscape portrait!");
        }
        [self customizeTabBarSelectionIndicatorImage];
    };
    [[NSNotificationCenter defaultCenter] addObserverForName:CYLTabBarItemWidthDidChangeNotification
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:deviceOrientationDidChangeBlock];
}

- (void)customizeTabBarSelectionIndicatorImage {
    ///Get initialized TabBar Height if exists, otherwise get Default TabBar Height.
    UITabBarController *tabBarController = [self cyl_tabBarController] ?: [[UITabBarController alloc] init];
    CGFloat tabBarHeight = tabBarController.tabBar.frame.size.height;
    CGSize selectionIndicatorImageSize = CGSizeMake(CYLTabBarItemWidth, tabBarHeight);
    //Get initialized TabBar if exists.
    UITabBar *tabBar = [self cyl_tabBarController].tabBar ?: [UITabBar appearance];
    [tabBar setSelectionIndicatorImage:
     [[self class] imageWithColor:[UIColor redColor]
                             size:selectionIndicatorImageSize]];
}

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
    if (!color || size.width <= 0 || size.height <= 0) return nil;
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width + 1, size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
