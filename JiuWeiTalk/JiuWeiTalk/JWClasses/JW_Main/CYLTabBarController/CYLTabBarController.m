//
//  CYLTabBarController.m
//  CYLTabBarController
//
//  v1.10.0 Created by 微博@iOS程序犭袁 ( http://weibo.com/luohanchenyilong/ ) on 10/20/15.
//  Copyright © 2015 https://github.com/ChenYilong . All rights reserved.
//

#import "CYLTabBarController.h"
#import "CYLTabBar.h"
#import <objc/runtime.h>
#import <UIButton+WebCache.h>

NSString *const CYLTabBarItemTitle = @"CYLTabBarItemTitle";
NSString *const CYLTabBarItemImage = @"CYLTabBarItemImage";
NSString *const CYLTabBarItemSelectedImage = @"CYLTabBarItemSelectedImage";
NSString *const CYLTabBarItemImageUrl = @"CYLTabBarItemImageUrl";
NSString *const CYLTabBarItemSelectedImageUrl = @"CYLTabBarItemSelectedImageUrl";

NSUInteger CYLTabbarItemsCount = 0;
NSUInteger CYLPlusButtonIndex = 0;
CGFloat CYLTabBarItemWidth = 0.0f;
NSString *const CYLTabBarItemWidthDidChangeNotification = @"CYLTabBarItemWidthDidChangeNotification";
static void * const CYLSwappableImageViewDefaultOffsetContext = (void*)&CYLSwappableImageViewDefaultOffsetContext;

@interface NSObject (CYLTabBarControllerItemInternal)

- (void)cyl_setTabBarController:(CYLTabBarController *)tabBarController;

@end

@interface CYLTabBarController () <UITabBarControllerDelegate>

@property (nonatomic, assign, getter=isObservingSwappableImageViewDefaultOffset) BOOL observingSwappableImageViewDefaultOffset;

@end
@implementation CYLTabBarController

@synthesize viewControllers = _viewControllers;

#pragma mark -
#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // 处理tabBar，使用自定义 tabBar 添加 发布按钮
    [self setUpTabBar];
    // KVO注册监听
    if (!self.isObservingSwappableImageViewDefaultOffset) {
        [self.tabBar addObserver:self forKeyPath:@"swappableImageViewDefaultOffset" options:NSKeyValueObservingOptionNew context:CYLSwappableImageViewDefaultOffsetContext];
        self.observingSwappableImageViewDefaultOffset = YES;
    }
    self.delegate = self;
}

//Fix issue #93
- (void)viewDidLayoutSubviews {
    [self.tabBar layoutSubviews];
}
/**
 * get current topController
 */
- (BaseNavController *)jw_getCurrentViewController {
    return (BaseNavController *)[self.viewControllers objectAtIndex:self.selectedIndex];
}
- (void)viewWillLayoutSubviews {
    if (!self.tabBarHeight) {
        return;
    }
    self.tabBar.frame = ({
        CGRect frame = self.tabBar.frame;
        CGFloat tabBarHeight = self.tabBarHeight;
        frame.size.height = tabBarHeight;
        frame.origin.y = self.view.frame.size.height - tabBarHeight;
        frame;
    });
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    UIViewController *controller = self.selectedViewController;
    if ([controller isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)controller;
        return navigationController.topViewController.supportedInterfaceOrientations;
    } else {
        return controller.supportedInterfaceOrientations;
    }
}

- (void)dealloc {
    // KVO反注册
    if (self.isObservingSwappableImageViewDefaultOffset) {
//        [self.tabBar removeObserver:self forKeyPath:@"swappableImageViewDefaultOffset"];
        [self.tabBar removeAllSubviews];
    }
}

#pragma mark -
#pragma mark - public Methods

- (instancetype)initWithViewControllers:(NSArray<UIViewController *> *)viewControllers tabBarItemsAttributes:(NSArray<NSDictionary *> *)tabBarItemsAttributes {
    if (self = [super init]) {
        _tabBarItemsAttributes = tabBarItemsAttributes;
        self.viewControllers = viewControllers;
    }
    return self;
}

+ (instancetype)tabBarControllerWithViewControllers:(NSArray<UIViewController *> *)viewControllers tabBarItemsAttributes:(NSArray<NSDictionary *> *)tabBarItemsAttributes {
   return [[self alloc] initWithViewControllers:viewControllers tabBarItemsAttributes:tabBarItemsAttributes];
}

+ (BOOL)havePlusButton {
    if (CYLExternPlusButton) {
        return YES;
    }
    return NO;
}

+ (NSUInteger)allItemsInTabBarCount {
    NSUInteger allItemsInTabBar = CYLTabbarItemsCount;
    if ([CYLTabBarController havePlusButton]) {
        allItemsInTabBar += 1;
    }
    return allItemsInTabBar;
}

- (id<UIApplicationDelegate>)appDelegate {
    return [UIApplication sharedApplication].delegate;
}

- (UIWindow *)rootWindow {
    UIWindow *result = nil;
    
    do {
        if ([self.appDelegate respondsToSelector:@selector(window)]) {
            result = [self.appDelegate window];
        }
        
        if (result) {
            break;
        }
    } while (NO);
    
    return result;
}

#pragma mark -
#pragma mark - Private Methods

/**
 *  利用 KVC 把系统的 tabBar 类型改为自定义类型。
 */
- (void)setUpTabBar {
    [self setValue:[[CYLTabBar alloc] init] forKey:@"tabBar"];
}

- (void)setViewControllers:(NSArray *)viewControllers {
    if (_viewControllers && _viewControllers.count) {
        for (UIViewController *viewController in _viewControllers) {
            [viewController willMoveToParentViewController:nil];
            [viewController.view removeFromSuperview];
            [viewController removeFromParentViewController];
        }
    }
    if (viewControllers && [viewControllers isKindOfClass:[NSArray class]]) {
        if ((!_tabBarItemsAttributes) || (_tabBarItemsAttributes.count != viewControllers.count)) {
            [NSException raise:@"CYLTabBarController" format:@"The count of CYLTabBarControllers is not equal to the count of tabBarItemsAttributes.【Chinese】设置_tabBarItemsAttributes属性时，请确保元素个数与控制器的个数相同，并在方法`-setViewControllers:`之前设置"];
        }
        
        if (CYLPlusChildViewController) {
            NSMutableArray *viewControllersWithPlusButton = [NSMutableArray arrayWithArray:viewControllers];
            [viewControllersWithPlusButton insertObject:CYLPlusChildViewController atIndex:CYLPlusButtonIndex];
            _viewControllers = [viewControllersWithPlusButton copy];
        } else {
            _viewControllers = [viewControllers copy];
        }
        CYLTabbarItemsCount = [viewControllers count];
        CYLTabBarItemWidth = ([UIScreen mainScreen].bounds.size.width - CYLPlusButtonWidth) / (CYLTabbarItemsCount);
        NSUInteger idx = 0;
        for (UIViewController *viewController in _viewControllers) {
            NSString *title = nil;
            id normalImageInfo = nil;
            id selectedImageInfo = nil;
            id normalImageInfoUrl = nil;
            id selectedImageInfoUrl = nil;
            if (viewController != CYLPlusChildViewController) {
                title = _tabBarItemsAttributes[idx][CYLTabBarItemTitle];
                normalImageInfo = _tabBarItemsAttributes[idx][CYLTabBarItemImage];
                selectedImageInfo = _tabBarItemsAttributes[idx][CYLTabBarItemSelectedImage];
                normalImageInfoUrl = _tabBarItemsAttributes[idx][CYLTabBarItemImageUrl];
                selectedImageInfoUrl = _tabBarItemsAttributes[idx][CYLTabBarItemSelectedImageUrl];
            } else {
                idx--;
            }
            
            [self addOneChildViewController:viewController
                                  WithTitle:title
                            normalImageInfo:normalImageInfo
                          selectedImageInfo:selectedImageInfo
                         normalImageInfoUrl:normalImageInfoUrl
                       selectedImageInfoUrl:selectedImageInfoUrl];
            [viewController cyl_setTabBarController:self];
            idx++;
        }
    } else {
        for (UIViewController *viewController in _viewControllers) {
            [viewController cyl_setTabBarController:nil];
        }
        _viewControllers = nil;
    }
}

/**
 *  添加一个子控制器
 *
 *  @param viewController    控制器
 *  @param title             标题
 *  @param normalImageInfo   图片
 *  @param selectedImageInfo 选中图片
 */
- (void)addOneChildViewController:(UIViewController *)viewController
                        WithTitle:(NSString *)title
                  normalImageInfo:(id)normalImageInfo
                selectedImageInfo:(id)selectedImageInfo
               normalImageInfoUrl:(id)normalImageInfoUrl
             selectedImageInfoUrl:(id)selectedImageInfoUrl {
    viewController.tabBarItem.title = title;
    if (normalImageInfo) {
        UIImage *normalImage = [self getImageFromImageInfo:normalImageInfo];
        normalImage = [normalImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        viewController.tabBarItem.image = normalImage;
    }
    if (selectedImageInfo) {
        UIImage *selectedImage = [self getImageFromImageInfo:selectedImageInfo];
        selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        viewController.tabBarItem.selectedImage = selectedImage;
    }
    
    if (normalImageInfoUrl) {
        UIImage *normalImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:normalImageInfoUrl]]];
        normalImage = [normalImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        viewController.tabBarItem.image = normalImage;
    }
    if (selectedImageInfoUrl) {
        UIImage *selectedImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:selectedImageInfoUrl]]];
        selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        viewController.tabBarItem.selectedImage = selectedImage;
    }

    
    if (self.shouldCustomizeImageInsets) {
        viewController.tabBarItem.imageInsets = self.imageInsets;
    }
    if (self.shouldCustomizeTitlePositionAdjustment) {
        viewController.tabBarItem.titlePositionAdjustment = self.titlePositionAdjustment;
    }
    [self addChildViewController:viewController];
}

- (UIImage *)getImageFromImageInfo:(id)imageInfo {
    UIImage *image = nil;
    if ([imageInfo isKindOfClass:[NSString class]]) {
        image = [UIImage imageNamed:imageInfo];
    } else if ([imageInfo isKindOfClass:[UIImage class]]) {
        image = (UIImage *)imageInfo;
    }
    return image;
}

- (BOOL)shouldCustomizeImageInsets {
    BOOL shouldCustomizeImageInsets = self.imageInsets.top != 0.f || self.imageInsets.left != 0.f || self.imageInsets.bottom != 0.f || self.imageInsets.right != 0.f;
    return shouldCustomizeImageInsets;
}

- (BOOL)shouldCustomizeTitlePositionAdjustment {
    BOOL shouldCustomizeTitlePositionAdjustment = self.titlePositionAdjustment.horizontal != 0.f || self.titlePositionAdjustment.vertical != 0.f;
    return shouldCustomizeTitlePositionAdjustment;
}

#pragma mark -
#pragma mark - KVO Method

// KVO监听执行
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if(context != CYLSwappableImageViewDefaultOffsetContext) {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        return;
    }
    if(context == CYLSwappableImageViewDefaultOffsetContext) {
        CGFloat swappableImageViewDefaultOffset = [change[NSKeyValueChangeNewKey] floatValue];
        [self offsetTabBarSwappableImageViewToFit:swappableImageViewDefaultOffset];
    }
}

- (void)offsetTabBarSwappableImageViewToFit:(CGFloat)swappableImageViewDefaultOffset {
    if (self.shouldCustomizeImageInsets) {
        return;
    }
    NSArray<UITabBarItem *> *tabBarItems = [self cyl_tabBarController].tabBar.items;
    [tabBarItems enumerateObjectsUsingBlock:^(UITabBarItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIEdgeInsets imageInset = UIEdgeInsetsMake(swappableImageViewDefaultOffset, 0, -swappableImageViewDefaultOffset, 0);
        obj.imageInsets = imageInset;
        if (!self.shouldCustomizeTitlePositionAdjustment) {
            obj.titlePositionAdjustment = UIOffsetMake(0, MAXFLOAT);
        }
    }];
}

#pragma mark - delegate

- (void)updateSelectionStatusIfNeededForTabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    NSUInteger selectedIndex = tabBarController.selectedIndex;
    UIButton *plusButton = CYLExternPlusButton;
    BOOL shouldConfigureSelectionStatus = CYLPlusChildViewController && ((selectedIndex == CYLPlusButtonIndex) && (viewController != CYLPlusChildViewController));
    if (shouldConfigureSelectionStatus) {
        plusButton.selected = NO;
    }
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    UIViewController *vc = ((UINavigationController *)viewController).visibleViewController;
    NSInteger selectedIndex = tabBarController.selectedIndex;
    NSArray *arr = [self.tabBar valueForKeyPath:@"tabBarButtonArray"];
    if (arr.count < 4) {
        return;
    }
    UIView *tab1 = arr[0];
    UIView *tab2 = arr[1];
    UIView *tab3 = arr[2];
    UIView *tab4 = arr[3];
    SEL setImage = NSSelectorFromString(@"_setImage:selected:offset:");
    
    if ([tab1 respondsToSelector:setImage]) {
        UIImage *image = [UIImage animatedImageNamed:@"icon_home_" duration:0.3];
        [tab1 performSelectorWithArgs:setImage, image, @(YES), CGSizeZero];
    }
    if ([tab2 respondsToSelector:setImage]) {
        UIImage *image = [UIImage animatedImageNamed:@"tab_anli_icon_" duration:0.3];
        [tab2 performSelectorWithArgs:setImage, image, @(YES), CGSizeZero];
    }
    if ([tab3 respondsToSelector:setImage]) {
        UIImage *image = [UIImage animatedImageNamed:@"icon_faxian_" duration:0.3];
        [tab3 performSelectorWithArgs:setImage, image, @(YES), CGSizeZero];
    }
    if ([tab4 respondsToSelector:setImage]) {
        UIImage *image = [UIImage animatedImageNamed:@"icon_wode_" duration:0.3];
        [tab4 performSelectorWithArgs:setImage, image, @(YES), CGSizeZero];
    }
    
    if ([vc.className isEqualToString:@"NT_HomeViewController"]) {
        UIImage *image = [[UIImage imageNamed:@"icon_home_10"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        if ([tab1 respondsToSelector:setImage]) {
            [tab1 performSelectorWithArgs:setImage afterDelay:0.2, image, @(YES), CGSizeZero];
        }
        [DYCommonTool appUserTrackDataPage:@"029" event:@"029001"];
    }else if ([vc.className isEqualToString:@"NT_FoundViewController"]){
        if (selectedIndex == 1) {
            UIImage *image = [[UIImage imageNamed:@"tab_anli_icon_10"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            if ([tab2 respondsToSelector:setImage]) {
                [tab2 performSelectorWithArgs:setImage afterDelay:0.2, image, @(YES), CGSizeZero];
            }
        }else {
            UIImage *image = [[UIImage imageNamed:@"icon_faxian_10"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            if ([tab3 respondsToSelector:setImage]) {
                [tab3 performSelectorWithArgs:setImage afterDelay:0.2, image, @(YES), CGSizeZero];
            }
            [DYCommonTool appUserTrackDataPage:@"029" event:@"029002"];
        }
        
    }else if ([vc.className isEqualToString:@"NT_MeViewController"]){
        UIImage *image = [[UIImage imageNamed:@"icon_wode_10"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        if ([tab4 respondsToSelector:setImage]) {
            [tab4 performSelectorWithArgs:setImage afterDelay:0.2, image, @(YES), CGSizeZero];
        }
        [DYCommonTool appUserTrackDataPage:@"029" event:@"029003"];
    }
    
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    if ([((UINavigationController *)viewController).visibleViewController.className isEqualToString:@"NT_MeViewController"]) {
        
        if (![JW_UserModel sharedInstance].userId.length) {
            [DYCommonTool alertLoginVC];
            return NO;
        }
    }
    
    [[self cyl_tabBarController] updateSelectionStatusIfNeededForTabBarController:tabBarController shouldSelectViewController:viewController];
    return YES;
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex{
    [super setSelectedIndex:selectedIndex];
    NSArray *arr = [self.tabBar valueForKeyPath:@"tabBarButtonArray"];
    if (arr.count < 3) {
        return;
    }
    UIView *tab1 = arr[0];
    UIView *tab2 = arr[1];
    UIView *tab3 = arr[2];
    SEL setImage = NSSelectorFromString(@"_setImage:selected:offset:");
    
    if ([tab1 respondsToSelector:setImage]) {
        UIImage *image = [UIImage animatedImageNamed:@"icon_home_" duration:0.3];
        [tab1 performSelectorWithArgs:setImage, image, @(YES), CGSizeZero];
    }
    if ([tab2 respondsToSelector:setImage]) {
        UIImage *image = [UIImage animatedImageNamed:@"icon_faxian_" duration:0.3];
        [tab2 performSelectorWithArgs:setImage, image, @(YES), CGSizeZero];
    }
    if ([tab3 respondsToSelector:setImage]) {
        UIImage *image = [UIImage animatedImageNamed:@"icon_wode_" duration:0.3];
        [tab3 performSelectorWithArgs:setImage, image, @(YES), CGSizeZero];
    }
    if (selectedIndex == 0) {
        UIImage *image = [[UIImage imageNamed:@"icon_home_10"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        if ([tab1 respondsToSelector:setImage]) {
            [tab1 performSelectorWithArgs:setImage afterDelay:0.2, image, @(YES), CGSizeZero];
        }
        [DYCommonTool appUserTrackDataPage:@"029" event:@"029001"];
    } else if (selectedIndex == 1) {
        UIImage *image = [[UIImage imageNamed:@"icon_faxian_10"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        if ([tab2 respondsToSelector:setImage]) {
            [tab2 performSelectorWithArgs:setImage afterDelay:0.2, image, @(YES), CGSizeZero];
        }
        [DYCommonTool appUserTrackDataPage:@"029" event:@"029002"];
    } else if (selectedIndex == 2) {
        UIImage *image = [[UIImage imageNamed:@"icon_wode_10"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        if ([tab3 respondsToSelector:setImage]) {
            [tab3 performSelectorWithArgs:setImage afterDelay:0.2, image, @(YES), CGSizeZero];
        }
        [DYCommonTool appUserTrackDataPage:@"029" event:@"029003"];
    }
}
@end

#pragma mark - NSObject+CYLTabBarControllerItem

@implementation NSObject (CYLTabBarControllerItemInternal)

- (void)cyl_setTabBarController:(CYLTabBarController *)tabBarController {
    objc_setAssociatedObject(self, @selector(cyl_tabBarController), tabBarController, OBJC_ASSOCIATION_ASSIGN);
}

@end

@implementation NSObject (CYLTabBarController)

- (CYLTabBarController *)cyl_tabBarController {
    CYLTabBarController *tabBarController = objc_getAssociatedObject(self, @selector(cyl_tabBarController));
    if (tabBarController) {
        return tabBarController;
    }
    if ([self isKindOfClass:[UIViewController class]] && [(UIViewController *)self parentViewController]) {
        tabBarController = [[(UIViewController *)self parentViewController] cyl_tabBarController];
        return tabBarController;
    }
    id<UIApplicationDelegate> delegate = ((id<UIApplicationDelegate>)[[UIApplication sharedApplication] delegate]);
    UIWindow *window = delegate.window;
    if ([window.rootViewController isKindOfClass:[CYLTabBarController class]]) {
        tabBarController = (CYLTabBarController *)window.rootViewController;
    }
    return tabBarController;
}

@end
