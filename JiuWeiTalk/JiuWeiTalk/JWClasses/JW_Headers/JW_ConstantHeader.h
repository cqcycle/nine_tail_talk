//
//  JW_ConstantHeader.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#ifndef JW_ConstantHeader_h
#define JW_ConstantHeader_h

#endif


///------
/// NSLog
///------
#ifdef DEBUG
#define NSLog(format, ...) printf("\n[%s] %s [第%d行] %s\n", __TIME__, __FUNCTION__, __LINE__, [[NSString stringWithFormat:format, ## __VA_ARGS__] UTF8String]);
#define debugLog(...) NSLog(__VA_ARGS__)
#define debugMethod() NSLog(@"%s", __func__)
#define Dnvironment @"DEBUG"
#else
#define NSLog(format, ...)
#define debugLog(...)
#define debugMethod()
#define Dnvironment @""
#endif


#define KJWLogError(error) NSLog(@"Error: %@", error)

#define iOS(version) ([[UIDevice currentDevice].systemVersion doubleValue] >= version)

///------
/// singleton
///------

// .h
#define single_interface(class)  + (class *)shared##class;

// .m
// \ 代表下一行也属于宏
// ## 是分隔符
#define single_implementation(class) \
static class *_instance; \
\
+ (class *)shared##class \
{ \
if (_instance == nil) { \
_instance = [[self alloc] init]; \
} \
return _instance; \
} \
\
+ (id)allocWithZone:(NSZone *)zone \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [super allocWithZone:zone]; \
}); \
return _instance; \
}

// 如果数据为NULL，设为nil
#define kPASS_NULL_TO_NIL(instance) (([instance isKindOfClass:[NSNull class]]) ? nil : instance)

// 处理nil，为空字符串@""
#define kSTRING_NIL_NULL(x) if(x == nil || [x isKindOfClass:[NSNull class]]){x = @"";}

#define kARRAY_NIL_NULL(x) \
if(x == nil || [x isKindOfClass:[NSNull class] ]) \
{x = @[];}

#define kDICTIONARY_NIL_NULL(x) \
if(x == nil || [x isKindOfClass:[NSNull class] ]) \
{x = @{};}



///image
#define KJWImageNamed(name) [UIImage imageNamed:name]

///font
#define KJWFont(size) [UIFont systemFontOfSize:size]
#define KJWBoldFont(size) [UIFont systemFontOfSize:size weight:UIFontWeightMedium]
#define KJWFontAttributeName(size) [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:size],NSFontAttributeName, nil]

///screen
#define KJWSCREEN_WIDTH  ([UIScreen mainScreen].bounds.size.width)
#define KJWSCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)
#define KJWSCREEN_FIT (SCREEN_WIDTH / 375)



///有关距离、位置
#define KJWCountWidth(w)  ((w)/750.0)*KJWSCREEN_WIDTH
///弱引用
#define kJWWeakSelf __weak __typeof__(self) weakSelf = self;
///NSUserDefaults单例
#define kJWUserDefManager NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];

///屏幕尺寸
#define iPhone5size ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone6size ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size)) : NO)
#define iPhone6plusSize ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(1125, 2001), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size)) : NO)
#define iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? \
(CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) || \
CGSizeEqualToSize(CGSizeMake(750, 1624), [[UIScreen mainScreen] currentMode].size)  || \
CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size)  || \
CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size))   \
: NO)


///------
///系统信息、公参
///------
#define APP_CURNAME  ([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"])
#define APP_VERSION ([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"])
#define APP_BUILD   ([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"])
#define INNER_VER @"6" //内部版本号
#define PLATFORM @"2"
#define DEVICE_NAME ([UIDevice currentDevice].machineModelName)
#define SYS_VER ([UIDevice currentDevice].systemVersion)
#define CHANNEL @"appstore"
#define APPNAME @"jiuweiplus"
#define APPSECRET @"5ac7b8cde25042eaa15d70c23c86233b" //正式
//#define APPSECRET @"1234567890abcdefg" //测试


///------
/// YYCache
///------
#define Cache @"JW_Cache"
#define Cache_JW_UserModel @"JW_JW_UserModel"
#define Cache_LastEvent @"JW_LastEvent"


///搜索按钮name
static NSString * const kSearchImageName = @"jw_home_search";






