//
//  JW_ColorHeader.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#ifndef JW_ColorHeader_h
#define JW_ColorHeader_h

#define KJWRGB(r, g, b) [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]
#define KJWRGBA(r, g, b,a) [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:(a)]
#define KJWHexRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define KJWHexRGBA(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:(a)]

///主题色
#define KJWMainColor  KJWHexRGB(0xCA2DE1)

#define kNormalTextColor_51       KJWRGB(51.f, 51.f, 51.f)
#define kNormalTextColor_85       KJWRGB(85.f, 85.f, 85.f)
#define kNormalTextColor_153      KJWRGB(153.f, 153.f, 153.f)
#define kNormalTextColor_173      KJWRGB(173.f, 173.f, 173.f)
#define kNormalTextColor_124      KJWRGB(124.f, 124.f, 124.f)
#define kNormalTextColor_253      KJWRGB(253.f, 253.f, 253.f)
#define kNormalTextColor_141      KJWRGB(141.f, 141.f, 141.f)
#define kNormalTextColor_216      KJWRGB(216.f, 216.f, 216.f)

// 线条浅灰颜色
#define kNormalColor_230          KJWRGB(230.f, 230.f, 230.f)



#endif /* JW_ColorHeader_h */
