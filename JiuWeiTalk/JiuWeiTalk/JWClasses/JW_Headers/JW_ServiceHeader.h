//
//  JW_ServiceHeader.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#ifndef JW_ServiceHeader_h
#define JW_ServiceHeader_h

///测试服 EnvironmentNum =  2
static NSString * const kDevDomain = @"";

///正式服EnvironmentNum = 1
static NSString * const kDomain = @"";
/////测试服 EnvironmentNum =  2
//static NSString * const kDevDomain = @"http://plustest.jiuweidb.com/";
//
/////正式服EnvironmentNum = 1
//static NSString * const kDomain = @"http://plus.jiuweidb.com/";

///拼接url
#define KJWJoinString(url) [NSString stringWithFormat:@"%@%@",[NSObject jw_getBaseUrl], url]

static NSString * const kRequestFailureMsg = @"网络好像开小差了，请稍后重试~";

///第三方SDK
static NSString * const kUmengAppKey = @"5ce3aebd4ca357ace8000d16"; // 友盟
static NSString * const kJPUSHAppKey = @""; // 极光
//static NSString * const kBaiduMapAppKey = @"HIFG93KGx2258GaZdPbN2g1611aGqYhw"; // 百度地图
static NSString * const kWechatAppkey = @"";                  // 微信
static NSString * const kWechatAppSecret = @"";
//static NSString * const kQQAppkey = @"1107229728";                              // QQ
//static NSString * const kSinaAppkey = @"1834079387";                            // 新浪微博
//static NSString * const kSinaAppSecret = @"55ebf6818c14eadbd711da77851c0429";
//static NSString * const kSinaRedirectURL = @"https://sns.whalecloud.com/sina2/callback";

// 用户协议
static NSString * const kAgreementUrl = @"http://dayou-picture.oss-cn-shanghai.aliyuncs.com/ninetales/jwp_agreement.html";

#endif /* JW_ServiceHeader_h */
