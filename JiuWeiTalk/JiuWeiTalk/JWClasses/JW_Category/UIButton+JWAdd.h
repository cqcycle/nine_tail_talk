//
//  UIButton+JWAdd.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (JWAdd)
/**
 *  使用颜色设置按钮背景
 *
 *  @param backgroundColor 背景颜色
 *  @param state           按钮状态
 */
- (void)jw_setBackgroundColor:(UIColor *)backgroundColor
                     forState:(UIControlState)state;
@end

NS_ASSUME_NONNULL_END
