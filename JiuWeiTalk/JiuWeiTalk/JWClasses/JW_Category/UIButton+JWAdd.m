//
//  UIButton+JWAdd.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "UIButton+JWAdd.h"
#import "UIImage+JWAdd.h"
@implementation UIButton (JWAdd)
- (void)jw_setBackgroundColor:(UIColor *)backgroundColor
                      forState:(UIControlState)state {
    [self setBackgroundImage:[UIImage jw_imageWithColor:backgroundColor] forState:state];
}
@end
