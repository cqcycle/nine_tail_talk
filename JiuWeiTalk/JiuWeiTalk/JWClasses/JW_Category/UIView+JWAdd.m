//
//  UIView+JWAdd.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "UIView+JWAdd.h"



@implementation UIView (JWAdd)

/**
 *  设置图层的圆角
 *
 *  @param cornerRadius 圆角弧度
 */
- (void)jw_setCornerRadius:(CGFloat)cornerRadius {
    self.layer.cornerRadius = cornerRadius;
    self.clipsToBounds = YES;
}

/*
 *  设置一个圆角
 */
- (void)jw_setRoundedCorners:(UIRectCorner)corners
                      radius:(CGFloat)radius {
    CGRect rect = self.bounds;
    
    // Create the path
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:rect
                                                   byRoundingCorners:corners
                                                         cornerRadii:CGSizeMake(radius, radius)];
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = rect;
    maskLayer.path = maskPath.CGPath;
    
    // Set the newly created shape layer as the mask for the view's layer
    self.layer.mask = maskLayer;
}

/**
 *  设置layer边框
 *
 *  @param width 边框宽度
 *  @param color 边框颜色
 */
- (void)jw_setBorderWidth:(CGFloat)width color:(nullable UIColor *)color {
    self.layer.borderWidth = width;
    self.layer.borderColor = [color CGColor];
}

/**
 设置view.layer的阴影
 
 @param color  阴影颜色
 @param offset 阴影offset
 @param radius 阴影radius
 */
- (void)jw_setLayerShadow:(nullable UIColor*)color
                   offset:(CGSize)offset
                   radius:(CGFloat)radius {
    self.layer.shadowColor = color.CGColor;
    self.layer.shadowOffset = offset;
    self.layer.shadowRadius = radius;
    self.layer.shadowOpacity = 1;
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
}


/**
 *
 *  找到第一个处于活动中的view
 *  @return 活动中的View,如果找不到返回nil
 */
- (UIView *)findFirstResponder {
    if ([self isFirstResponder]) {
        return self;
    }
    
    NSArray *subviews = [self subviews];
    for (UIView *subview in subviews) {
        UIView *firstResponder = [subview findFirstResponder];
        
        if (firstResponder) {
            return firstResponder;
        }
    }
    return nil;
}


/*
 周边加阴影，并且同时圆角
 */
- (void)addShadowToView:(UIView *)view
            withOpacity:(float)shadowOpacity
           shadowRadius:(CGFloat)shadowRadius
        andCornerRadius:(CGFloat)cornerRadius
{
    //////// shadow /////////
    CALayer *shadowLayer = [CALayer layer];
    shadowLayer.frame = view.layer.frame;
    
    shadowLayer.shadowColor = [UIColor blackColor].CGColor;//shadowColor阴影颜色
    shadowLayer.shadowOffset = CGSizeMake(0, 0);//shadowOffset阴影偏移，默认(0, -3),这个跟shadowRadius配合使用
    shadowLayer.shadowOpacity = shadowOpacity;//0.8;//阴影透明度，默认0
    shadowLayer.shadowRadius = shadowRadius;//8;//阴影半径，默认3
    
    //路径阴影
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    float width = shadowLayer.bounds.size.width;
    float height = shadowLayer.bounds.size.height;
    float x = shadowLayer.bounds.origin.x;
    float y = shadowLayer.bounds.origin.y;
    
    CGPoint topLeft      = shadowLayer.bounds.origin;
    CGPoint topRight     = CGPointMake(x + width, y);
    CGPoint bottomRight  = CGPointMake(x + width, y + height);
    CGPoint bottomLeft   = CGPointMake(x, y + height);
    
    CGFloat offset = -1.f;
    [path moveToPoint:CGPointMake(topLeft.x - offset, topLeft.y + cornerRadius)];
    [path addArcWithCenter:CGPointMake(topLeft.x + cornerRadius, topLeft.y + cornerRadius) radius:(cornerRadius + offset) startAngle:M_PI endAngle:M_PI_2 * 3 clockwise:YES];
    [path addLineToPoint:CGPointMake(topRight.x - cornerRadius, topRight.y - offset)];
    [path addArcWithCenter:CGPointMake(topRight.x - cornerRadius, topRight.y + cornerRadius) radius:(cornerRadius + offset) startAngle:M_PI_2 * 3 endAngle:M_PI * 2 clockwise:YES];
    [path addLineToPoint:CGPointMake(bottomRight.x + offset, bottomRight.y - cornerRadius)];
    [path addArcWithCenter:CGPointMake(bottomRight.x - cornerRadius, bottomRight.y - cornerRadius) radius:(cornerRadius + offset) startAngle:0 endAngle:M_PI_2 clockwise:YES];
    [path addLineToPoint:CGPointMake(bottomLeft.x + cornerRadius, bottomLeft.y + offset)];
    [path addArcWithCenter:CGPointMake(bottomLeft.x + cornerRadius, bottomLeft.y - cornerRadius) radius:(cornerRadius + offset) startAngle:M_PI_2 endAngle:M_PI clockwise:YES];
    [path addLineToPoint:CGPointMake(topLeft.x - offset, topLeft.y + cornerRadius)];
    
    //设置阴影路径
    shadowLayer.shadowPath = path.CGPath;
    
    //////// cornerRadius /////////
    view.layer.cornerRadius = cornerRadius;
    view.layer.masksToBounds = YES;
    view.layer.shouldRasterize = YES;
    view.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    [view.superview.layer insertSublayer:shadowLayer below:view.layer];
}

/*
 *  设置一个斜角
 */
- (void)jw_setCutCorners:(UIRectCorner)corners{
    
    // Create the path
    CGMutablePathRef wavePath = CGPathCreateMutable();
    
    switch (corners) {
        case UIRectCornerTopLeft:
            CGPathMoveToPoint(wavePath, nil,  0, self.height);
            CGPathAddLineToPoint(wavePath, nil, self.height / 2, 0);
            CGPathAddLineToPoint(wavePath, nil, self.width, 0);
            CGPathAddLineToPoint(wavePath, nil, self.width, self.height);
            break;
        case UIRectCornerTopRight:
            CGPathMoveToPoint(wavePath, nil, 0, 0);
            CGPathAddLineToPoint(wavePath, nil, self.width - self.height / 2, 0);
            CGPathAddLineToPoint(wavePath, nil, self.width, self.height);
            CGPathAddLineToPoint(wavePath, nil, 0, self.height);
            break;
        case UIRectCornerBottomLeft:
            CGPathMoveToPoint(wavePath, nil, 0, 0);
            CGPathAddLineToPoint(wavePath, nil, self.width, 0);
            CGPathAddLineToPoint(wavePath, nil, self.width, self.height);
            CGPathAddLineToPoint(wavePath, nil, self.height / 2, self.height);
            break;
        case UIRectCornerBottomRight:
            CGPathMoveToPoint(wavePath, nil, 0, 0);
            CGPathAddLineToPoint(wavePath, nil, self.width, 0);
            CGPathAddLineToPoint(wavePath, nil, self.width - self.height / 2, self.height);
            CGPathAddLineToPoint(wavePath, nil, 0, self.height);
            break;
            
        default:
            break;
    }
    
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.bounds;
    maskLayer.path = wavePath;
    // Set the newly created shape layer as the mask for the view's layer
    self.layer.mask = maskLayer;
}
@end
