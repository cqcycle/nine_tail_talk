//
//  NSString+JWAddForRegex.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (JWAddForRegex)

///手机号是否有效
- (BOOL)isValidPhoneNumber;

///密码是否有效
- (BOOL)isValidPassword;


/// 秒转化成时分秒
/// @param interval 秒数
+ (NSString *)stringWithTimeInterval:(NSTimeInterval)interval;

/// 时长转化秒数
/// @param duration 时长 00:00:00(格式)
+ (NSTimeInterval)intervalWithDuration:(NSString *)duration;

/// 判断字符串是否含有网址
/// @param content 字符串
+ (BOOL)isUrlWithContent:(NSString *)content;

@end

NS_ASSUME_NONNULL_END
