//
//  UITextField+create.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "UITextField+create.h"

@implementation UITextField (create)


+ (instancetype)jw_textFieldWithTextColor:(UIColor *)color
                                 textFont:(CGFloat)fontSize
                              placeholder:(NSString *)placeholder
{
    
    UITextField *textField = [UITextField new];
    textField.placeholder = placeholder;
    textField.textColor = color;
    textField.font = KJWFont(fontSize);
    
    return textField;
}

- (void)setLeftViewWidth:(CGFloat)width{
    
    self.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 0)];
    self.leftViewMode = UITextFieldViewModeAlways;
    
}

/** 设置占位文字颜色 + 字体样式 */
- (void)jw_setattributedPlaceholder:(NSString *)placeholder textColor:(UIColor *)color textFont:(UIFont *)font{
    
    NSDictionary *dict = @{NSForegroundColorAttributeName : color, NSFontAttributeName : font};
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:placeholder attributes:dict];
    self.attributedPlaceholder = attrString;
    
}


@end
