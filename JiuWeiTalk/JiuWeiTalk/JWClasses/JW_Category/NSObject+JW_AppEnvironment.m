//
//  NSObject+JW_AppEnvironment.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "NSObject+JW_AppEnvironment.h"
#import<objc/runtime.h>
static NSString *const outSideKey = @"AppConfig";
static NSString *const intSideKey = @"EnvironmentNum";
static NSInteger const appConfig  = 2;///默认开发环境
@implementation NSObject (JW_AppEnvironment)
/**
 * 1. 获取开发环境，获取的时候（AppConfig在info.plist中为字典，EnvironmentNum为该字典下的key）
 */
- (int)jw_getEnvironmentType {
        NSDictionary *resultDict = [self appConfigDictionary];
        
        if (resultDict.count) {
    //        NSLog(@"%@",resultDict);
            if ([resultDict.allKeys containsObject:intSideKey]) {
                NSString *value = resultDict[intSideKey];
    //            NSLog(@"%@",value);
                 return [value intValue];
            }else{
                NSLog(@"没有对应的%@ 的key",intSideKey);
                return appConfig;
            }
        }
        ///默认为开发环境
        return appConfig;
}
- (NSDictionary *)appConfigDictionary {
    NSDictionary *dict    = [self appInfoDictionary];
    NSArray *dictKeyArray = dict.allKeys;
    if (dictKeyArray.count) {
            if ([dictKeyArray containsObject:outSideKey]) {
                return [dict objectForKey:outSideKey];
            }else{
                NSLog(@"没有对应的%@ 的key",outSideKey);
                return nil;
            }
//        }
        
    }
    return nil;
}
- (NSDictionary *)appInfoDictionary {
    NSDictionary *infoDict = objc_getAssociatedObject(self, "app_info");
       // NSLog(@"1---%@",infoDict);
    if (!infoDict) {
        infoDict = [[NSBundle mainBundle] infoDictionary];
        objc_setAssociatedObject(self, "app_info", infoDict, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
             //   NSLog(@"2---%@",infoDict);
        // NSLog(@"%@",[NSBundle mainBundle].infoDictionary[@"AppConfig"]);
    }
    
    return infoDict;
}
/**
 * 2.根据对应结果 设置基本url
 */
+ (NSString *)jw_getBaseUrl {
    int environment = [self jw_getEnvironmentType];
    if (environment == EnvironmentTypeOnLine) {
        ///线上环境
        return kDomain;
    } else if (environment == EnvironmentTypeDevelop) {
        ///开发环境
        return kDevDomain;
    } else if (environment == EnvironmentTypeUat) {
        ///Uat环境
        return nil;
    } else {
        return nil;
    }
}
@end
