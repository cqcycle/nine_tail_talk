//
//  UIScrollView+EmptyData.m
//  tongrenbao
//
//  Created by zcx on 15/8/13.
//  Copyright (c) 2015年 ZhengCaixi. All rights reserved.
//

#import "UIScrollView+EmptyData.h"
#import <objc/runtime.h>

static char titleKey;
static char iconKey;
static char descriptionKey;
static char showEmptyKey;

static NSString * const titleColor = @"D7D7D7";
static NSString * const descriptionColor = @"D7D7D7";

@implementation UIScrollView (EmptyData)

- (void)configEmptyData:(NSString *)title description:(NSString *)description icon:(NSString *)iconName {
    
    self.emptyDataSetSource = self;
    self.emptyDataSetDelegate = self;

    objc_setAssociatedObject(self, &titleKey, title, OBJC_ASSOCIATION_COPY);
    objc_setAssociatedObject(self, &descriptionKey, description, OBJC_ASSOCIATION_COPY);
    objc_setAssociatedObject(self, &iconKey, iconName, OBJC_ASSOCIATION_COPY);
    
    [self reloadEmptyDataSet];
}

- (void)showEmptyView {
    objc_setAssociatedObject(self, &showEmptyKey, @(YES), OBJC_ASSOCIATION_COPY);
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = [objc_getAssociatedObject(self, &titleKey) copy];
    if (text) {
        UIFont *font = KJWFont(20.0);
        UIColor *textColor = [UIColor colorWithHexString:titleColor];
        NSMutableDictionary *attributes = [NSMutableDictionary new];
        [attributes setObject:font forKey:NSFontAttributeName];
        [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
        return [[NSAttributedString alloc] initWithString:text attributes:attributes];
    }
    return nil;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *description = [objc_getAssociatedObject(self, &descriptionKey) copy];
    if (description) {
        UIFont *font = KJWFont(14.0);
        UIColor *textColor = [UIColor colorWithHexString:descriptionColor];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 5;
        paragraphStyle.alignment = NSTextAlignmentCenter;
        NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
        [attributes setObject:font forKey:NSFontAttributeName];
        [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
        [attributes setObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
        
        return [[NSAttributedString alloc] initWithString:description attributes:attributes];
    }
    return nil;
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *imageName = [objc_getAssociatedObject(self, &iconKey) copy];
    if (imageName) {
        return [UIImage imageNamed:imageName];
    }
    return nil;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    if (CGRectGetHeight([UIScreen mainScreen].bounds)==480) {
        return -10;
    }
    return -80;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return YES;
}

//- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
//    NSNumber *showEmpty = [objc_getAssociatedObject(self, &showEmptyKey) copy];
//    return [showEmpty boolValue];
//}

@end
