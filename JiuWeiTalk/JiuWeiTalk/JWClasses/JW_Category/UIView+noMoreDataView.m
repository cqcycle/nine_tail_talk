//
//  UIView+noMoreDataView.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "UIView+noMoreDataView.h"
#import "UIView+DDAddition.h"
#define kNoDataViewTag 2138912381924
@implementation UIView (noMoreDataView)

- (void)jw_showNoDataViewWithContent:(NSString *)content {
    
    [self jw_dismissNoDataView];
    UIView *view = [self noDataViewWithContent:content];
    view.center = CGPointMake(self.width/2, self.height/2+10);
    [self addSubview:view];
}


- (UIView *)noDataViewWithContent:(NSString *)content {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 150)];
    view.tag = kNoDataViewTag;
    
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"img_noDataSource"]];
    imgView.frame = CGRectMake(0, 0, 150, 120);
    imgView.center = CGPointMake(view.center.x, view.center.y-30);
    [view addSubview:imgView];
    
    
    UILabel *label = [UILabel new];
    label.text = content;
    label.font = [UIFont systemFontOfSize:14];
    label.frame = CGRectMake(0, CGRectGetMaxY(imgView.frame)+15, view.width, 15);
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    
    return view;
    
}


- (void)jw_dismissNoDataView
{
    for (UIView *view in self.subviews)
    {
        if (view.tag == kNoDataViewTag) {
            [view removeFromSuperview];
        }
    }
}


- (void)jw_showNoDataViewWithContent:(NSString *)content andImage:(NSString *)imageName{
    
    [self jw_dismissNoDataView];
    
    UIView *addView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 150)];
    addView.tag = kNoDataViewTag;
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imageName]];
    [addView addSubview:imageView];
    imageView.frame = CGRectMake(0, 0, 150, 120);
    imageView.center = CGPointMake(addView.center.x, addView.center.y - 30);
    UILabel *label = [UILabel new];
    label.text = content;
    label.font = [UIFont systemFontOfSize:14];
    label.frame = CGRectMake(0, CGRectGetMaxY(imageView.frame)+15, addView.width, 15);
    label.textAlignment = NSTextAlignmentCenter;
    [addView addSubview:label];
    
    addView.center = CGPointMake(self.width/2, self.height/2+10) ;
    [self addSubview:addView];
}



- (void)jw_showNoDataViewWithFrame:(CGRect)frame Content:(NSString *)content andImage:(NSString *)imageName{
    //先移除之前的数据
    [self jw_dismissNoDataView];
    
    UIView *addView = [[UIView alloc]initWithFrame:frame];
    addView.tag = kNoDataViewTag;
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imageName]];
    [addView addSubview:imageView];
    imageView.frame = CGRectMake(0, 0, 150, 120);
    imageView.center = CGPointMake(addView.center.x, addView.center.y - 30);
    UILabel *label = [UILabel new];
    label.text = content;
    label.font = [UIFont systemFontOfSize:14];
    label.frame = CGRectMake(10, imageView.top+15, addView.width, 15);
    label.textAlignment = NSTextAlignmentCenter;
    [addView addSubview:label];
    
    addView.center = CGPointMake(self.width/2, self.height/2+10) ;
    [self addSubview:addView];
}
@end
