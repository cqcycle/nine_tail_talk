//
//  UIViewController+DarkModel.m
//  NineTails
//
//  Created by 亚刘 on 2019/11/11.
//  Copyright © 2019 dayou. All rights reserved.
//

#import "UIViewController+DarkModel.h"



@implementation UIViewController (DarkModel)


- (UIUserInterfaceStyle)overrideUserInterfaceStyle{
    
    return UIUserInterfaceStyleLight;
    
}


@end
