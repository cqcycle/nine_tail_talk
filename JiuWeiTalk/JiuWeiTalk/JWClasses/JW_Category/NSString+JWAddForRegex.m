//
//  NSString+JWAddForRegex.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "NSString+JWAddForRegex.h"

@implementation NSString (JWAddForRegex)



///手机号是否有效
- (BOOL)isValidPhoneNumber {
    NSString *regex = @"^[0-9]{11}";
    return [self isValidateByRegex:regex];
}

///密码是否有效
- (BOOL)isValidPassword {
    //以字母开头，只能包含“字母”，“数字”，“标点符号”，长度6~18
    //    NSString *regex = @"^([a-zA-Z]|[a-zA-Z0-9]|[0-9]){6,18}$";
    //    return [self wl_isValidateByRegex:regex];
    //8到16位
    if (self.length >= 6 && self.length <= 18) {
        return YES;
    }else {
        return NO;
    }
}

#pragma mark - 正则相关
- (BOOL)isValidateByRegex:(NSString *)regex {
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [pre evaluateWithObject:self];
}

/// 时长转化秒数
/// @param duration 时长 00:00:00(格式)
+ (NSTimeInterval)intervalWithDuration:(NSString *)duration{

    NSTimeInterval interval = 0;
    NSArray *array = [duration componentsSeparatedByString:@":"]; //字符串按照;分隔成数组
    
    if (array.count <= 2) { // 分秒00:00
        
        NSString *minStr = [array firstObject];
        NSString *secStr = [array lastObject];
        
        NSTimeInterval min = [self intervalMinWithString:minStr];
        NSTimeInterval sec = [self intervalSecWithString:secStr];
        return min + sec;
        
    }
    
    else{ // 时分秒 00:00:00
        
        NSString *hourStr = array[0];
        NSString *minStr = array[1];
        NSString *secStr = array[2];
        NSTimeInterval hour = [self intervalHourWithString:hourStr];
        NSTimeInterval min = [self intervalMinWithString:minStr];
        NSTimeInterval sec = [self intervalSecWithString:secStr];
        return hour + min + sec;
        
    }
    return interval;
    
}


/// 秒
/// @param string 00
+ (NSTimeInterval)intervalSecWithString:(NSString *)string{

    if ([string hasPrefix:@"0"]) {

        string = [string substringFromIndex:1];
        return [string doubleValue];
        
    }else{
        
        return [string doubleValue];
    }
    
}

/// 分
/// @param string 00
+ (NSTimeInterval)intervalMinWithString:(NSString *)string{

    if ([string hasPrefix:@"0"]) {

        string = [string substringFromIndex:1];
        return [string doubleValue] * 60;
        
    }else{
        
        return [string doubleValue] * 60;
    }
}


/// 时
/// @param string 00
+ (NSTimeInterval)intervalHourWithString:(NSString *)string{
    
    if ([string hasPrefix:@"0"]) {

        string = [string substringFromIndex:1];
        return [string doubleValue] * 3600;
        
    }else{
        
        return [string doubleValue] * 3600;
    }
    
}




+ (NSString *)stringWithTimeInterval:(NSTimeInterval)interval{
    
    NSInteger min = interval / 60;
    NSInteger sec = (NSInteger)interval % 60;
    NSInteger hour = (NSInteger)interval % 3600;
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",hour,sec,min];
    
}


+ (BOOL)isUrlWithContent:(NSString *)content{

    if(content.length > 0){
        
        NSString *url;
        if (content.length > 4 && [[content substringToIndex:4] isEqualToString:@"www."]) {
            
            url = [NSString stringWithFormat:@"http://%@",content];
            
        }else{
        
            url = content;
            
        }
        
        NSString *urlRegex = @"(https|http|ftp|rtsp|igmp|file|rtspt|rtspu)://((((25[0-5]|2[0-4]\\d|1?\\d?\\d)\\.){3}(25[0-5]|2[0-4]\\d|1?\\d?\\d))|([0-9a-z_!~*'()-]*\\.?))([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\\.([a-z]{2,6})(:[0-9]{1,4})?([a-zA-Z/?_=]*)\\.\\w{1,5}";
        
        NSPredicate* urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegex];
        return [urlTest evaluateWithObject:url];
        
    }else{
        
        return NO;
    }
    
}


@end
