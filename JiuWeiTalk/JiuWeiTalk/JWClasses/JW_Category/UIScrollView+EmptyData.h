//
//  UIScrollView+EmptyData.h
//  tongrenbao
//
//  Created by zcx on 15/8/13.
//  Copyright (c) 2015年 ZhengCaixi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@interface UIScrollView (EmptyData)<DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

- (void)configEmptyData:(NSString *)title description:(NSString *)description icon:(NSString *)iconName;

//- (void)showEmptyView;

@end
