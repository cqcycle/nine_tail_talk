//
//  UIBarButtonItem+barButtonItem.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "UIBarButtonItem+barButtonItem.h"

@implementation UIBarButtonItem (barButtonItem)
+ (UIBarButtonItem *)jw_barButtonItemWithTitleName:(NSString *)titleName itemBlock:(void(^)(void))block{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:titleName forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithHexString:@"#262626"] forState:UIControlStateNormal];
    btn.titleLabel.font = KJWFont(16);
    [btn sizeToFit];
    [[btn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        if (block) {
            block();
        }
    }];
    
    return  [[UIBarButtonItem alloc] initWithCustomView:btn];
}
+ (UIBarButtonItem *)jw_buttonItemWithTitleName:(NSString *)titleName itemBlock:(void(^)(UIButton *btn))block{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:titleName forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = KJWFont(15);
    [btn sizeToFit];
    [[btn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        if (block) {
            block(btn);
        }
    }];
    
    return  [[UIBarButtonItem alloc] initWithCustomView:btn];
}
+ (UIBarButtonItem *)jw_barButtonItemWithImageName:(NSString *)imageName itemBlock:(void(^)(void))block{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    
    [btn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_select",imageName]] forState:UIControlStateHighlighted];
    
    [btn sizeToFit];
    
    [[btn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        if (block) {
            block();
        }
    }];
    
    return  [[UIBarButtonItem alloc] initWithCustomView:btn];
}
+ (UIBarButtonItem *)jw_buttonItemWithImageName:(NSString *)imageName itemBlock:(void(^)(UIButton *btn))block{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    
    [btn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_select",imageName]] forState:UIControlStateHighlighted];
    
    [btn sizeToFit];
    
    [[btn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        if (block) {
            block(btn);
        }
    }];
    
    return  [[UIBarButtonItem alloc] initWithCustomView:btn];
}
@end
