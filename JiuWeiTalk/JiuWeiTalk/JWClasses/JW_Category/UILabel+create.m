//
//  UILabel+create.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "UILabel+create.h"

@implementation UILabel (create)



+ (UILabel *)jw_labelWithTextColor:(UIColor *)textColor
                          textFont:(CGFloat)textFont
                              text:(NSString *)text{

    UILabel *label = [[UILabel alloc] init];
    label.text = text;
    label.textColor = textColor;
    
    label.font = KJWFont(textFont);
    
    return label;
    
}

+ (UILabel *)jw_labelWithTextColor:(UIColor *)textColor
                      boldTextFont:(CGFloat)textFont
                              text:(NSString *)text{
    UILabel *label = [[UILabel alloc] init];
    label.text = text;
    label.textColor = textColor;
    label.font = KJWBoldFont(textFont);
    
    return label;
}

@end
