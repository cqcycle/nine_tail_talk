//
//  UITextView+Placeholder.h
//  TongRenWeiShi_iOS
//
//  Created by zcx on 15/9/24.
//  Copyright © 2015年 继续前行. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (Placeholder)

@property (nonatomic, assign) BOOL showPlaceholder;/**< 是否需要显示placeholder，默认不显示*/
@property (nonatomic, copy) NSString *placeholder;/**< 占位符文本*/
@property (strong, nonatomic) UILabel *placeholderLabel;

@end
