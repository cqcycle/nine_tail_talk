//
//  UIImage+JWAdd.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (JWAdd)

/**
 用给定的颜色创建并返回一个1*1大小的图像
 @param color  颜色.
 */
+ (nullable UIImage *)jw_imageWithColor:(UIColor *)color;

/**
 用指定的颜色和大小创建一个纯彩色图像。
 
 @param color  颜色.
 @param size   新图片的大小.
 */
+ (nullable UIImage *)jw_imageWithColor:(UIColor *)color size:(CGSize)size;


/**
 在指定的矩形区域内绘制图像，用contentMode改变内容。
 
 @讨论 该方法在当前的图像上下文中绘制图像，遵守图像的方向设置。
 在默认坐标系中,图像在指定的矩形区域的位置。该方法适用于当前图形的任何变换。
 
 @param rect        绘制图像的区域.
 @param contentMode 绘制模式
 @param clips       一个BOOL值用来确定内容局限在矩形区域。
 */
- (void)jw_drawInRect:(CGRect)rect withContentMode:(UIViewContentMode)contentMode clipsToBounds:(BOOL)clips;

/**
 从这个图像的缩放返回一个新的图像。图像根据需要将被拉伸。
 @param size  要缩放的新的尺寸，值必须为正值.
 @return      给定大小的新图像.
 */
- (nullable UIImage *)jw_imageByResizeToSize:(CGSize)size;

/**
 返回一个从这个图像缩放的新的图像。图像内容将通过contentMode改变。
 
 @param size        要缩放的新尺寸，应为正.
 @param contentMode 图像内容模式.
 @return 给定大小的新图像.
 */
- (nullable UIImage *)jw_imageByResizeToSize:(CGSize)size contentMode:(UIViewContentMode)contentMode;

/**
 用给定的圆角大小圈一个新的图片
 @param radius 每个角点的半径.
 */
- (nullable UIImage *)jw_imageByRoundCornerRadius:(CGFloat)radius;

/**
 用给定的圆角大小圈一个新的图片
 
 @param radius       每个角点的半径.
 @param borderWidth  边框线宽度.
 @param borderColor  边框的笔画颜色. nil指clear color.
 */
- (nullable UIImage *)jw_imageByRoundCornerRadius:(CGFloat)radius
                                      borderWidth:(CGFloat)borderWidth
                                      borderColor:(nullable UIColor *)borderColor;

/**
 用给定的圆角大小圈一个新的图片
 
 @param radius       每个角点的半径.
 @param corners      你想圆角的值的标识。你可以用这个参数来设置圆角的子集。
 @param borderWidth  边框线宽度.
 @param borderColor  边框的笔画颜色. nil指clear color.
 @param borderLineJoin 边界线.
 */
- (nullable UIImage *)jw_imageByRoundCornerRadius:(CGFloat)radius
                                          corners:(UIRectCorner)corners
                                      borderWidth:(CGFloat)borderWidth
                                      borderColor:(nullable UIColor *)borderColor
                                   borderLineJoin:(CGLineJoin)borderLineJoin;


@end

NS_ASSUME_NONNULL_END
