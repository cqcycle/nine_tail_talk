//
//  UITextView+Placeholder.m
//  TongRenWeiShi_iOS
//
//  Created by zcx on 15/9/24.
//  Copyright © 2015年 继续前行. All rights reserved.
//

#import "UITextView+Placeholder.h"
#import <objc/runtime.h>

static char kShowPlaceholderKey;
static char kPlaceholderLabelKey;
static char kPlaceholderStringKey;

@interface UITextView ()

@end

@implementation UITextView (Placeholder)

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)setShowPlaceholder:(BOOL)showPlaceholder {
    objc_setAssociatedObject(self, &kShowPlaceholderKey, @(showPlaceholder), OBJC_ASSOCIATION_ASSIGN);
    if (showPlaceholder) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChangeNotification:) name:UITextViewTextDidChangeNotification object:self];
    }
}

- (BOOL)showPlaceholder {
    return [objc_getAssociatedObject(self, &kShowPlaceholderKey) boolValue] ;
}

- (void)setPlaceholder:(NSString *)placeholder {
    objc_setAssociatedObject(self, &kPlaceholderStringKey, placeholder, OBJC_ASSOCIATION_COPY);
    if (self.placeholderLabel) {
        self.placeholderLabel.text = placeholder;
        self.placeholderLabel.font = self.font;
        self.placeholderLabel.frame = CGRectMake(5, 8, self.placeholderLabel.intrinsicContentSize.width, self.placeholderLabel.intrinsicContentSize.height);
        self.placeholderLabel.hidden = self.text.length;
    }
}

- (NSString *)placeholder {
    return [objc_getAssociatedObject(self, &kPlaceholderStringKey) copy];
}

- (UILabel *)placeholderLabel {
    UILabel *label = objc_getAssociatedObject(self, &kPlaceholderLabelKey);
    if (!label) {
        label = [[UILabel alloc] init];
        label.textColor = [UIColor lightGrayColor];
        label.font = self.font;
        [self addSubview:label];
        objc_setAssociatedObject(self, &kPlaceholderLabelKey, label, OBJC_ASSOCIATION_ASSIGN);
    }
    return label;
}

- (void)textDidChangeNotification:(NSNotification *)notification {
    if (self.placeholderLabel) {
        self.placeholderLabel.hidden = self.text.length;
    }
}

@end
