//
//  UIBarButtonItem+barButtonItem.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIBarButtonItem (barButtonItem)
+ (UIBarButtonItem *)jw_barButtonItemWithTitleName:(NSString *)titleName itemBlock:(void(^)(void))block;
+ (UIBarButtonItem *)jw_buttonItemWithTitleName:(NSString *)titleName itemBlock:(void(^)(UIButton *btn))block;
+ (UIBarButtonItem *)jw_barButtonItemWithImageName:(NSString *)imageName itemBlock:(void(^)(void))block;
+ (UIBarButtonItem *)jw_buttonItemWithImageName:(NSString *)imageName itemBlock:(void(^)(UIButton *btn))block;
@end

NS_ASSUME_NONNULL_END
