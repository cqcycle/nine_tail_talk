//
//  UIColor+method.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//


// 创建带透明度的RGB色
#define kRGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

// 创建alpha为1的RGB色
#define kRGB(r, g, b) kRGBA(r, g, b, 1)

// 创建alpha为1的灰度色
#define kRGBG(r) kRGB(r, r, r)

#define kALPHA(c, a) [c jw_colorWithAlpha:a]

#define kHEX(h) [UIColor jw_colorWithHexString:h]
#define kHEXG(h) [UIColor jw_colorWithGrayHexString:h]


NS_ASSUME_NONNULL_BEGIN

@interface UIColor (method)



/**
 根据颜色的16进制表示获取

 @param hexString 16进制颜色字符串
 @return 颜色
 */
+ (UIColor *)jw_colorWithHexString:(NSString *)hexString;
+ (UIColor *)jw_colorWithGrayHexString:(NSString *)grayhexString;

/**
 根据颜色创建图片

 @param color 颜色
 @return 图片
 */
+ (UIImage *)jw_createImageWithColor:(UIColor *)color;
+ (UIImage *)jw_createImageWithColor:(UIColor *)color andHeight:(CGFloat)height;


/**
 改变已有颜色的透明度

 @param alpha 透明度
 @return 颜色
 */
- (instancetype)jw_colorWithAlpha:(CGFloat)alpha;


@end

NS_ASSUME_NONNULL_END
