//
//  UIView+JWAdd.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//


NS_ASSUME_NONNULL_BEGIN

@interface UIView (JWAdd)

/**
 *  设置图层的圆角
 *
 *  @param cornerRadius 圆角弧度
 */
- (void)jw_setCornerRadius:(CGFloat)cornerRadius;

/*
 *  设置一个圆角
 */
- (void)jw_setRoundedCorners:(UIRectCorner)corners
                      radius:(CGFloat)radius;

/**
 *  设置layer边框
 *
 *  @param width 边框宽度
 *  @param color 边框颜色
 */
- (void)jw_setBorderWidth:(CGFloat)width color:(nullable UIColor *)color;

/**
 设置view.layer的阴影
 
 @param color  阴影颜色
 @param offset 阴影offset
 @param radius 阴影radius
 */
- (void)jw_setLayerShadow:(nullable UIColor*)color
                   offset:(CGSize)offset
                   radius:(CGFloat)radius;


/**
 *
 *  找到第一响应者
 *  @return 活动中的View,如果找不到返回nil
 */
- (UIView *_Nullable)findFirstResponder;

/*
 周边加阴影，并且同时圆角
 */
- (void)addShadowToView:(UIView *)view
            withOpacity:(float)shadowOpacity
           shadowRadius:(CGFloat)shadowRadius
        andCornerRadius:(CGFloat)cornerRadius;

/*
 *  设置一个斜角
 */
- (void)jw_setCutCorners:(UIRectCorner)corners;


@end

NS_ASSUME_NONNULL_END
