//
//  UIView+noMoreDataView.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (noMoreDataView)


- (void)jw_showNoDataViewWithContent:(NSString *)content;

- (void)jw_dismissNoDataView;

- (void)jw_showNoDataViewWithContent:(NSString *)content andImage:(NSString *)imageName;

- (void)jw_showNoDataViewWithFrame:(CGRect)frame Content:(NSString *)content andImage:(NSString *)imageName;


@end

NS_ASSUME_NONNULL_END
