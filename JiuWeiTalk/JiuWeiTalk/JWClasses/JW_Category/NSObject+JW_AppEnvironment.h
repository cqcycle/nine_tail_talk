//
//  NSObject+JW_AppEnvironment.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//
#import <UIKit/UIKit.h>
///开发时候的开发环境和线上环境
typedef NS_ENUM(int,EnvironmentType){
    ///线上环境
    EnvironmentTypeOnLine  = 1,
    ///开发环境
    EnvironmentTypeDevelop = 2,
    ///uat环境
    EnvironmentTypeUat     = 3
   
};
@interface NSObject (JW_AppEnvironment)
/**
 * 1. 获取开发环境，获取的时候（AppConfig在info.plist中为字典，EnvironmentNum为该字典下的key）
 */
- (int)jw_getEnvironmentType;
/**
 * 2.根据对应结果 设置基本url
 */
+ (NSString *)jw_getBaseUrl;

@end

