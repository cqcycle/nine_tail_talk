//
//  UIButton+create.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//


NS_ASSUME_NONNULL_BEGIN

// 定义一个枚举（包含了四种类型的button）
typedef NS_ENUM(NSUInteger, YSButtonEdgeInsetsStyle) {
    YSButtonEdgeInsetsStyleTop, // image在上，label在下
    YSButtonEdgeInsetsStyleLeft, // image在左，label在右
    YSButtonEdgeInsetsStyleBottom, // image在下，label在上
    YSButtonEdgeInsetsStyleRight // image在右，label在左
};
@interface UIButton (create)

/**
 *  设置button的titleLabel和imageView的布局样式，及间距
 *
 *  @param style titleLabel和imageView的布局样式
 *  @param space titleLabel和imageView的间距
 */
- (void)jw_layoutButtonWithEdgeInsetsStyle:(YSButtonEdgeInsetsStyle)style
                           imageTitleSpace:(CGFloat)space;



/**
 添加按钮点击缩放效果
 */
- (void)jw_addAnimation;


/**
 按钮点击block
 */
typedef void (^ActionBlock)(void);
@property (readonly) NSMutableDictionary *event;
- (void)jw_handleControlEvent:(UIControlEvents)controlEvent withBlock:(ActionBlock)block;
- (void)jw_handleControlBlock:(ActionBlock)block;
- (void)jw_addAnimationAndHandleControlBlock:(ActionBlock)block;

/**
 文本button构造方法

 @param title 文本
 @param textcolor 文本颜色
 @param fontSize 文本字体
 @return button
 */
+ (instancetype)jw_buttonWithTextColor:(UIColor *)textcolor
                                  font:(CGFloat)fontSize
                                 title:(NSString *)title;

+ (instancetype)jw_buttonWithTextColor:(UIColor *)textcolor
                                  font:(CGFloat)fontSize
                                 title:(NSString *)title
                       backgroundColor:(UIColor *)backgroundColor;



/************************************ 响应区域 ***********************************************/

/**
 *  同时向按钮的四个方向延伸响应面积
 *
 *  @param size 间距
 */
- (void)jw_setEnlargeEdge:(CGFloat) size;

/**
 *  向按钮的四个方向延伸响应面积
 *
 *  @param top    上间距
 *  @param left   左间距
 *  @param bottom 下间距
 *  @param right  右间距
 */
- (void)jw_setEnlargeEdgeWithTop:(CGFloat) top left:(CGFloat) left bottom:(CGFloat) bottom right:(CGFloat) right;

/** 响应区域 */

@end

NS_ASSUME_NONNULL_END
