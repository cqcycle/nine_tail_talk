//
//  UITextField+create.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (create)




/**
 构造方法

 @param placeholder 占位文字
 @param color 文字颜色
 @param fontSize 字体
 @return 实例对象
 */
+ (instancetype)jw_textFieldWithTextColor:(UIColor *)color
                                 textFont:(CGFloat)fontSize
                              placeholder:(NSString *)placeholder;


- (void)setLeftViewWidth:(CGFloat)width;


/**
 设置占位文字的字体样式大小 和 颜色

 @param placeholder 占位文字
 @param color 颜色
 @param font 字体样式大小
 */
- (void)jw_setattributedPlaceholder:(NSString *)placeholder
                          textColor:(UIColor *)color
                           textFont:(UIFont *)font;


@end

NS_ASSUME_NONNULL_END
