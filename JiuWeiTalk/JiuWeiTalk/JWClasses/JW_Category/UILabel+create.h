//
//  UILabel+create.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//


NS_ASSUME_NONNULL_BEGIN

@interface UILabel (create)

/**
 label生成

 @param text 文本内容
 @param textColor 文本颜色
 @param textFont 文本字体
 @return label
 */
+ (UILabel *)jw_labelWithTextColor:(UIColor *)textColor
                          textFont:(CGFloat)textFont
                              text:(NSString *)text;


+ (UILabel *)jw_labelWithTextColor:(UIColor *)textColor
                      boldTextFont:(CGFloat)textFont
                              text:(NSString *)text;


@end

NS_ASSUME_NONNULL_END
