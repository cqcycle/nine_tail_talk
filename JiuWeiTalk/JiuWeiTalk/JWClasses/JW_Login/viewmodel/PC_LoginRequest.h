//
//  PC_LoginRequest.h
//  NineTails
//
//  Created by 程睿 on 2018/7/18.
//  Copyright © 2018年 dayou. All rights reserved.
//

#import <Foundation/Foundation.h>

// 发送短信验证码
#define MobileSmsSend                   KJWJoinString(@"app/sms-model/sendSms")
// 登录
#define MobileUserLogin                 KJWJoinString(@"app/sms-model/authSms")
// 发送短信验证码-旧手机号
#define AppMobileRecordModelOldSendSms  KJWJoinString(@"app/mobile-record-model/oldSendSms")
// 验证旧手机号
#define AppMobileRecordModelOldAuth     KJWJoinString(@"app/mobile-record-model/oldAuth")
// 发送短信验证码-新手机号
#define AppMobileRecordModelNewSendSms  KJWJoinString(@"app/mobile-record-model/newSendSms")
// 验证新手机号
#define AppMobileRecordModelNewAuth     KJWJoinString(@"app/mobile-record-model/newAuth")
// 填写用户名，生成用户
#define MobileUserGenerateUser          KJWJoinString(@"mobile/user/generateUser")
// 添加登录记录（不是验证码登录的时候需要调）
#define MobileUserAddLoginRecord        KJWJoinString(@"mobile/user/addLoginRecord")
// 获取协议
#define AppLoginGetAgreement            KJWJoinString(@"app/login/getAgreement")
// 微信登录
#define WeixinWeixinUserModelLogin      KJWJoinString(@"weixin/weixin-user-model/login")
// 记录用户打开关闭app
#define AppAppUsageModelAdd             KJWJoinString(@"app/app-usage-model/add")
// app阅读行为
#define AppAppReadModelAdd              KJWJoinString(@"app/app-read-model/add")

@interface PC_LoginRequest : NSObject

/**
 发送短信验证码
 */
+ (void)getMobileSmsSendParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure;

/**
 登录
 */
+ (void)getMobileUserLoginParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure;

/**
 填写用户名，生成用户
 */
+ (void)getMobileUserGenerateUserParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure;

/**
 添加登录记录（不是验证码登录的时候需要调）
 */
+ (void)getMobileUserAddLoginRecordSuccess:(SuccessBlock)success failure:(FailureBlock)failure;

/**
 获取协议
 */
+ (void)getAppLoginGetAgreementParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure;

/**
 微信登录
 */
+ (void)getWeixinWeixinUserModelLoginParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure;

/**
 发送短信验证码-旧手机号
 */
+ (void)getAppMobileRecordModelOldSendSmsParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure;

/**
 验证旧手机号
 */
+ (void)getAppMobileRecordModelOldAuthParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure;

/**
 发送短信验证码-新手机号
 */
+ (void)getAppMobileRecordModelNewSendSmsParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure;

/**
 验证新手机号
 */
+ (void)getAppMobileRecordModelNewAuthParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure;

/**
 微信登录
 */
+ (void)getAppAppUsageModelAddParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure;

/**
 app阅读行为
 */
+ (void)getAppAppReadModelAddParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure;

@end
