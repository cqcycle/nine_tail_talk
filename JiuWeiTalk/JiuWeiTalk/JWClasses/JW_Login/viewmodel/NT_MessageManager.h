//
//  NT_MessageManager.h
//  NineTails
//
//  Created by 杨钰棋 on 2019/6/13.
//  Copyright © 2019 dayou. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NT_MessageManager : NSObject

+ (instancetype)sharedInstance;

- (void)start;

- (void)disconnect;

- (void)writeData:(NSData *)data withTimeout:(NSInteger)timeout tag:(NSInteger)tag;

@end

NS_ASSUME_NONNULL_END
