//
//  PC_LoginRequest.m
//  NineTails
//
//  Created by 程睿 on 2018/7/18.
//  Copyright © 2018年 dayou. All rights reserved.
//

#import "PC_LoginRequest.h"

@implementation PC_LoginRequest

/**
 发送短信验证码
 */
+ (void)getMobileSmsSendParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure{
    [DYNetworkHelper POST:MobileSmsSend parameters:params success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 登录
 */
+ (void)getMobileUserLoginParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure{
    [DYNetworkHelper POST:MobileUserLogin parameters:params success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 发送短信验证码-旧手机号
 */
+ (void)getAppMobileRecordModelOldSendSmsParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure{
    [DYNetworkHelper POST:AppMobileRecordModelOldSendSms parameters:params success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 验证旧手机号
 */
+ (void)getAppMobileRecordModelOldAuthParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure{
    [DYNetworkHelper POST:AppMobileRecordModelOldAuth parameters:params success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 发送短信验证码-新手机号
 */
+ (void)getAppMobileRecordModelNewSendSmsParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure{
    [DYNetworkHelper POST:AppMobileRecordModelNewSendSms parameters:params success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 验证新手机号
 */
+ (void)getAppMobileRecordModelNewAuthParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure{
    [DYNetworkHelper POST:AppMobileRecordModelNewAuth parameters:params success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 填写用户名，生成用户
 */
+ (void)getMobileUserGenerateUserParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure{
    [DYNetworkHelper POST:MobileUserGenerateUser parameters:params success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 添加登录记录（不是验证码登录的时候需要调）
 */
+ (void)getMobileUserAddLoginRecordSuccess:(SuccessBlock)success failure:(FailureBlock)failure{
    [DYNetworkHelper POST:MobileUserAddLoginRecord parameters:@{} showFailureMsg:YES success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 获取协议
 */
+ (void)getAppLoginGetAgreementParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure{
    [DYNetworkHelper POST:AppLoginGetAgreement parameters:params success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 微信登录
 */
+ (void)getWeixinWeixinUserModelLoginParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure{
    [DYNetworkHelper POST:WeixinWeixinUserModelLogin parameters:params success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 记录用户打开关闭app
 */
+ (void)getAppAppUsageModelAddParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure{
    [DYNetworkHelper POST:AppAppUsageModelAdd parameters:params showFailureMsg:NO success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 app阅读行为
 */
+ (void)getAppAppReadModelAddParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure{
    [DYNetworkHelper POST:AppAppReadModelAdd parameters:params showFailureMsg:NO success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}



@end
