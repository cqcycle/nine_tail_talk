//
//  NT_MessageManager.m
//  NineTails
//
//  Created by 杨钰棋 on 2019/6/13.
//  Copyright © 2019 dayou. All rights reserved.
//

#import "NT_MessageManager.h"
#import <GCDAsyncSocket.h>
#import <UserNotifications/UserNotifications.h>
//#import "NT_MessageData.h"
//#import "EBBannerView.h"
//#import "FK_OrderDataViewController.h"
#import "WLAlertView.h"
//#import "NT_AlertViewOne.h"
//#import "NT_AlertViewTwo.h"
//#import "NT_AlertViewThree.h"
//#import "DXQAdView.h"
#import <JPUSHService.h>


//#import "NT_AlertServiceView.h"

static dispatch_once_t onceToken;

@interface NT_MessageManager ()<GCDAsyncSocketDelegate, UNUserNotificationCenterDelegate>{
    BOOL _shouldReconnect;
    NSInteger _messageCounts;
}
@property (nonatomic, strong) GCDAsyncSocket *clientSocket;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, copy) NSString *hostname;
@property (nonatomic, copy) NSString *port;
@property (strong, nonatomic) WLAlertView *wlAlertView;

@end

@implementation NT_MessageManager

+ (instancetype)sharedInstance
{
    
    static NT_MessageManager *model;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        model = [[NT_MessageManager alloc]init];
    });
    return model;
}

- (void)bannerDidClick:(NSNotification*)noti{
    //... = noti.object
//    NSDictionary *dict = [noti object];
    BOOL contain = NO;
    for (UIViewController *vc in [DYCommonTool topViewController].navigationController.viewControllers) {
        if ([vc.className isEqualToString:@"FK_OrderDataViewController"]) {
            contain = YES;
            break;
        }
    }
    
    if (!contain) {
//        FK_OrderDataViewController *vc = [[FK_OrderDataViewController alloc] init];
//        [[DYCommonTool topViewController].navigationController pushViewController:vc animated:YES];
    }
}

- (void)start
{
    if (self.clientSocket.isConnected) {
        return;
    }
    _shouldReconnect = YES;
    self.clientSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    @weakify(self)
    [self connectToServerCompletion:^(BOOL success) {
        @strongify(self)
        if (success) {
            
        }
        else {
            [self connectToServerCompletion:nil];
        }
    }];
}

- (void)connectToServerCompletion:(void(^)(BOOL success))completion{
    
//    [PC_HomeRequest getChatMessageAddressSuccess:^(id responseData) {
//        self.hostname = responseData[@"hostname"];
//        self.port = responseData[@"port"];
//        [self.clientSocket connectToHost:self.hostname onPort:[self.port intValue] error:nil];
//        if (completion) {
//            completion(YES);
//        }
//    } failure:^(NSError *error) {
//        if (completion) {
//            completion(NO);
//        }
//    }];
    
}

- (void)repeat
{
    NSData *data = [@"hello\n" dataUsingEncoding:NSUTF8StringEncoding];
    [self.clientSocket writeData:data withTimeout:-1 tag:0];
}

- (void)disconnect
{
    if (self.clientSocket.isConnected) {
        _shouldReconnect = NO;
        [self.clientSocket disconnect];
        [self removeTimer];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        onceToken = 0;
    }
}

- (void)writeData:(NSData *)data withTimeout:(NSInteger)timeout tag:(NSInteger)tag
{
    [self.clientSocket writeData:data withTimeout:timeout tag:tag];
}

- (void)removeTimer
{
    if ([self.timer isValid]) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)recordUserInfo
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"from"] = [JW_UserModel sharedInstance].userId;
    params[@"createTime"] = [DYCommonTool getCurrentTime];
    params[@"eventType"] = @"connect";
    params[@"channel"] = @"jiuweiplus";
    params[@"source"] = @"ios";
    params[@"msgType"] = @"event";
    NSString *msgToSend = [[params modelToJSONString] stringByAppendingString:@"\n"];
    NSLog(@"\n准备发送消息：%@", msgToSend);
    [self.clientSocket writeData:[msgToSend dataUsingEncoding:NSUTF8StringEncoding] withTimeout:-1 tag:0];
}


#pragma mark - GCDAsyncSocketDelegate
- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port{
    NSLog(@"连接成功！");
    
    [self removeTimer];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(repeat) userInfo:nil repeats:YES];
    dispatch_once(&onceToken, ^{
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bannerDidClick:) name:EBBannerViewDidClickNotification object:nil];
    });
    
    [self.clientSocket readDataWithTimeout:-1 tag:0];
    
    [self recordUserInfo];
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err{
    
    NSLog(@"连接断开！");
    if (_shouldReconnect) {
        if (kIsNetwork) {
            [self connectToServerCompletion:nil];
        }else{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self connectToServerCompletion:nil];
            });
        }
    }
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag{
    
//    NSLog(@"\n did write data, tag = %ld", tag);
    [self.clientSocket readDataWithTimeout:-1 tag:0];
}
- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag{
    
    NSString *msg = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"\n收到msg = %@, tag = %ld", msg, tag);
    
    [self.clientSocket readDataWithTimeout:-1 tag:tag];
    
    if ([msg containsString:@"}\n{"]) {
        NSMutableArray *array = [NSMutableArray arrayWithArray:[msg componentsSeparatedByString:@"}\n{"]];
        array[0] = [array[0] stringByAppendingString:@"}"];
        for (int i = 1; i < array.count - 1; i++) {
            array[i] = [NSString stringWithFormat:@"{%@}", array[i]];
        }
        array[array.count-1] = [@"{" stringByAppendingString:[array lastObject]];
        for (NSString *str in array) {
            [self continueWithStr:str];
        }
    }else{
        [self continueWithStr:msg];
    }
    
}

- (void)continueWithStr:(NSString *)str
{
//    NT_MessageData *data = [NT_MessageData modelWithJSON:str];
//    if (!data) {
//        if ([str containsString:@"账号异常"]) {
//            [DYHUDView showOnlyTextHUD:@"当前账号已在其他设备登录"];
//            [BaseUserModel cleanCache];
//            [BaseUserModel cleanSharedInstance];
//            //注销极光别名
//            [JPUSHService setAlias:@"" completion:nil seq:0];
//            [self disconnect];
//        }else{
//            [DYHUDView showOnlyTextHUD:str];
//        }
//        return;
//    }
//    if ([data.msgType isEqualToString:@"window"]) {
//        if (self.wlAlertView) {
//            return;
//        }
//
//        if ([data.content[@"type"] integerValue] == 1) {
//            NT_AlertViewOne *alertView = [NT_AlertViewOne alertViewOne];
//            NSData *jsonData = [str dataUsingEncoding:NSUTF8StringEncoding];
//            alertView.dict = [NSJSONSerialization JSONObjectWithData:jsonData
//                                                                options:NSJSONReadingMutableContainers
//                                                                  error:nil][@"content"][@"data"];
//            alertView.block = ^{
//                [self.wlAlertView hide];
//                self.wlAlertView = nil;
//            };
//            self.wlAlertView = [[WLAlertView alloc] initWithCustomView:alertView isShowControlsButton:NO delegate:nil buttonTitles:nil];
//            [self.wlAlertView show:WLAnimationDefault];
//        }else if ([data.content[@"type"] integerValue] == 2){
//            NT_AlertViewThree *alertView = [NT_AlertViewThree alertViewThree];
//            NSData *jsonData = [str dataUsingEncoding:NSUTF8StringEncoding];
//            alertView.dict = [NSJSONSerialization JSONObjectWithData:jsonData
//                                                             options:NSJSONReadingMutableContainers
//                                                               error:nil][@"content"][@"data"];
//            alertView.block = ^{
//                [self.wlAlertView hide];
//                self.wlAlertView = nil;
//            };
//            self.wlAlertView = [[WLAlertView alloc] initWithCustomView:alertView isShowControlsButton:NO delegate:nil buttonTitles:nil];
//            [self.wlAlertView show:WLAnimationDefault];
//        }else if ([data.content[@"type"] integerValue] == 3){
//            NT_AlertViewTwo *alertView = [NT_AlertViewTwo alertViewTwo];
//            NSData *jsonData = [str dataUsingEncoding:NSUTF8StringEncoding];
//            alertView.dict = [NSJSONSerialization JSONObjectWithData:jsonData
//                                                             options:NSJSONReadingMutableContainers
//                                                               error:nil][@"content"][@"data"];
//            alertView.block = ^{
//                [self.wlAlertView hide];
//                self.wlAlertView = nil;
//            };
//            self.wlAlertView = [[WLAlertView alloc] initWithCustomView:alertView isShowControlsButton:NO delegate:nil buttonTitles:nil];
//            [self.wlAlertView show:WLAnimationDefault];
//        }else if ([data.content[@"type"] integerValue] == 4){
//            DXQAdView *alertView = [DXQAdView dxqAdView];
//            [alertView.adImage sd_setImageWithURL:[NSURL URLWithString:data.content[@"data"][@"title"]]];
//            @weakify(self)
//            [alertView.adImage bk_whenTapped:^{
//                @strongify(self)
//                WebViewController *web = [[WebViewController alloc] initWithTitle:@"99会员中心" webUrl:data.content[@"data"][@"rank"]];
//                web.isVipCard = YES;
//                [[DYCommonTool topViewController].navigationController pushViewController:web animated:YES];
//                [self.wlAlertView hide];
//                self.wlAlertView = nil;
//            }];
//            [alertView.closeButton bk_whenTapped:^{
//                @strongify(self)
//                [self.wlAlertView hide];
//                self.wlAlertView = nil;
//            }];
//            self.wlAlertView = [[WLAlertView alloc] initWithCustomView:alertView isShowControlsButton:NO delegate:nil buttonTitles:nil];
//            [self.wlAlertView show:WLAnimationDefault];
//
//        }else if ([data.content[@"type"] integerValue] == 5){
//
//            NT_AlertServiceView *serviceView = [[NT_AlertServiceView alloc] initWithFrame:CGRectMake(30, 0, SCREEN_WIDTH - 60, 381)];
//            [serviceView.bgImageView sd_setImageWithURL:[NSURL URLWithString:data.content[@"data"][@"title"]]];
//
//            @weakify(self)
//            serviceView.block = ^{
//
//                @strongify(self)
//                [self.wlAlertView hide];
//                self.wlAlertView = nil;
//            };
//
//            [serviceView.bgImageView bk_whenTapped:^{
//
//                @strongify(self)
//                FK_OrderDataViewController *orderVC = [[FK_OrderDataViewController alloc] init];
//                [[DYCommonTool topViewController].navigationController pushViewController:orderVC animated:YES];
//
//                [self.wlAlertView hide];
//                self.wlAlertView = nil;
//            }];
//
//            self.wlAlertView = [[WLAlertView alloc] initWithCustomView:serviceView isShowControlsButton:NO delegate:nil buttonTitles:nil];
//            [self.wlAlertView show:WLAnimationDefault];
//
//        }
//        return;
//    }
//    if ([[DYCommonTool topViewController].className isEqualToString:@"FK_OrderDataViewController"]) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:KSocketInfoNotification object:@{@"messageStr":str}];
//    }else{
//        kUserDef
//        NSMutableArray *array = [NSMutableArray arrayWithArray:[ud objectForKey:KTempNotReadArray]];
//        if (!array) {
//            array = [NSMutableArray array];
//        }
//        [array addObject:str];
//        [ud setObject:array forKey:KTempNotReadArray];
//
//
//        //        if ([data.content[@"content"] containsString:@"请问您想要咨询什么问题"]) {
//        //            return;
//        //        }
//        [BaseUserModel sharedInstance].redPoint = YES;
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShouldShowLocalNotification" object:nil];
//        EBBannerView *banner = [EBBannerView bannerWithBlock:^(EBBannerViewMaker *make) {
//            make.style = 13;
//            //            make.icon = [UIImage imageNamed:@"icon"];
//            //            make.title = @"custom title";
//            if ([data.msgType isEqualToString:@"text"]) {
//                make.content = data.content[@"content"];
//            }else if ([data.msgType isEqualToString:@"image"]) {
//                make.content = @"[图片]";
//            }else if ([data.msgType isEqualToString:@"voice"]) {
//                make.content = @"[语音]";
//            }else if ([data.msgType isEqualToString:@"card"]) {
//                make.content = @"[链接]";
//            }
//        }];
//        if (![[DYCommonTool topViewController].className isEqualToString:@"NT_MessageViewController"]) {
//            [banner show];
//        }
//    }
}

@end
