//
//  PC_LoginViewController.h
//  NineTails
//
//  Created by 程睿 on 2018/7/12.
//  Copyright © 2018年 dayou. All rights reserved.
//


@interface PC_LoginViewController : UIViewController
@property (nonatomic, copy) NSString *openId;
@property (nonatomic, copy) NSString *unionId;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, assign) BOOL isFirst;
@property (nonatomic, assign) BOOL isPad;

@end
