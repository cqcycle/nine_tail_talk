//
//  PC_LoginViewController.m
//  NineTails
//
//  Created by 程睿 on 2018/7/12.
//  Copyright © 2018年 dayou. All rights reserved.
//

#import "PC_LoginViewController.h"
#import "PC_LoginRequest.h"
#import "JW_HomeViewController.h"
#import "JKCountDownButton.h"
#import "NSString+JWAddForRegex.h"
#import "JPUSHService.h"
#import "WebViewController.h"
#import "NT_LoginChangeSuccessViewController.h"
//#import "PC_FeedbackViewController.h"
#import "CYLTabBarControllerConfig.h"
#import "MLHealthManager.h"
#import "NT_MessageManager.h"
//#import "NT_UserTagViewController.h"

@interface PC_LoginViewController () <UITextFieldDelegate> {
    BOOL _phoneNumValid;
    BOOL _vcodeValid;
}
@property (weak, nonatomic) IBOutlet UITextField *phoneNumTF;
@property (weak, nonatomic) IBOutlet UITextField *vcodeTF;
@property (weak, nonatomic) IBOutlet JKCountDownButton *vcodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UILabel *reportLabel;
@property (weak, nonatomic) IBOutlet UILabel *pageLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneDescLabel;
@property (nonatomic, copy) NSString *correctCode;
@property (nonatomic, copy) NSString *agreementUrl;

@end

@implementation PC_LoginViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!self.type) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.type == 0) {
        [DYCommonTool appUserTrackDataPage:@"004" event:@"004000"];
    }
    
    [self setupUI];
    
    [self setupActions];
    
}

- (void)setupActions {
    @weakify(self)
    [_vcodeBtn addToucheHandler:^(JKCountDownButton *countDownButton, NSInteger tag) {
        @strongify(self)
        [countDownButton didChange:^NSString *(JKCountDownButton *countDownButton,int second) {
            NSString *title = [NSString stringWithFormat:@"(%ds)重发",second];
//            [self.vcodeBtn setTitleColor:[UIColor colorWithHexString:@"939393"] forState:UIControlStateNormal];
            [self.vcodeBtn setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"939393"]] forState:UIControlStateNormal];
            return title;
        }];
        [countDownButton didFinished:^NSString *(JKCountDownButton *countDownButton, int second) {
            countDownButton.enabled = NO;
            [self enableVcodeBtn];
//            [self.vcodeBtn setTitleColor:[UIColor colorWithHexString:@"FFB243"] forState:UIControlStateNormal];
            [self.vcodeBtn setBackgroundImage:[UIImage imageNamed:@"btn_fasongyanzhengma"] forState:UIControlStateNormal];
            return @"获取验证码";
        }];
        
        BOOL isOK = [self sendSmsVcode:countDownButton];
        if (!isOK) return;
        [self.vcodeTF becomeFirstResponder];
        [self disableVcodeBtn];
        [countDownButton startWithSecond:40];
        self.vcodeTF.text = nil;
        
    }];
    [_phoneNumTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_vcodeTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _phoneNumTF.delegate = self;
}

- (void)setupUI {
    [self.vcodeBtn jw_setCornerRadius:21];
    [self disableVcodeBtn];
    [self disableConfirmBtn];
    
    if (self.type == 1) {
        self.reportLabel.hidden = NO;
        [self.reportLabel bk_whenTapped:^{
            //feendback
//            PC_FeedbackViewController *vc = [[PC_FeedbackViewController alloc] init];
//            vc.selectedItem = 5;
//            vc.issueStr = @"【手机号码变更相关】";
//            [self.navigationController pushViewController:vc animated:YES];
        }];
        self.pageLabel.hidden = NO;
        self.pageLabel.text = @"(1/2)";
        self.phoneNumTF.placeholder = @"请输入原手机号";
        [self.confirmBtn setTitle:@"下一步" forState:UIControlStateNormal];
        self.phoneDescLabel.text = @"更换手机号";
    }else if (self.type == 2){
        self.pageLabel.hidden = NO;
        self.pageLabel.text = @"(2/2)";
        self.phoneNumTF.placeholder = @"请输入新手机号";
        [self.confirmBtn setTitle:@"确定" forState:UIControlStateNormal];
        self.phoneDescLabel.text = @"更换手机号";
    }else if (self.isPad){
        self.phoneDescLabel.text = @"手机登录";
    }
    
}

- (IBAction)confirmAction:(UIButton *)sender {
    if (!_phoneNumValid) {
        [DYHUDView showOnlyTextHUD:@"请输入正确的手机号码"];
        return;
    }
    if (!_vcodeValid) {
        [DYHUDView showOnlyTextHUD:@"请输入正确的手机验证码"];
        return;
    }
    
    if (self.isPad) {
        if ([self.phoneNumTF.text isEqualToString:@"15757118010"] && [self.vcodeTF.text isEqualToString:@"0000"]) {
            [DYHUDView showDefaultIndicatorView];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [JW_UserModel sharedInstance].userId = @"JP582567595732979712";
                [JW_UserModel sharedInstance].imgUrl = @"https://dayou-picture.oss-cn-shanghai.aliyuncs.com/1/t5251653177200062977.jpg";
                [JW_UserModel sharedInstance].userName = @"面条";
                [JW_UserModel sharedInstance].mobile = @"15757118010";
                [JW_UserModel sharedInstance].unionId = @"o9Uxu0k3IL1kgoPdC2ZM8NUvJ1mE";
                [DYHUDView showSuccessHUD:@"登录成功"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"loginSuccessNotification" object:nil];
                [JW_UserModel saveDataToCache];
                [DYCommonTool restoreRootViewController:[[CYLTabBarControllerConfig alloc] init].tabBarController];
                [[NT_MessageManager sharedInstance] start];
            });
        }
        return;
    }
    
    if (_phoneNumValid && _vcodeValid) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        params[@"phoneNumber"] = self.phoneNumTF.text;
        params[@"smsCode"] = self.vcodeTF.text;
        params[@"openId"] = self.openId;
        if (self.type == 0) {
            [DYCommonTool appUserTrackDataPage:@"004" event:@"004001"];
            [PC_LoginRequest getMobileUserLoginParams:params success:^(id responseData) {
                [JW_UserModel sharedInstance].mobile = params[@"phoneNumber"];
                [self loginWithData:responseData];
            } failure:^(NSError *error) {
            }];
        }else if (self.type == 1){
            [PC_LoginRequest getAppMobileRecordModelOldAuthParams:params success:^(id responseData) {
                PC_LoginViewController *vc = [[PC_LoginViewController alloc] init];
                vc.type = 2;
                [self.navigationController pushViewController:vc animated:YES];
            } failure:^(NSError *error) {
            }];
        }else if (self.type == 2){
            params[@"oldPhoneNumber"] = [JW_UserModel sharedInstance].mobile;
            params[@"newPhoneNumber"] = self.phoneNumTF.text;
            [PC_LoginRequest getAppMobileRecordModelNewAuthParams:params success:^(id responseData) {
                [JW_UserModel sharedInstance].mobile = self.phoneNumTF.text;
                NT_LoginChangeSuccessViewController *vc = [[NT_LoginChangeSuccessViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            } failure:^(NSError *error) {
            }];
        }
    }
    
}

- (void)loginWithData:(id)responseData
{
    [JW_UserModel sharedInstance].userId = responseData[@"userNum"];
    [JW_UserModel sharedInstance].imgUrl = responseData[@"headImgUrl"];
    [JW_UserModel sharedInstance].userName = responseData[@"nickname"];
    [JW_UserModel sharedInstance].unionId = self.unionId;
    //注册极光别名
    [JPUSHService setAlias:[JW_UserModel sharedInstance].userId completion:nil seq:0];
    [DYHUDView showSuccessHUD:@"登录成功"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loginSuccessNotification" object:nil];
    [JW_UserModel saveDataToCache];
    
    kJWUserDefManager
    if ([ud boolForKey:@"hasAlertHealth"]) {
        [[MLHealthManager sharedInstance] getIphoneHealthDataCompletion:^(BOOL success) {
            if (!success) {
                [DYHUDView showErrorHUD:@"授权失败"];
            }
        } withAlwaysAlert:NO withController:self];
    }
    [[NT_MessageManager sharedInstance] start];
    if (self.isFirst) {
        [DYCommonTool restoreRootViewController:[[CYLTabBarControllerConfig alloc] init].tabBarController];
    }else {
        [self.navigationController.viewControllers[0] dismissViewControllerAnimated:YES completion:nil];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    
//        NT_UserTagViewController *userTagVC = [[NT_UserTagViewController alloc] init];
//        userTagVC.modalPresentationStyle = UIModalPresentationFullScreen;
//        [[DYCommonTool topViewController] presentViewController:userTagVC animated:YES completion:nil];
    });
}

- (BOOL)sendSmsVcode:(UIButton *)btn{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"phoneNumber"] = self.phoneNumTF.text;
    params[@"openId"] = self.openId;
    if (self.type == 0) {
        [PC_LoginRequest getMobileSmsSendParams:params success:^(id responseData) {
            [DYHUDView showOnlyTextHUD:responseData];
        } failure:^(NSError *error) {
        }];
    }else if (self.type == 1){
        [PC_LoginRequest getAppMobileRecordModelOldSendSmsParams:params success:^(id responseData) {
            [DYHUDView showOnlyTextHUD:responseData];
        } failure:^(NSError *error) {
        }];
    }else if (self.type == 2){
        [PC_LoginRequest getAppMobileRecordModelNewSendSmsParams:params success:^(id responseData) {
            [DYHUDView showOnlyTextHUD:responseData];
        } failure:^(NSError *error) {
        }];
    }
    
    return YES;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)textFieldDidChange:(UITextField *)textField{
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (textField.tag == 1) {
        // phoneNum
        if ([textField.text isValidPhoneNumber]) {
            [self enableVcodeBtn];
            _phoneNumValid = YES;
            [self enableConfirmBtn];
            [textField resignFirstResponder];
        }else {
            [self disableVcodeBtn];
            _phoneNumValid = NO;
            [self disableConfirmBtn];
        }
    }else if (textField.tag == 2) {
        // vcode
        if (textField.text.length == 4) {
            [textField resignFirstResponder];
            _vcodeValid = YES;
            [self enableConfirmBtn];
        }else {
            _vcodeValid = NO;
            [self disableConfirmBtn];
        }
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.tag==1) {
//        [DYCommonTool appUserTrackDataPage:@"001" event:@"000001"];
    }else if (textField.tag == 2) {
//        [DYCommonTool appUserTrackDataPage:@"001" event:@"000005"];
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag==1) {
//        [DYCommonTool appUserTrackDataPage:@"001" event:@"000003"];
    }else if (textField.tag == 2) {
//        [DYCommonTool appUserTrackDataPage:@"001" event:@"000007"];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length >= 11 && string.length != 0) {
        return NO;
    }
    return YES;
}

- (void)enableVcodeBtn{
    if (!_vcodeBtn.isEnabled) {
        _vcodeBtn.enabled = YES;
//        [_vcodeBtn dxq_setBorderWidth:1.f color:mainColor];
    }
}
- (void)disableVcodeBtn{
    if (_vcodeBtn.isEnabled) {
        _vcodeBtn.enabled = NO;
//        [_vcodeBtn dxq_setBorderWidth:1.f color:HexRGB(0x958b99)];
    }
}
- (void)enableConfirmBtn{
    if (!_confirmBtn.isEnabled) {
        if (_phoneNumValid && _vcodeValid) {
            _confirmBtn.enabled = YES;
//            [_confirmBtn setBackgroundColor:[UIColor colorWithHexString:@"262626"]];
        }
    }
}
- (void)disableConfirmBtn{
    if (_confirmBtn.isEnabled) {
        _confirmBtn.enabled = NO;
//        [_confirmBtn setBackgroundColor:[UIColor colorWithHexString:@"C8C8C8"]];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeBtnClick:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

@end
