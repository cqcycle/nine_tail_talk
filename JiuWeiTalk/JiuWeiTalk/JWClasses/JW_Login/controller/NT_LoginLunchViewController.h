//
//  NT_LoginLunchViewController.h
//  NineTails
//
//  Created by 杨钰棋 on 2019/5/20.
//  Copyright © 2019 dayou. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NT_LoginLunchViewController : UIViewController
@property (nonatomic, assign) BOOL isFirst;
@property (nonatomic, assign) BOOL isPad;

@end

NS_ASSUME_NONNULL_END
