//
//  NT_LoginLunchViewController.m
//  NineTails
//
//  Created by 杨钰棋 on 2019/5/20.
//  Copyright © 2019 dayou. All rights reserved.
//

#import "NT_LoginLunchViewController.h"
#import <UMSocialCore/UMSocialCore.h>
#import "PC_LoginViewController.h"
#import "JPUSHService.h"
#import "CYLTabBarControllerConfig.h"
#import "MLHealthManager.h"
#import "NT_MessageManager.h"
#import "PC_LoginRequest.h"

@interface NT_LoginLunchViewController ()
@property (weak, nonatomic) IBOutlet UIButton *weixinBtn;
@property (weak, nonatomic) IBOutlet UIButton *lookBtn;
@property (weak, nonatomic) IBOutlet UIButton *readBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnCons;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoTopSpacing;

@end

@implementation NT_LoginLunchViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_weixinBtn jw_setCornerRadius:22];
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.logoTopSpacing.constant = 116;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
    
    kJWUserDefManager
    if (![ud boolForKey:@"isFirst"]) {
        [ud setBool:YES forKey:@"isFirst"];
        self.isFirst = YES;
        self.btnCons.constant = 60;
        self.lookBtn.hidden = NO;
        self.closeBtn.hidden = YES;
        [DYCommonTool appUserTrackDataPage:@"002" event:@"002000"];
    }else{
        [DYCommonTool appUserTrackDataPage:@"003" event:@"003000"];
    }
    
    if (self.isPad) {
        [self.weixinBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [self.weixinBtn setTitle:@"登录" forState:UIControlStateNormal];
    }
    
}

- (IBAction)readBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (IBAction)weixinBtnClick:(UIButton *)sender {
    if (!self.readBtn.selected) {
        [DYHUDView showOnlyTextHUD:@"请先同意软件服务协议及隐私政策"];
        return;
    }
    
    if (self.isFirst) {
        [DYCommonTool appUserTrackDataPage:@"002" event:@"002001"];
    }else{
        [DYCommonTool appUserTrackDataPage:@"003" event:@"003001"];
    }
    
    if (self.isPad) {
        PC_LoginViewController *vc = [[PC_LoginViewController alloc] init];
        vc.isPad = self.isPad;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:nil completion:^(id result, NSError *error) {
        if (error) {
            [DYHUDView showErrorHUD:@"微信授权失败"];
        } else {
            
            UMSocialUserInfoResponse *resp = result;
            //
            //                // 授权信息
            //                NSLog(@"Wechat uid: %@", resp.uid);
            //                NSLog(@"Wechat openid: %@", resp.openid);
            //                NSLog(@"Wechat unionid: %@", resp.unionId);
            //                NSLog(@"Wechat accessToken: %@", resp.accessToken);
            //                NSLog(@"Wechat refreshToken: %@", resp.refreshToken);
            //                NSLog(@"Wechat expiration: %@", resp.expiration);
            //
            //                // 用户信息
            //                NSLog(@"Wechat name: %@", resp.name);
            //                NSLog(@"Wechat iconurl: %@", resp.iconurl);
            //                NSLog(@"Wechat gender: %@", resp.unionGender);
            //
            //                // 第三方平台SDK源数据
            //                NSLog(@"Wechat originalResponse: %@", resp.originalResponse);
            
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            param[@"openId"] = resp.openid;
            param[@"nickname"] = resp.name;
            param[@"headImgUrl"] = resp.iconurl;
            param[@"sex"] = resp.originalResponse[@"sex"];
            param[@"country"] = resp.originalResponse[@"country"];
            param[@"province"] = resp.originalResponse[@"province"];
            param[@"city"] = resp.originalResponse[@"city"];
            param[@"language"] = resp.originalResponse[@"language"];
            param[@"unionId"] = resp.originalResponse[@"unionid"];
            
            [PC_LoginRequest getWeixinWeixinUserModelLoginParams:param success:^(id json) {
                
                
                if ([[json[@"hasMobile"] stringValue] isEqualToString:@"0"]) {
                    [DYHUDView showSuccessHUD:@"授权成功"];
                    PC_LoginViewController *vc = [[PC_LoginViewController alloc] init];
                    vc.openId = resp.openid;
                    vc.unionId = resp.originalResponse[@"unionid"];
                    vc.isFirst = self.isFirst;
                    [self.navigationController pushViewController:vc animated:YES];
                    
                    
                }else{
                    
                    
                    [JW_UserModel sharedInstance].userId = json[@"userNum"];
                    [JW_UserModel sharedInstance].imgUrl = json[@"headImgUrl"];
                    [JW_UserModel sharedInstance].userName = json[@"nickname"];
                    [JW_UserModel sharedInstance].mobile = json[@"mobile"];
                    [JW_UserModel sharedInstance].unionId = resp.originalResponse[@"unionid"];
                    [JW_UserModel saveDataToCache];
                    //注册极光别名
                    [JPUSHService setAlias:[JW_UserModel sharedInstance].userId completion:nil seq:0];
                    [DYHUDView showSuccessHUD:@"登录成功"];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"loginSuccessNotification" object:nil];
                    kJWUserDefManager
                    if ([ud boolForKey:@"hasAlertHealth"]) {
                        [[MLHealthManager sharedInstance] getIphoneHealthDataCompletion:^(BOOL success) {
                            if (!success) {
                                [DYHUDView showErrorHUD:@"授权失败"];
                            }
                        } withAlwaysAlert:NO withController:self];
                    }
                    [[NT_MessageManager sharedInstance] start];
                    if (self.isFirst) {
                        [DYCommonTool restoreRootViewController:[[CYLTabBarControllerConfig alloc] init].tabBarController];
                    }else {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }
                }

            } failure:^(NSError *error) {
                
                
                
            }];
        }
    }];
}

- (IBAction)lookBtnClick:(UIButton *)sender {
    if (self.isFirst) {
        [DYCommonTool appUserTrackDataPage:@"002" event:@"002002"];
    }
    self.isFirst?[DYCommonTool restoreRootViewController:[[CYLTabBarControllerConfig alloc] init].tabBarController]:[self dismissViewControllerAnimated:YES completion:nil];;
}

- (IBAction)servceBtnClick:(UIButton *)sender {
    WebViewController *web = [[WebViewController alloc]initWithTitle:@"用户服务协议&隐私政策" webUrl:kAgreementUrl];
    web.cancelDismiss = YES;
    [self.navigationController pushViewController:web animated:YES];
}

- (IBAction)closeBtnClick:(UIButton *)sender {
    [DYCommonTool appUserTrackDataPage:@"003" event:@"003002"];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
