//
//  NT_LoginChangeSuccessViewController.h
//  NineTails
//
//  Created by 杨钰棋 on 2019/5/27.
//  Copyright © 2019 dayou. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NT_LoginChangeSuccessViewController : UIViewController
@property (nonatomic, assign) BOOL isFeedBack;

@end

NS_ASSUME_NONNULL_END
