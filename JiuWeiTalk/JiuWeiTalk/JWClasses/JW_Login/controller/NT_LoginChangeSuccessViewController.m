//
//  NT_LoginChangeSuccessViewController.m
//  NineTails
//
//  Created by 杨钰棋 on 2019/5/27.
//  Copyright © 2019 dayou. All rights reserved.
//

#import "NT_LoginChangeSuccessViewController.h"

@interface NT_LoginChangeSuccessViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation NT_LoginChangeSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.isFeedBack) {
        _imgView.image = [UIImage imageNamed:@"img_fankuichenggong"];
        _titleLabel.text = @"反馈成功";
        self.navigationItem.title = @"意见反馈";
    }else{
        self.navigationItem.title = @"更换成功";
    }
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:@selector(no)];
    
}

- (void)no
{
    
}

- (IBAction)backBtnClick:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
