//
//  NT_HomeCycleModel.h
//  NineTails
//
//  Created by 亚刘 on 2019/10/31.
//  Copyright © 2019 dayou. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NT_HomeCycleModel : NSObject

/** 标题 */
@property(nonatomic, strong) NSString *title;

/**  内容分类 (1 cps文章 3 音频 5 app文章) */
@property(nonatomic, strong) NSString *type;

/** 相关主键 */
@property(nonatomic, copy) NSString *otherId;

/**  观看数量 */
@property(nonatomic, copy) NSString *viewCount;

/** 标签 */
@property(nonatomic, copy) NSString *gradeStr;

/**  图片地址 */
@property(nonatomic, copy) NSString *indexPicUrl;

/**  相关地址 */
@property(nonatomic, copy) NSString *otherUrl;

/**  时长 */
@property(nonatomic, copy) NSString *duration;

/** 合集主键 */
@property(nonatomic, copy) NSString *albumId;

/** 时间 */
@property(nonatomic, copy) NSString *createTime;


@end

NS_ASSUME_NONNULL_END
