//
//  JW_CommonModel.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/6.
//  Copyright © 2019 jiuwei. All rights reserved.
//  评论数据模型

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JW_CommonModel : NSObject

@property (nonatomic, strong) NSString * adUrl;
@property (nonatomic, strong) NSString * articleId;
@property (nonatomic, assign) BOOL isAd;
@property (nonatomic, strong) NSArray * picUrl;
@property (nonatomic, strong) NSString * readTimes;
@property (nonatomic, assign) NSInteger showPicType;
@property (nonatomic, strong) NSString * time;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, assign) BOOL isTopRank;
@property (nonatomic, assign) BOOL isRead;
@property (nonatomic, strong) NSArray * readArr;
@property (nonatomic, assign) BOOL isRelated;
@property (nonatomic, copy) NSString *author;
@property (nonatomic, copy) NSString *digest;
@property (nonatomic, copy) NSString *id;
/*
 adUrl = "";
 articleId = 78;
 author = "\U4e5d\U5c3e\U541b";
 isAd = 0;
 picUrl =     (
     "https://dayou-picture.oss-cn-shanghai.aliyuncs.com/4/w8333409463148756996.jpg"
 );
 readTimes = 7392;
 showPicType = 1;
 time = "05-29 10:04";
 title = "\U652f\U4ed8\U5b9d\U3001\U5fae\U4fe1\U3001\U4eac\U4e1c\U3001\U6c34\U6ef4\U7b79\U90fd\U6709\U767e\U4e07\U533b\U7597\Uff0c\U54ea\U4e00\U5bb6\U7684\U6700\U4f18\U79c0\Uff1f";
 
 */
@end

NS_ASSUME_NONNULL_END
