//
//  JW_ArticleDetailModel.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/6.
//  Copyright © 2019 jiuwei. All rights reserved.
//  文章详情数据模型

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JW_ArticleDetailModel : NSObject

@property (nonatomic, strong) NSString * author;
@property (nonatomic, assign) BOOL collect;
@property (nonatomic, strong) NSString * commentNum;
@property (nonatomic, strong) NSString * detail;
@property (nonatomic, assign) BOOL subscribe;
@property (nonatomic, strong) NSString * time;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, copy) NSString *downloadUrl;


@end

NS_ASSUME_NONNULL_END
