//
//  JW_HomeSearchCell.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/5.
//  Copyright © 2019 jiuwei. All rights reserved.
//

#import "JW_HomeSearchCell.h"
@interface JW_HomeSearchCell()
@property (weak, nonatomic) IBOutlet UILabel *searchTitleLabel;

@end
@implementation JW_HomeSearchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    @weakify(self);
    [RACObserve(self, dataString) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if (self.dataString.length == 0)return;
        self.searchTitleLabel.text = self.dataString;
    }];
}

@end
