//
//  JW_HomeSearchRecordCell.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/6.
//  Copyright © 2019 jiuwei. All rights reserved.
//  搜索记录的cell

#import "JW_BaseTableViewCell.h"


NS_ASSUME_NONNULL_BEGIN

@interface JW_HomeSearchRecordCell : JW_BaseTableViewCell
///数据
@property (nonatomic, copy) NSString *dataString;

///当前indexPath
@property (nonatomic, strong) NSIndexPath *currentRowIndexPath;

///点击删除的信号
@property (nonatomic, strong) RACSignal *deleteSignal;


@end

NS_ASSUME_NONNULL_END
