//
//  JW_HomeSearchCell.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/5.
//  Copyright © 2019 jiuwei. All rights reserved.
//  搜索列表的cell

#import "JW_BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface JW_HomeSearchCell : JW_BaseTableViewCell
///数据
@property (nonatomic, copy) NSString *dataString;
@end

NS_ASSUME_NONNULL_END
