//
//  JW_HomeCell.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/5.
//  Copyright © 2019 apple. All rights reserved.
//

#import "JW_BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface JW_HomeCell : JW_BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end

NS_ASSUME_NONNULL_END
