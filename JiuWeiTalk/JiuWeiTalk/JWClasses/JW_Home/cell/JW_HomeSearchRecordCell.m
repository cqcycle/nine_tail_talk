//
//  JW_HomeSearchRecordCell.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/6.
//  Copyright © 2019 jiuwei. All rights reserved.
//

#import "JW_HomeSearchRecordCell.h"

@interface JW_HomeSearchRecordCell()

@property (weak, nonatomic) IBOutlet UILabel *searchRecordTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *searchRecordLeftImageView;
@property (weak, nonatomic) IBOutlet UIButton *searchRecordDeleteButton;

@end

@implementation JW_HomeSearchRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    @weakify(self);
    ///删除按钮
    self.deleteSignal = [self.searchRecordDeleteButton rac_signalForControlEvents:UIControlEventTouchUpInside];
   ///监听数据,UITableViewCell和UICollectionViewCell复用使用RAC的问题,解决复用cell中信号的办法就是在cell里面创建的信号加上takeUntil:cell.rac_prepareForReuseSignal来让cell在每次重用的时候都去disposable创建的信号(解绑信号)
    [RACObserve(self, dataString) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if (self.dataString.length == 0)return;
        self.searchRecordTitleLabel.text = self.dataString;
    }];
}

@end
