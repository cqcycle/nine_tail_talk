//
//  JW_SearchFieldView.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/5.
//  Copyright © 2019 jiuwei. All rights reserved.
//  搜索文本框

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JW_SearchFieldView : UITextField
- (instancetype)initWithSearchFieldTitle:(NSString *)title frame:(CGRect)frame;
- (instancetype)initWithSearchFieldTitle:(NSString *)title frame:(CGRect)frame textBlock:(void(^)(NSString *text))block;
@end

NS_ASSUME_NONNULL_END
