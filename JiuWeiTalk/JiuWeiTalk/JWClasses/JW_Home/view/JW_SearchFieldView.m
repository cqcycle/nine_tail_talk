//
//  JW_SearchFieldView.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/5.
//  Copyright © 2019 jiuwei. All rights reserved.
//

#import "JW_SearchFieldView.h"

@implementation JW_SearchFieldView

- (instancetype)initWithSearchFieldTitle:(NSString *)title frame:(CGRect)frame {
    if (self = [super init]) {
        self.frame = frame;
        self.backgroundColor = [UIColor colorWithHexString:@"#EEEEEE"];
        self.clearButtonMode = UITextFieldViewModeWhileEditing;
        //搜索图标
        UIView *leftSearchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, 16)];
        leftSearchView.backgroundColor = [UIColor clearColor];
        UIImageView *searchImgView = [[UIImageView alloc]initWithFrame:CGRectMake(16, 0, 17, 16)];
        searchImgView.image = KJWImageNamed(kSearchImageName);
        searchImgView.contentMode = UIViewContentModeScaleAspectFit;
        [leftSearchView addSubview:searchImgView];
        self.leftView = leftSearchView;
        //文本
        NSMutableAttributedString *placeHolderAttr = [[NSMutableAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName : [UIColor colorWithHexString:@"#9B9B9B"],NSFontAttributeName : KJWFont(14.f)}];
        self.attributedPlaceholder = placeHolderAttr;
        self.textColor = [UIColor colorWithHexString:@"#262626"];
        [self jw_setCornerRadius:frame.size.height*0.5];
        self.font = KJWFont(15.f);
        self.leftViewMode = UITextFieldViewModeAlways;
        self.returnKeyType = UIReturnKeySearch;
        [self.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
            NSLog(@"----");
        }];
    }
    return self;
}

- (instancetype)initWithSearchFieldTitle:(NSString *)title frame:(CGRect)frame textBlock:(void(^)(NSString *text))block {
    if (self = [super init]) {
        self.frame = frame;
        self.backgroundColor = [UIColor colorWithHexString:@"#EEEEEE"];
        self.clearButtonMode = UITextFieldViewModeWhileEditing;
        //搜索图标
        UIView *leftSearchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, 16)];
        leftSearchView.backgroundColor = [UIColor clearColor];
        UIImageView *searchImgView = [[UIImageView alloc]initWithFrame:CGRectMake(16, 0, 17, 16)];
        searchImgView.image = KJWImageNamed(kSearchImageName);
        searchImgView.contentMode = UIViewContentModeScaleAspectFit;
        [leftSearchView addSubview:searchImgView];
        self.leftView = leftSearchView;
        //文本
        NSMutableAttributedString *placeHolderAttr = [[NSMutableAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName : [UIColor colorWithHexString:@"#9B9B9B"],NSFontAttributeName : KJWFont(14.f)}];
        self.attributedPlaceholder = placeHolderAttr;
        self.textColor = [UIColor colorWithHexString:@"#262626"];
        [self jw_setCornerRadius:frame.size.height*0.5];
        self.font = KJWFont(15.f);
        self.leftViewMode = UITextFieldViewModeAlways;
        self.returnKeyType = UIReturnKeySearch;
        [self.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
            !block ?:block(x);
        }];
    }
    return self;
}

@end
