//
//  JW_HomeSearchButton.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/5.
//  Copyright © 2019 jiuwei. All rights reserved.
//  首页搜索按钮

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JW_HomeSearchButton : UIButton

- (instancetype)initWithSearchTitle:(NSString *)title frame:(CGRect)frame block:(void(^)(void))block;

@end

NS_ASSUME_NONNULL_END
