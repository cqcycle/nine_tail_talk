//
//  JW_HomeSearchButton.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/5.
//  Copyright © 2019 jiuwei. All rights reserved.
//

#import "JW_HomeSearchButton.h"

@implementation JW_HomeSearchButton

- (instancetype)initWithSearchTitle:(NSString *)title frame:(CGRect)frame block:(void(^)(void))block{
    if (self = [super init]) {
        self.frame = frame;
        [self jw_setCornerRadius:frame.size.height*0.5];
        [self setTitle:title forState:UIControlStateNormal];
        [self setTitleColor:KJWHexRGB(0x9b9b9b) forState:UIControlStateNormal];
        self.titleLabel.font = KJWFont(14.f);
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.backgroundColor = KJWHexRGB(0xF8F8F8);
        self.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        [self setImage:KJWImageNamed(kSearchImageName) forState:UIControlStateNormal];
        [[self rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
            !block ?:block();
        }];
    }
    return self;
}

@end
