//
//  JW_HomeSearchHeaderFooterView.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/5.
//  Copyright © 2019 jiuwei. All rights reserved.
//

#import "JW_HomeSearchHeaderFooterView.h"
@interface JW_HomeSearchHeaderFooterView()

///标题Label
@property (nonatomic, strong) UILabel *titleLabel;
///更多数据Label
@property (nonatomic, strong) UILabel *moreDataLabel;

@end

@implementation JW_HomeSearchHeaderFooterView


///懒加载标题Label
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = [UIFont systemFontOfSize:20.f weight:0.3];
        _titleLabel.text = @"";
        _titleLabel.textColor = kNormalTextColor_51;
        _titleLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _titleLabel;
}

///懒加载更多数据Label
- (UILabel *)moreDataLabel {
    if (!_moreDataLabel) {
        _moreDataLabel = [UILabel new];
        _moreDataLabel.font = [UIFont systemFontOfSize:13 weight:0.0];
        _moreDataLabel.text = @"";
        _moreDataLabel.textColor = kNormalTextColor_153;
        _moreDataLabel.textAlignment = NSTextAlignmentRight;
        _moreDataLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapLabel = [UITapGestureRecognizer new];
        [_moreDataLabel addGestureRecognizer:tapLabel];
        @weakify(self);
        [tapLabel.rac_gestureSignal subscribeNext:^(__kindof UIGestureRecognizer * _Nullable x) {
            @strongify(self);
            [self.moreSubject sendNext:x.view];
        }];
    }
    return _moreDataLabel;
}

- (void)jw_addSubViews {
    self.contentView.backgroundColor = [UIColor whiteColor];
    ///标题label
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.bottom.mas_equalTo(0);
    }];
    ///更多数据Label
    [self.contentView addSubview:self.moreDataLabel];
    [self.moreDataLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.bottom.mas_equalTo(0);
    }];
    
    
    
    
    ///监听数据改变
    @weakify(self);
    [RACObserve(self, titleStr) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if(self.titleStr.length == 0)return;
        self.titleLabel.text = x;
    }];
    [RACObserve(self, moreStr) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        self.moreDataLabel.hidden = !self.moreStr.length;
        self.moreDataLabel.text = x;
    }];

}



///懒加载 subject
- (RACSubject *)moreSubject {
    if (!_moreSubject) {
        _moreSubject = [RACSubject subject];
    }
    return _moreSubject;
}
@end
