//
//  JW_HomeSearchHeaderFooterView.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/5.
//  Copyright © 2019 jiuwei. All rights reserved.
//  首页搜索组标题

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JW_HomeSearchHeaderFooterView : JW_BaseTableViewHeaderFooterView
///点击查看更多
@property (nonatomic, strong) RACSubject *moreSubject;
///标题
@property (nonatomic, copy) NSString *titleStr;
///更多
@property (nonatomic, copy) NSString *moreStr;

@end

NS_ASSUME_NONNULL_END
