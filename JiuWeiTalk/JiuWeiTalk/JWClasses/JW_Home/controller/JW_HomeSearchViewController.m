//
//  JW_HomeSearchViewController.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/5.
//  Copyright © 2019 jiuwei. All rights reserved.
//

#import "JW_HomeSearchViewController.h"
#import "JW_SearchFieldView.h"
#import "JW_HomeSearchCell.h"
#import "JW_HomeSearchRecordCell.h"
#import "JW_HomeSearchHeaderFooterView.h"
@interface JW_HomeSearchViewController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
///tableView
@property (nonatomic, strong) JW_BaseTableView *tableView;
///根数据
@property (nonatomic, strong) NSMutableArray *rootDataSourcce;
///用户搜索输入的内容
@property (nonatomic, strong) NSMutableArray *searchDataSourcce;
///用户搜索的结果内容
@property (nonatomic, strong) NSMutableArray *searchResultDataSourcce;

///是否是正在搜索状态
@property (nonatomic, assign) BOOL isSearchStatus;

@end

static JW_HomeSearchViewController *object;

@implementation JW_HomeSearchViewController
///懒加载 用户搜索的结果内容
- (NSMutableArray *)searchResultDataSourcce {
    if (!_searchResultDataSourcce) {
        _searchResultDataSourcce = [NSMutableArray array];
    }
    return _searchResultDataSourcce;
}
///懒加载“用户搜索输入的内容”
- (NSMutableArray *)searchDataSourcce {
    if (!_searchDataSourcce) {
        _searchDataSourcce = [NSMutableArray array];
    }
    return _searchDataSourcce;
}

///懒加载根数据
- (NSMutableArray *)rootDataSourcce {
    if (!_rootDataSourcce) {
        _rootDataSourcce = [NSMutableArray arrayWithObjects:@"很多数据",@"很多数据",@"很多数据",@"假的数据",@"请搜索吧",@"好好查看下哦",@"相信你可以的", nil];
    }
    return _rootDataSourcce;
}

///懒加载tableView
- (JW_BaseTableView *)tableView {
    if (!_tableView) {
        _tableView = [[JW_BaseTableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([JW_HomeSearchCell class]) bundle:nil] forCellReuseIdentifier:[JW_HomeSearchCell jw_cellReuseIdentifier]];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([JW_HomeSearchRecordCell class]) bundle:nil] forCellReuseIdentifier:[JW_HomeSearchRecordCell jw_cellReuseIdentifier]];
        
        [_tableView registerClass:[JW_HomeSearchHeaderFooterView class] forHeaderFooterViewReuseIdentifier:[JW_HomeSearchHeaderFooterView jw_headerFooterReuseIdentifier]];
    }
    return _tableView;
}

#pragma mark -- 系统回调函数
- (void)jw_addSubViews {
    object = self;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(self.view.jw_masSafeTop);
        make.bottom.mas_equalTo(self.view.jw_masSafeBottom);
    }];
}
#pragma mark -- 设置导航栏
- (void)jw_layoutNavigation {
    @weakify(self);
    ///搜索框
    JW_SearchFieldView *titleView = [[JW_SearchFieldView alloc]initWithSearchFieldTitle:@"搜索一下内容" frame:CGRectMake(0, 0, kScreenWidth, 35.f) textBlock:^(NSString * _Nonnull text) {
        if (text.length == 0) {
            self.isSearchStatus = NO;
            [self.tableView reloadData];
        }
    }];
    titleView.delegate = self;
    self.navigationItem.titleView = titleView;
    ///取消按钮
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem jw_barButtonItemWithTitleName:@"取消" itemBlock:^{
        @strongify(self);
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

#pragma mark -- 请求数据
- (void)jw_getNewData {
    
}
#pragma mark -- 绑定数据
- (void)jw_bindViewModel {
    
}
#pragma mark -- UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"return 搜索");
    
    NSString *text = textField.text;
    if (text.length == 0) {
        [DYHUDView showOnlyTextHUD:@"请输入想搜索的内容"];
        self.isSearchStatus = NO;
        return YES;
    }
    self.isSearchStatus = YES;
    
    searchDataWithText(text);
    
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    NSLog(@"开始编辑");
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
//    ///点击搜索（return）或者（done）都会来到这里哦
//    NSString *text = textField.text;
//    if (text.length == 0) {
//        [DYHUDView showOnlyTextHUD:@"请输入想搜索的内容"];
//        self.isSearchStatus = NO;
//        return;
//    }
//    self.isSearchStatus = YES;
//    searchDataWithText(text);
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    return YES;
}
void searchDataWithText(NSString *text) {
    ///NSLog(@"点击搜索（return）或者（done）都会来到这里哦 ---- %@",text);
    ///1.搜索前移除之前的“搜索结果”数据
    [object.searchResultDataSourcce removeAllObjects];
    if (object.rootDataSourcce.count) {
        ///搜索的结果内容
        for (NSString *content in object.rootDataSourcce) {
            if ([content containsString:text]) {
                [object.searchResultDataSourcce addObject:content];
            }
        }
    }
    ///2.搜索输入的内容(如果输入的是同样的内容，将输入的内容置到最前面)
    BOOL isTheSameContent = NO;
    if (object.searchDataSourcce.count) {
        for (NSString *content in object.searchDataSourcce) {
            ///内容相同
            if ([text isEqualToString:content]) {
                isTheSameContent = YES;
                [object.searchDataSourcce removeObject:content];
                [object.searchDataSourcce insertObject:content atIndex:0];
                break;
            }
        }
    }
    ///3.不相同的数据添加到搜索记录里面
    if (!isTheSameContent) {
       [object.searchDataSourcce insertObject:text atIndex:0];
    }
    [object.tableView reloadData];
}
#pragma mark -- <UITableViewDelegate,UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(self.isSearchStatus){
        return 1;
    } else {
        if (self.searchDataSourcce.count) {
            return 2;
        }
        return 1;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isSearchStatus) {
        ///是否显示暂无数据图表
        (self.searchResultDataSourcce.count)>0?([self.tableView jw_dismissNoDataView]):([self.tableView jw_showNoDataViewWithContent:@"暂无搜索数据"]);
        return self.searchResultDataSourcce.count;
    } else {
        if (self.searchDataSourcce.count) {
            ///退出提示
            [self.tableView jw_dismissNoDataView];
            if (section == 0) {
                ///最多显示3条最新搜索记录
                return (self.searchDataSourcce.count > 3) ? 3:self.searchDataSourcce.count;
            } else {
                return self.rootDataSourcce.count;
            }
        } else {
            ///是否显示暂无数据图表
            (self.rootDataSourcce.count)>0?([self.tableView jw_dismissNoDataView]):([self.tableView jw_showNoDataViewWithContent:@"暂无数据"]);
            return self.rootDataSourcce.count;
        }
        
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.isSearchStatus) {
                ///搜索状态下
                JW_HomeSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:[JW_HomeSearchCell jw_cellReuseIdentifier]];
                cell.dataString = self.searchResultDataSourcce[indexPath.row];
                return cell;

    } else {
        if (self.searchDataSourcce.count) {
            if(indexPath.section == 0){
                JW_HomeSearchRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:[JW_HomeSearchRecordCell jw_cellReuseIdentifier]];
                cell.currentRowIndexPath = indexPath;
                cell.dataString = self.searchDataSourcce[indexPath.row]; ///订阅用户点击删除的信号,UITableViewCell和UICollectionViewCell复用使用RAC的问题,解决复用cell中信号的办法就是在cell里面创建的信号加上takeUntil:cell.rac_prepareForReuseSignal来让cell在每次重用的时候都去disposable创建的信号(解绑信号)
                [[cell.deleteSignal takeUntil:cell.rac_prepareForReuseSignal] subscribeNext:^(id  _Nullable x) {
                    ///删除数据
                    [self.searchDataSourcce removeObjectAtIndex:indexPath.row];
                    ///刷新表格
                    [self.tableView reloadData];
                }];
                return cell;
            } else {
                JW_HomeSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:[JW_HomeSearchCell jw_cellReuseIdentifier]];
                cell.dataString = self.rootDataSourcce[indexPath.row];
                return cell;
            }
        } else {
            JW_HomeSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:[JW_HomeSearchCell jw_cellReuseIdentifier]];
            cell.dataString = self.rootDataSourcce[indexPath.row];
            return cell;
        }
        
    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
///行高
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%d---%d--- %d",self.isSearchStatus,indexPath.section,indexPath.row);
}
///组标题
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    ///搜索状态下
    if (self.isSearchStatus) {
        return nil;
    } else {
        if (self.searchDataSourcce.count) {
            if (section == 0) {
                JW_HomeSearchHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:[JW_HomeSearchHeaderFooterView jw_headerFooterReuseIdentifier]];
                headerView.titleStr = @"全部结果数据";
                ///最多显示3条
                NSString *showMoreStr = nil;
                if (self.searchDataSourcce.count>3) {
                    showMoreStr = @"查看更多数据";
                    [headerView.moreSubject subscribeNext:^(id  _Nullable x) {
                        NSLog(@"%@",x);
                    }];
                } else {
                    showMoreStr = nil;
                }
                headerView.moreStr = showMoreStr;
                return headerView;
            } else if (section == 1) {
                JW_HomeSearchHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:[JW_HomeSearchHeaderFooterView jw_headerFooterReuseIdentifier]];
                headerView.titleStr = @"全部数据";
                headerView.moreStr = nil;
                return headerView;
            }
            return nil;
        } else {
            if (section == 0) {
                JW_HomeSearchHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:[JW_HomeSearchHeaderFooterView jw_headerFooterReuseIdentifier]];
                headerView.titleStr = @"全部数据";
                headerView.moreStr = nil;
                return headerView;
            }
            return nil;
        }
    }
    
}
///组头高
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(self.isSearchStatus){
     ///搜索状态下
        return 0.0000000000001f;
    } else {
        if (self.searchDataSourcce.count) {
            if (section == 0 || section == 1) {
                return 50.f;
            }
        } else {
            return 50.f;
        }
        
        return 0.0000000000001f;
    }
    
}
///组尾高
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.000000000000001f;
}
- (void)dealloc {
    object = nil;
}
@end
