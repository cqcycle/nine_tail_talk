//
//  JW_HomeViewController.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "JW_HomeViewController.h"
#import "JW_HomeCell.h"
#import "JW_HomeSearchButton.h"
#import "JW_HomeSearchViewController.h"
@interface JW_HomeViewController ()<UITableViewDelegate,UITableViewDataSource>
///tableView
@property (nonatomic, strong) JW_BaseTableView *tableView;
@end

@implementation JW_HomeViewController
///懒加载tableView
- (JW_BaseTableView *)tableView {
    if (!_tableView) {
        _tableView = [[JW_BaseTableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([JW_HomeCell class]) bundle:nil] forCellReuseIdentifier:[JW_HomeCell jw_cellReuseIdentifier]];
    }
    return _tableView;
}
#pragma mark -- 设置导航栏
- (void)jw_layoutNavigation {
    ///搜索
    @weakify(self);
    CGRect titleRect = CGRectMake(0, 0, kScreenWidth, 35);
    JW_HomeSearchButton *searchButton = [[JW_HomeSearchButton alloc]initWithSearchTitle:@"搜索一下" frame:titleRect block:^{
        @strongify(self);
//        [DYCommonTool appUserTrackDataPage:@"" event:@""];
        JW_HomeSearchViewController *searchVC = [JW_HomeSearchViewController new];
        BaseNavController *nav = [[BaseNavController alloc]initWithRootViewController:searchVC];
        nav.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:nav animated:YES completion:nil];
    }];
    self.navigationItem.titleView = searchButton;
    ///左边
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem jw_barButtonItemWithImageName:@"jw_message" itemBlock:^{
        NSLog(@"点击左边消息");
    }];
    ///右边
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem jw_barButtonItemWithImageName:@"jw_message" itemBlock:^{
        NSLog(@"点击右边消息");
    }];
}

#pragma mark -- 系统回调函数
- (void)jw_addSubViews {
    ///添加并布局tableView
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(self.view.jw_masSafeTop);
        make.bottom.mas_equalTo(self.view.jw_masSafeBottom);
    }];
   
}

#pragma mark -- 获取数据
- (void)jw_getNewData {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    dictionary[@"adUrl"] = @"";
    dictionary[@"articleId"] = @"78";
    dictionary[@"author"] = @"九尾君";
    dictionary[@"isAd"] = @"0";
    dictionary[@"readTimes"] = @"7392";
    dictionary[@"showPicType"] = @"1";
    dictionary[@"time"] = @"05-29 10:04";
    dictionary[@"title"] = @"15款热门寿险产品大PK！让你一次性学会如何买寿险";
    dictionary[@"picUrl"] = [NSArray arrayWithObject:@"https://dayou-picture.oss-cn-shanghai.aliyuncs.com/4/w8333409463148756996.jpg"];
    JW_CommonModel *model = [JW_CommonModel modelWithJSON:dictionary];
    if(!model.isRead && !model.isAd){
        //根据articleId,获取文章详情
        NSMutableDictionary *resultDic = [NSMutableDictionary dictionary];
        resultDic[@"author"] = @"九尾君";
        resultDic[@"collect"] = @"0";
        resultDic[@"commentNum"] = @"1";
        resultDic[@"detail"] = [self getHtmlContent];
        resultDic[@"downloadUrl"] = @"http://dayou-picture.oss-cn-shanghai.aliyuncs.com/ninetales/download.png";
        resultDic[@"subscribe"] = @"";
        resultDic[@"subscribe"] = @"1";
        resultDic[@"time"] = @"05-29 10:09";
        resultDic[@"title"] = @"15款热门寿险产品大PK！让你一次性学会如何买寿险";
        
    } else {
        WebViewController *webVC = [[WebViewController alloc]initWithTitle:model.title webUrl:model.adUrl];
        [self.navigationController pushViewController:webVC animated:YES];
    }
    NSLog(@"%@ ---- %@",model.author,model.title);
    
    
}
#pragma mark -- 绑定数据
- (void)jw_bindViewModel {
    
}

#pragma mark -- <UITableViewDelegate,UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JW_HomeCell *cell = [tableView dequeueReusableCellWithIdentifier:[JW_HomeCell jw_cellReuseIdentifier]];
    cell.titleLabel.text = [NSString stringWithFormat:@"------ %zd",indexPath.row];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%zd",indexPath.row);
}


- (NSString *)getHtmlContent{
    NSString *html = @"<div class=\"document\">\
    <p class=\"1 DocDefaults\"><span class=\"3\" style=""><span class="" style="">目前，保险市场上“群雄割据”，重疾险、百万医疗险等各类产品你方唱罢我登场。</span></span></p>\
     <p class=\"1 DocDefaults\"><span class=\"3\" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults\"><span class=\"3\" style=""><span class="" style="">但是在众多的保险类型中，九尾君最喜欢的其实还是寿险。作为最基础的保险之一，寿险的保险责任非常纯粹——身故赔付。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3\" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults\"><span class=\"3\" style=""><span class="" style="">寿险的意义就在于家庭经济支柱不幸身故后，家人的生活质量依旧能够得以保障，而不至于一落千丈。</span></span></p>\
     <p class=\"1 DocDefaults\"><span class=\"3\" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">很多小伙伴咨询九尾君，寿险应该怎么买？九尾君感觉这一句两句也讲不清楚啊，所以就写了这篇文章，带大家全面深入地扒一扒寿险。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">话不多说，先上图！</span></span></p>\
     <p class=\"1 DocDefaults \"><img height=\"423\" id=\"rId4\" src=\"https://dayou-picture.oss-cn-shanghai.aliyuncs.com/250/a6956172629599239930.png\" width=\"415\"></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">这是九尾君精心整理的15款热门寿险产品，以此为例，点评下各家寿险产品的亮点和坑点所在。</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">终身寿险和定期寿险哪个好？</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">目前，市面上的寿险主要分为终身寿险和定期寿险两大类。那么，终身寿险和定期寿险哪个更好呢？这也是很多朋友非常关心的一个问题。</span></span></p>\
     <p class=\"1 DocDefaults \"><img height=\"203\" id=\"rId5\" src=\"https://dayou-picture.oss-cn-shanghai.aliyuncs.com/90/u7745430303283498586.png\" width=\"414\"></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style="">定期寿险的优缺点</span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">优点：年交保费低，性价比高。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">缺点：保障期间内不出险就无法获得赔偿金。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style="">终身寿险的优缺点</span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">优点：保险期限为终身，不论什么情况都能赔付，兼具储蓄功能。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">缺点：保费较高，杠杆率偏低，未来面临通货膨胀的风险。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">总的来说，定期寿险和终身寿险各有优劣。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">定期寿险更像是一款“保”姓产品，购买的目的主要是为了避免家庭经济支柱突然身故造成财务危机。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">推荐购买人群：工薪阶层的家庭经济支柱</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">而终身寿险更像是一种投资，是把财产传承给下一代的一种方式。并且理赔的保险金一般是不收取税费的，可以作为合理避遗产税的一种方式。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">推荐购买人群：经济能力充裕的高净值人群</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">九尾君个人建议，对于大部分普通家庭来说，定期寿险已经足够满足风险保障的需求了，没必要花大价钱去购买终身寿险。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">选购寿险时需要注意什么？</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">想要挑选一款好的寿险产品，可是一门大学问。我们要从多方面进行排查筛选，剔除那些缺陷明显的产品，最后优中选优找出性价比最高的寿险产品。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">1. 保障范围：身故+全残</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">目前，主流的寿险产品一般都是同时保障身故和全残的。只有极少数的产品敢于冒天下之大不韪剔除了全残保障。</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">那么全残保障重不重要呢？答案是肯定的！</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">身故的话，人死了就结束了。可要是成了全残，不仅失去了基本的劳动能力，后续的一系列医疗费、生活费等等都是一笔不菲的开支。所以，全残给一个家庭带来的冲击有可能大于身故，更需要保险金来缓解家庭经济负担。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">幸运的是，在全残保障这一点上，九尾君所选的15款产品全都包含，集体通关！</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">2. 投保门槛：健康告知越宽松越好</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">一款寿险产品的健康告知内容是否宽松，直接决定了你能不能购买这款产品，需要重点关注。这里九尾君需要提醒大家，健康告知宽不宽松，与条款数量多少无关，而是看对疾病种类的限制、对投保人身体健康程度的要求是否严苛。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">尤其是疾病方面，我们需要特别注意健康告知中对结节、乙肝、高血压等常见高发疾病是否有所限制。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">接下来可能要涉及一大堆疾病专业术语，为了大家更流畅的阅读。九尾君先给大家做一个简单的疾病科普，对疾病知识有所了解的小伙伴也可以选择跳过~~</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style="">乙肝</span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style="">乙肝算是我国的常见疾病了。平均十个人中几乎有一个人是乙肝病毒携带者。</span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">肝炎包括甲、乙、丙型这三种病毒性肝炎，自然也包括慢性乙型肝炎和慢性活动性肝炎。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">慢性乙型肝炎一般指病程半年以上的乙型肝炎，半年以下的为急性肝炎。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">乙肝大三阳是免疫学上的叫法，乙肝病毒处于活动和复制期，传染性高于小三阳。所以很多保险产品会限制乙肝大三阳患者投保，而不限制小三阳患者。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">慢性活动性肝炎，属于感染乙肝病毒后的潜伏期，还未发展为慢性乙肝。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">乙肝表面抗原阳性，意味着感染了乙肝病毒，但不一定患有乙肝，也可能只是乙肝病毒的携带者。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">所以，在乙肝限制的严格程度上，乙肝表面抗原阳性＞肝炎＞慢性活动性肝炎＞慢性乙肝/乙肝大三阳。</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style="">结节</span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">其次是结节，结节分很多类型，常见的有乳腺结节、甲状腺结节、肺结节等。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">而一些健康告知里所说的性质不明的结节是比较宏观的概念，涵盖所有的结节病症。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">所以，在结节限制的严格程度上，性质不明的结节＞乳腺结节/甲状腺结节/肺结节。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style="">高血压</span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">最后是高血压，相信这个大家也不陌生了。以前高血压多出现在老年人群体，如今则有低龄化发展的趋势。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">高血压具体分级如下：</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">1、轻度高血压（I级）：收缩压在140—159mmHg和/或舒张压在90—99mmHg范围内为高血压1级。</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">2、中度高血压（II级）：收缩压在160—179mmHg和/或舒张压在100—109mmHg范围内为高血压2级。</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">3、重度高血压（III级）：收缩压≥180mmHg和/或舒张压≥110mmHg为高血压3级。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">因此，在高血压限制的严格程度上，高血压病＞高血压1级及以上＞高血压2级及以上＞高血压3级。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">下面是九尾君研究15款寿险产品的健康告知内容，整理的一份表单，大家可以作为投保参考。</span></span></p>\
     <p class=\"1 DocDefaults \"><img height=\"159\" id=\"rId6\" src=\"https://dayou-picture.oss-cn-shanghai.aliyuncs.com/148/u8674959681151480724.png\" width=\"415\"></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">以排名第一的瑞泰瑞和定期为例，我们可以看到，瑞和定期对乙肝、结节等病症都没有任何限制。高血压也只是限制了二级及以上的中/高度高血压，轻度高血压患者亦可投保。再加上对职业也没有任何要求。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">综合而言，瑞和定期的健康告知已经是市场最低门槛了。要是连瑞和定期都不能投保，那你基本也只能和寿险说拜拜了。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style=\"white-space:pre-wrap;\"> “高门槛”的寿险产品也不少——弘利相传就是其中的佼佼者。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">弘利相传对乙肝、结节、高血压这三项常见疾病限制严格，疾病健康限制内容堪比一些市面上的重疾险和医疗险产品了。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">除此之外，还对你的生活习惯、投保历史、甚至家人的身体情况有所要求，其中有4条如下：</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">1.是否每日吸烟支数乘以烟龄（年）＞400？</span></span></p>\
     <p class=\"1 DocDefaults \"></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">2.是否参与跑酷、滑翔、攀岩、探险、搏击、赛车、蹦极等极限运动？</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">3.您的父母是否患多囊肾病、是否在40岁前确诊结肠癌、直肠癌、乳腺癌或卵巢癌？</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">4.是否被保险公司拒保、延期、加费或除外责任承保？</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">虽然知道这是保险公司为了降低赔付率的条款设置，但是你这长达12条的健康告知是在查户口吗？</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">所以，九尾君建议，除非是对自己身体健康状况非常有信心的小伙伴，不然这款投保难度五星的产品可以直接pass了。</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">3. 理赔难度：免责条款越少越好</span></span></p>\
     <p class=\"1 DocDefaults \"></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">免责条款又称除外责任，通俗的讲就是保险公司合理“甩锅”拒赔的条款设定。一旦你因为这些免责条款里的情形导致死亡，保险公司可以理直气壮地拒绝赔付。所以，免责条款一般越少越好。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">老规矩，九尾君也做了一个表单，以供参考。</span></span></p>\
     <p class=\"1 DocDefaults \"><img height=\"397\" id=\"rId7\" src=\"https://dayou-picture.oss-cn-shanghai.aliyuncs.com/203/n4266527375409788619.png\" width=\"415\"></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">免责条款没有健康告知那么复杂，其宽松程度和条款的数量直接挂钩。条款的数量越少，理赔越容易，理赔难度越低。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">瑞和定期、大麦定寿依旧位列前三，限制十分宽松。只要不是故意伤害、不犯罪、不是两年内自杀保险公司都可赔付。</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">最常见的都是5条免责条款，都属于比较合理的</span></span></p>\
     <p class=\"1 DocDefaults \"><img height=\"98\" id=\"rId8\" src=\"https://dayou-picture.oss-cn-shanghai.aliyuncs.com/30/q8551797049900748062.png\" width=\"414\"></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">4. 保额选择：大于家庭负债总额</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">寿险的保额要怎么选择呢？是不是越高越好？当然不是！</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">谁都希望保额越高越好，这样出事了就能获得更高的保障和更多的保险金赔付。但是，人家保险公司也不傻，越高的保额意味着越高的保费。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">在保额的选取上，大家还是要根据自身的经济预算和家庭状况确定所需的保额。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">因为寿险的保险金是留给家人，用来赡养父母、养育儿女以及支付其他日常生活开支，并偿还你来不及还清的房贷、车贷等！所以，九尾君建议，家庭经济支柱的寿险保额应该大于家庭负债总额。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">具体计算方式是：将家人所需生活费、教育费、供养金、对外负债、丧葬费等，扣除既有资产，所得缺额作为寿险保额的估算依据。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">15款寿险产品的最高可投保额如下图所示：</span></span></p>\
     <p class=\"1 DocDefaults \"><img height=\"543\" id=\"rId9\" src=\"https://dayou-picture.oss-cn-shanghai.aliyuncs.com/144/u5909453713433212560.png\" width=\"415\"></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">需要注意的是，保险公司对不同城市、不同年龄的人士投保有不同的保额限制，并不是谁都能以最高保额进行投保的。</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">比如擎天柱终身寿险规定：</span></span></p>\
     <p class=\"1 DocDefaults \"></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">（1）北上广深：18-40周岁150万、41-50周岁100万；</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">（2）二线城市：18-40周岁100万、41-50周岁50万；</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">（3）其他城市：18-40周岁60万、41-50周岁30万；</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">（4）51-70周岁不区分地区30万。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">如果想投保更高保额，可以试着提供体检、财务收入等资料，用来证明你身强体健、且你的收入值得你拥有更高保额。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">比如祯爱优选，若是线上免体检投保，最高只能选取100万的保额。但若是线下投保且参与体检，最高保额可提升至2000万。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">总之，在投保的时候，大家记得关注一下寿险的投保限额，并根据自身情况合理选择保额。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">15款寿险产品终极PK</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">经过一轮轮的产品功能筛查，相信大家对如何挑选一款寿险产品也有了一定的了解。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">下面，九尾君根据各项产品的综合属性，将15款产品进行了一个简单的排名，以供大家参考。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">保费测算标准：30周岁男性，100万保额，分20年缴费。其中，定期寿险的保障期限为保到60周岁。</span></span></p>\
     <p class=\"1 DocDefaults \"><img height=\"237\" id=\"rId10\" src=\"https://dayou-picture.oss-cn-shanghai.aliyuncs.com/73/t3079080127450231625.png\" width=\"415\"></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">如果喜欢限制条件宽松的产品，可以选择瑞和定期。</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">不仅没有任何的职业限制，健康告知内容也十分友善。免责条款还只有三条，理赔容易。感觉自己身体有些小毛病或者从事高危职业的朋友可以选择购买瑞和定期，一般不是什么大病都可以顺利通过健康告知。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">如果看重产品的性价比，可以选择大麦定寿。</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">以2000元不到的保额撬动100万的保障额度，杠杆率非常高。同时，大麦定寿的健康告知和免责条款的宽松程度也仅次于瑞和定期。如此物美价廉的产品，赶紧买啊，还等什么呢！</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">如果喜欢产品选择性比较多的产品，可以选择华贵擎天柱。</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">它为客户提供了多项选择，根据对投保人身体健康要求的严格程度，分为免体检版、智能体检版、健康A+、健康A++等四个版本，保费依次降低。对自身健康状况非常有信心的朋友可以选择健康A++版，其价格甚至比大麦定寿还低很多。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">如果想要获得较高的保额，可以选择祯爱优选。</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">该产品的线下体检版最高可投2000万，足够满足大多数人的保额需求。不过需要注意的是，祯爱优选对吸烟人群不是很友好。要是你是个“老烟枪”的话，还是绕道走吧~</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">如果比较在意产品的保障期限，可以选择擎天柱终身寿险或守护e家终身寿险。</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">虽然价格上终身寿险的保费普遍是定期寿险的4倍左右，但是确实有很多朋友想利用保终身的寿险实现财富传承，给后代留下一些储蓄。</span></span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style="">九尾有话说</span></p>\
     <p class=\"1 DocDefaults \">&nbsp;</p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">寿险是人的金融生命，一份寿险保单可以让责任继续，让爱永恒。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">给自己买一份寿险还是很有必要的，尤其是那些“上有老下有小，银行有贷款”的家庭顶梁柱。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">今天说了这么多，主要是为大家科普下寿险的投保须知，给身上有些小毛病的健康问题人群提个醒。免得因为健康问题被保险公司拒之门外，投保无门。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">但是总的来说，寿险的限制条件比起重疾险和医疗险还是非常宽松的，正常人想要投保，问题不大。</span></span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=\"white-space:pre-wrap;\"> </span></p>\
     <p class=\"1 DocDefaults \"><span class=\"3 \" style=""><span class="" style="">因此，购买寿险的时候，价格才是首要因素。以最低的保费获得足额的保障才是我们选购一款保险产品的不二准则。</span></span></p>\
    </div>";
    
    return html;
}
@end
