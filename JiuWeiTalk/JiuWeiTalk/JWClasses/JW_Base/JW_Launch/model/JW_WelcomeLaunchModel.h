//
//  JW_WelcomeLaunchModel.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JW_WelcomeLaunchModel : NSObject

@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *urlImg;
@property (nonatomic, copy) NSString *urlSkip;

@end

NS_ASSUME_NONNULL_END
