//
//  JW_GuideViewController.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "JW_GuideViewController.h"
#import <UIImage+GIF.h>
#import "CYLTabBarControllerConfig.h"
#import "NT_LoginLunchViewController.h"
@interface JW_GuideViewController () <UIScrollViewDelegate> {
    UIScrollView *_scrollView;
}
//@property (nonatomic ,weak) UIPageControl *pageControl;
@property (nonatomic ,strong) UIButton *skipButton;


@end

@implementation JW_GuideViewController


- (void)dealloc{
    
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupSkipBtn];
    [self setupSrollView];
    
    if (@available(iOS 13.0, *)) {
        
        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    } else {
        // Fallback on earlier versions
    }

    
    [self.view bringSubviewToFront:self.skipButton];
//    [self setuppageControl];
    kJWUserDefManager
    [ud setBool:YES forKey:@"showGuidePage"];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self scrollViewDidEndDecelerating:_scrollView];
}
-(void)setupSrollView{
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    scrollView.backgroundColor = [UIColor whiteColor];
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    
    CGFloat gifW = 280;
    CGFloat gifH = 350;
    CGFloat gifTop = (KJWSCREEN_HEIGHT - gifH) / 2 - 83;
    CGFloat gifMarginX = (KJWSCREEN_WIDTH - gifW) / 2;
    CGFloat imageW = scrollView.frame.size.width;
    CGFloat imageH = 124;
    CGFloat imageTop = gifTop + gifH + 38;

    for (int i = 0; i < 3; i++) {
        CGFloat imageX = i * imageW;
        UIImageView *gifImageView = [[UIImageView alloc]initWithFrame:CGRectMake(imageX + gifMarginX, gifTop, gifW, gifH)];
        UIImage *gif = [UIImage sd_animatedGIFNamed:[NSString stringWithFormat:@"guidegif%d", i+1]];
        
        gifImageView.image = gif.images.lastObject;
        gifImageView.animationImages = gif.images;
        gifImageView.animationRepeatCount = 1;
        gifImageView.contentMode = UIViewContentModeScaleAspectFit;
        gifImageView.tag = 100 + i;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageX, imageTop, imageW, imageH)];
        imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"wenzi%d", i+1]];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [scrollView addSubview:gifImageView];
        [scrollView addSubview:imageView];
    }
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(3 * imageW, self.skipButton.top - 20, imageW, scrollView.height)];
    imageView.image = [UIImage imageNamed:@"yindaoye_four"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [scrollView addSubview:imageView];
    
    [self setupLastImageView:imageView];
    
    scrollView.contentSize = CGSizeMake(imageW * 4, 0);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.pagingEnabled = YES;
    scrollView.bounces = NO;
    _scrollView = scrollView;
}

- (void)setupSkipBtn{

    UIButton *skipButton = [[UIButton alloc]init];
    [skipButton setTitle:@"跳过" forState:UIControlStateNormal];
    [skipButton setTitleColor:KJWHexRGB(0x3D3733) forState:UIControlStateNormal];
    skipButton.titleLabel.font = [UIFont systemFontOfSize:14];
    
    CGFloat top = iPhoneX ? 44 : 20;
    skipButton.frame = CGRectMake(KJWSCREEN_WIDTH - 65, top, 50, 40);
    [skipButton addTarget:self action:@selector(start) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:skipButton];
    self.skipButton = skipButton;
}

//-(void)setuppageControl{
//    // 1.添加
//    UIPageControl *pageControl = [[UIPageControl alloc] init];
//    pageControl.numberOfPages = 3;
//    CGFloat centerX = self.view.frame.size.width * 0.5;
//    CGFloat centerY = self.view.frame.size.height - 130;
//    pageControl.center = CGPointMake(centerX, centerY);
//    pageControl.bounds = CGRectMake(0, 0, 100, 30);
//    pageControl.userInteractionEnabled = NO;
//    [self.view addSubview:pageControl];
//    self.pageControl = pageControl;
//
//    // 2.设置圆点的颜色
//    pageControl.currentPageIndicatorTintColor = [UIColor clearColor];
//    pageControl.pageIndicatorTintColor = [UIColor clearColor];
//}

/**
 *  添加内容到最后一个图片
 */
- (void)setupLastImageView:(UIImageView *)imageView{
    
    // 让imageView能跟用户交互
    imageView.userInteractionEnabled = YES;
    @weakify(self)
    [imageView bk_whenTapped:^{
        @strongify(self)
        [self start];
//        [DYCommonTool appUserTrackDataPage:@"000" event:@"060000"];
    }];
}

/**
 *  开始
 */
- (void)start
{
//    // 显示状态栏
//    [UIApplication sharedApplication].statusBarHidden = NO;
    
    // 切换窗口的根控制器
    kJWUserDefManager
    if (![ud boolForKey:@"isFirst"]) {
        NT_LoginLunchViewController *vc = [[NT_LoginLunchViewController alloc] init];
        vc.isFirst = YES;
        if ([DEVICE_NAME containsString:@"iPad"]) {
            vc.isPad = YES;
        }
        [DYCommonTool restoreRootViewController:[[BaseNavController alloc] initWithRootViewController:vc]];
    }else{
        [DYCommonTool restoreRootViewController:[[CYLTabBarControllerConfig alloc] init].tabBarController];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{

    CGFloat offsetX = scrollView.contentOffset.x;
    CGFloat pageDouble = offsetX / scrollView.frame.size.width;
    int pageInt = (int)(pageDouble);
    //    self.pageControl.currentPage = pageInt;
    if (pageInt >= 3) {
        self.skipButton.hidden = YES;
    }else{
        self.skipButton.hidden = NO;
    }
    for (UIView *view in scrollView.subviews) {
        if (view.tag == pageInt + 100) {
            [(UIImageView *)view startAnimating];
            break;
        }
    }
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        [self scrollViewDidEndDecelerating:scrollView];
    }
}

///**
// *  隐藏状态栏
// *
// */
//- (BOOL)prefersStatusBarHidden{
//    return YES;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
