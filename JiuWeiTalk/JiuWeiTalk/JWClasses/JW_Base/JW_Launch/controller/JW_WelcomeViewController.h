//
//  JW_WelcomeViewController.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JW_WelcomeViewController : UIViewController
@property (nonatomic, strong) CYLTabBarController *tabBarController;
@end

NS_ASSUME_NONNULL_END
