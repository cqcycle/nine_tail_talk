//
//  JW_WelcomeViewController.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "JW_WelcomeViewController.h"
#import "JW_HomeViewController.h"
//#import "PC_LoginViewController.h"
#import "JW_WelcomeLaunchModel.h"
#import "WebViewController.h"
#import <JPUSHService.h>
#import "CYLTabBarControllerConfig.h"
#import <AFNetworkReachabilityManager.h>
#import "MLHealthManager.h"
#import "NT_LoginLunchViewController.h"
#import "NT_MessageManager.h"
#import "JW_GuideViewController.h"
@interface JW_WelcomeViewController (){
    NSInteger _alertTimes;
}
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (weak, nonatomic) IBOutlet UIImageView *contentImageView;
@property (weak, nonatomic) IBOutlet UIButton *skipButton;
@property (strong, nonatomic) JW_WelcomeLaunchModel *viewModel;
@property (assign, nonatomic) BOOL shouldEnterMainView;

@end

@implementation JW_WelcomeViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [DYCommonTool appUserTrackDataPage:@"001" event:@"001000"];
    
    NSString *imageName;
    if (iPhone5size) {
        imageName = @"640x1136";
    }else if (iPhone6size) {
        imageName = @"750x1334";
    }else if (iPhone6plusSize) {
        imageName = @"1242x2208";
    }else if (iPhoneX) {
        imageName = @"1125x2436";
        if (kScreenWidth == 2688) {
            imageName = @"1242×2688";
        }else if (KJWSCREEN_WIDTH == 1792){
            imageName = @"828×1792";
        }
    }else {
        imageName = @"640x960";
    }
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    NSString *imagePath = [[NSBundle mainBundle]pathForResource:imageName ofType:@"png"];
    self.bgImageView.image = [UIImage imageWithContentsOfFile:imagePath];
    
    if (@available(iOS 13.0, *)) {
        
        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    } else {
        // Fallback on earlier versions
    }
    
    
    
//    NSMutableDictionary *param = @{@"type" : @"1"}.mutableCopy;
//    [DYRequestData getMobileAppGetAdByTypeParams:param success:^(id responseData) {
//        self.viewModel = [PC_CommonAdModel modelWithJSON:responseData];
//
//    } failure:^(NSError *error) {
//        [self enterMainView];
//    }];
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    [self networkStatus];
    kJWUserDefManager
    if ([ud boolForKey:@"hasAlertHealth"] && [JW_UserModel sharedInstance].userId.length > 0) {
        [[MLHealthManager sharedInstance] getIphoneHealthDataCompletion:^(BOOL success) {
            if (!success) {
                [DYHUDView showErrorHUD:@"授权失败"];
            }
        } withAlwaysAlert:NO withController:self];
    }
//    });
    
}

- (void)networkStatus{
    if (kIsNetwork) {
        [self enterMainView];
    }else{
        if (_alertTimes >= 10) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"网络好像开小差了，请稍后重试~" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"重试" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self networkStatus];
            }]];
            
            [self presentViewController:alert animated:YES completion:^{
            }];
        }else{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self networkStatus];
                self->_alertTimes++;
            });
        }
    }
}

- (void)enterMainView {
    
    kJWUserDefManager
    if (![ud boolForKey:@"showGuidePage"]) {
        [DYCommonTool restoreRootViewController:[[JW_GuideViewController alloc] init]];
    }
    else if (![ud boolForKey:@"isFirst"]) {
        NT_LoginLunchViewController *vc = [[NT_LoginLunchViewController alloc] init];
        vc.isFirst = YES;
        if ([DEVICE_NAME containsString:@"iPad"]) {
            vc.isPad = YES;
        }
        [DYCommonTool restoreRootViewController:[[BaseNavController alloc] initWithRootViewController:vc]];
    }
    else{
        CYLTabBarController *tabBarController = [[CYLTabBarControllerConfig alloc] init].tabBarController;
        self.tabBarController = tabBarController;
        [DYCommonTool restoreRootViewController:tabBarController];
    }
}

- (void)setViewModel:(JW_WelcomeLaunchModel *)viewModel{
    _viewModel = viewModel;
    if (viewModel.urlImg.length > 0) {
        self.skipButton.hidden = NO;
        [self.contentImageView sd_setImageWithURL:[NSURL URLWithString:viewModel.urlImg]];
        self.shouldEnterMainView = YES;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (self.shouldEnterMainView) {
                [self enterMainView];
            }
        });
        
        if (viewModel.urlSkip.length > 0) {
            @weakify(self)
            [self.contentImageView bk_whenTapped:^{
                @strongify(self)
                self.shouldEnterMainView = NO;
                WebViewController *web = [[WebViewController alloc]initWithTitle:viewModel.title webUrl:viewModel.urlSkip];
                web.enterMainViewBlock = ^{
                    [self enterMainView];
                };
                [self.navigationController pushViewController:web animated:YES];
            }];
        }
    }else
        [self enterMainView];
}
- (IBAction)skipClick:(UIButton *)sender {
    self.shouldEnterMainView = NO;
    [self enterMainView];
}


@end
