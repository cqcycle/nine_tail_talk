//
//  WebViewController.m
//  JiaBan
//
//  Created by 程睿 on 2017/9/4.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import "WebViewController.h"
#import "WebViewJavascriptBridge.h"
#import <ContactsUI/ContactsUI.h>
//#import "PC_FeedbackViewController.h"
#import "IQKeyboardManager.h"
#import "DXQButton.h"
#import <UIImage+GIF.h>
#import "AppDelegate.h"
#import "UserLocation.h"
#import "AllPay.h"
#import <UShareUI/UShareUI.h>
//#import "PC_SharePanel.h"
//#import "NT_MeWalletViewController.h"

//#import "NT_CartoonModel.h"
//#import "NT_SubTipsModel.h"
//#import "NT_MasterVideoSharePanel.h"
#import "NT_UMengShareHelper.h"

//#import "FK_OrderDataViewController.h"

typedef void (^CameraCallbackBlock)(UIImage *image);
typedef void (^ContactCallbackBlock)(NSDictionary *contact);
typedef void (^SetTitleCallbackBlock)(void);
typedef void (^SavePhotoCallbackBlock)(void);

@interface WebViewController () <WKNavigationDelegate, WKUIDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UMSocialShareMenuViewDelegate, UIScrollViewDelegate>

@property WebViewJavascriptBridge* bridge;
//@property(nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong) UIBarButtonItem *backButton;//返回按钮
@property (nonatomic, assign, getter=isAllFinish) BOOL allFinish;//返回键是'关闭YES'或'后退NO'
@property (nonatomic, assign, getter=shouldStop) BOOL stop;//返回键是否等待弹窗
@property (nonatomic, assign) NSInteger allFinishCount;//allFinish时，关闭层数
@property (nonatomic, strong) UIBarButtonItem *closeButton;//关闭按钮
@property (nonatomic, copy) CameraCallbackBlock userCameraCallback; //调用照相机回调
@property (nonatomic, copy) ContactCallbackBlock userContactCallback; //选择联系人回调
@property (nonatomic, copy) SetTitleCallbackBlock setTitleCallBack; //setTitle的回调
@property (nonatomic, copy) SavePhotoCallbackBlock savePhotoCallback; //savePhoto的回调

//@property (nonatomic, strong) PC_SharePanel *sharePanel;

/// 漫画的分享
//@property(nonatomic, strong) NT_MasterVideoSharePanel *cartoonSharePanel;


@end

@implementation WebViewController

- (instancetype)initWithTitle:(NSString *)title webUrl:(NSString *)url{
    if (self = [super init]) {
        self.titleString = title;
        self.webUrl = url;
    }
    return self;
}

- (void)dealloc{
    if ([self isViewLoaded]) {
        [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
        [self.webView removeObserver:self forKeyPath:@"title"];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if (self.isDisease) {
        [DYCommonTool recordArticleWithEventType:2 andArticleId:[NSString stringWithFormat:@"%ld", (long)self.diseaseId] andArticleType:2 andLocation:0];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController setNavigationBarHidden:self.hideHeader animated:YES];// 显示或隐藏导航栏
    if (!self.hideHeader) {
        //        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]]
                                                     forBarPosition:UIBarPositionAny
                                                         barMetrics:UIBarMetricsDefault];
    }
    
    /**
     jsBridge 刷新指定层级的页面
     **/
//    NSLog(@"\nwebRefresh============================%d", (int)webRefresh);
    NSLog(@"\nweb url===%@\n", self.webView.URL.absoluteString);
    if (0 == webRefresh) {
        [self.webView reloadFromOrigin];
        webRefresh = bigInteger;
    }
    
}
- (void)deleteWebCache {
    //all WebsiteDataTypes清除所有缓存
    NSSet *websiteDataTypes = [WKWebsiteDataStore allWebsiteDataTypes];
    
    NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
    
    [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
        
    }];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [DYHUDView hiddenHud];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [DYHUDView showIndicatorView];
    webRefresh ++;
    self.navigationItem.title = self.titleString;
    
    [self setupView];
    
    [self setUpBridge];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSeccessNotification:) name:@"loginSuccessNotification" object:nil];
    
    [NT_UMengShareHelper setupShareMenuWithUMDelegate:self];
    
}

- (void)setupView{
    
    if (self.webView){
        [self.webView removeFromSuperview];
    }
    
    CGFloat height = KJWSCREEN_HEIGHT - 64;
    CGFloat top = 0;
    if (_isTabView) {
        height = KJWSCREEN_HEIGHT - 64 - 49;
    }
    if (iPhoneX) {
        height -= 34+24;
    }
    if (self.hideHeader) {
        height += 44;
        top += iPhoneX?44 : 20;
    }
    if (self.beginFromTop) {
        top = iPhoneX?44:0;
    }
    self.webView = [[WKWebView alloc]initWithFrame:CGRectMake(0, top, KJWSCREEN_WIDTH, height)];
    NSURL *weburl = [NSURL URLWithString:self.webUrl];
    [self.webView loadRequest:[NSURLRequest requestWithURL:weburl]];
    [self.view addSubview:self.webView];
    self.webView.backgroundColor = [UIColor whiteColor];
    self.webView.navigationDelegate = self;
    self.webView.UIDelegate = self;
//    self.webView.allowsBackForwardNavigationGestures = YES; // 左右滑前进后退手势，默认关闭
    // KVO
    [self.webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL];
    [self.webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:NULL];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recivePayStatus:) name:@"payStatusPost" object:nil];
    if (@available(iOS 11.0, *)) {
        self.webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    //自定义UA，判断网页是否是在app中打开, ios 9 之后
    // iOS 12 bug??
    if (@available(iOS 12.0, *)){
        NSString *baseAgent = @"Mozilla/5.0 (iPhone; CPU iPhone OS 12_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/16A366";
        self.webView.customUserAgent = [NSString stringWithFormat:@"%@ app:%@ platform:%@ channel:appstore version:%@ model:%@", baseAgent, APPNAME, PLATFORM, APP_VERSION, DEVICE_NAME];
    }else if ([SYS_VER floatValue] >= 9.0) {
        [self.webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id result, NSError *error) {
            self.webView.customUserAgent = [NSString stringWithFormat:@"%@ app:%@ platform:%@ channel:appstore version:%@ model:%@", result, APPNAME, PLATFORM, APP_VERSION, DEVICE_NAME];
        }];
    }
    
    if (_isTabView) {
        // tab
    }else{
        // not tab
        self.navigationItem.leftBarButtonItem = self.backButton;
    }
    self.webView.scrollView.bounces = NO;
//    self.webView.scrollView.showsVerticalScrollIndicator = NO;
    self.webView.scrollView.delegate = self;
    
    if (self.diseaseId != 0 || self.isVipCard || self.shareType == WebViewControllerShareTypeCartoon) {
        //疾病详情分享
        self.navigationItem.rightBarButtonItem = [self shareDiseaseBtn];
    }

    
}


- (void)setUpBridge{ // 注册jsbridge方法
    [WebViewJavascriptBridge enableLogging];
    _bridge = [WebViewJavascriptBridge bridgeForWebView:self.webView];
    [_bridge setWebViewDelegate:self];
    
    @weakify(self)
    //获取基本信息
    [_bridge registerHandler:@"getPhone" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSMutableDictionary *requestParam = [[NSMutableDictionary alloc]init];
        NSString *userId = [JW_UserModel sharedInstance].userId;
        requestParam[@"userId"] = userId;
        requestParam[@"app"] = APPSECRET;
        requestParam[@"unionId"] = [JW_UserModel sharedInstance].unionId;
        requestParam[@"phone"] = [JW_UserModel sharedInstance].mobile;
        NSString *paramStr = [requestParam modelToJSONString];
        responseCallback(paramStr);
    }];
    //弹出登录页
    [_bridge registerHandler:@"showLogin" handler:^(id data, WVJBResponseCallback responseCallback) {
        [DYCommonTool alertLoginVC];
    }];
    
    //弹窗并复制到剪贴板
    [_bridge registerHandler:@"showToast" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSDictionary *dict = data;
        if ([dict containsObjectForKey:@"msg"]) {
            [DYHUDView showSuccessHUD:[NSString stringWithFormat:@"成功复制 %@ 到剪贴板", dict[@"msg"]]];
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            [pasteboard setString:dict[@"msg"]];
        }
    }];
    
    [_bridge registerHandler:@"gotoChat" handler:^(id data, WVJBResponseCallback responseCallback) {
        
        @strongify(self);
//        FK_OrderDataViewController *vc = [[FK_OrderDataViewController alloc] init];
//        [self.navigationController pushViewController:vc animated:YES];
        
    }];
    
    
    //调用照相机
    [_bridge registerHandler:@"userCamera" handler:^(id data, WVJBResponseCallback responseCallback) {
        @strongify(self)
        UIImagePickerController *ipc = [[UIImagePickerController alloc] init];
        ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
        ipc.delegate = self;
        ipc.modalPresentationStyle = UIModalPresentationOverFullScreen;
        [self presentViewController:ipc animated:YES completion:nil];
        //回调照片
        self.userCameraCallback = ^(UIImage *image) {
            NSData *imgData = UIImageJPEGRepresentation(image, 0.1);
            NSString *encodedImageStr = [imgData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            responseCallback(encodedImageStr);
        };
    }];
    
    // push新的网页
    [self.bridge registerHandler:@"goWeb" handler:^(id data, WVJBResponseCallback responseCallback) {
        
        NSDictionary *dic = (NSDictionary *)data;
        if (dic) {
            @strongify(self)
            WebViewController *web = [[WebViewController alloc] init];
            web.titleString = [dic objectForKey:@"title"];
            web.webUrl = [dic objectForKey:@"url"];
            if ([[dic[@"isBack"] stringValue] isEqualToString:@"0"]) {
                web.hideHeader = YES;
            }else{
                web.hideHeader = NO;
            }
            web.beginFromTop = [[dic objectForKey:@"beginFromTop"] boolValue];
            [self.navigationController pushViewController:web animated:YES];
            
            NSInteger count = 0;
            if (dic[@"close"]) {
                count = [dic[@"close"] integerValue];
            }
            if (count>0) {
                NSMutableArray *controllers = self.navigationController.viewControllers.mutableCopy;
                for (; count>0; count--) {
                    if (controllers.count <= 2) {
                        break;
                    }
                    [controllers removeObjectAtIndex:controllers.count-2];
                }
                self.navigationController.viewControllers = controllers;
            }
        }
    }];
    
    // pop当前页面
    [self.bridge registerHandler:@"finish" handler:^(id data, WVJBResponseCallback responseCallback) {
        @strongify(self)
        NSInteger count = 1;
        NSDictionary *dict = [NSDictionary dictionary];
        if ([data isKindOfClass:[NSDictionary class]]) {
            dict = data;
        }
        if (dict[@"count"]) {
            count = [dict[@"count"] integerValue];
        }
        if (count>1) {
            NSMutableArray *controllers = self.navigationController.viewControllers.mutableCopy;
            for (; count>1; count--) {
                if (controllers.count <= 2) {
                    break;
                }
                [controllers removeObjectAtIndex:controllers.count-2];
            }
            self.navigationController.viewControllers = controllers;
        }
        [self closeNative];
        
    }];
    // 返回键是'关闭'或'后退'
    [self.bridge registerHandler:@"allFinish" handler:^(id data, WVJBResponseCallback responseCallback) {
        @strongify(self)
        self.allFinish = [data[@"allFinish"] boolValue];
        if (data[@"count"]) {
            self.allFinishCount = [data[@"count"] integerValue];
        }else{
            self.allFinishCount = 1;
        }
    }];
    
    // 用户行为打点
    [self.bridge registerHandler:@"dot" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSDictionary *dic = (NSDictionary *)data;
        if ([dic containsObjectForKey:@"currentEventPage"] && [dic containsObjectForKey:@"currentEvent"]) {
            [DYCommonTool appUserTrackDataPage:dic[@"currentEventPage"] event:dic[@"currentEvent"]];
        }
    }];
    
    // 隐藏顶部导航栏
    [self.bridge registerHandler:@"jump" handler:^(id data, WVJBResponseCallback responseCallback) {

        if ([data containsObjectForKey:@"flag"]) {
            BOOL flag = [data[@"flag"] boolValue];
            NSInteger naviHeight = iPhoneX ? 88 : 64;
            NSInteger statusHeight = iPhoneX ? 44 : 20;
            NSInteger tabHeight = iPhoneX ? 83 : 49;
            @strongify(self)
            if (flag) {
                self.view.top = naviHeight;
                self.webView.top = 0;
                self.webView.height = KJWSCREEN_HEIGHT - naviHeight + 1 - (iPhoneX?34:0);
                self.navigationController.navigationBarHidden = NO;
                self.hideHeader = NO;
            }else{
                self.view.top = 0;
                self.webView.top = statusHeight;
                self.webView.height = KJWSCREEN_HEIGHT - statusHeight + 1 - (iPhoneX?34:0);
                self.navigationController.navigationBarHidden = YES;
                self.hideHeader = YES;
            }
            if (self.isTabView) {
                self.webView.height -= tabHeight;
            }
        }
    }];
    // 设置导航标题
    [self.bridge registerHandler:@"setTitle" handler:^(id data, WVJBResponseCallback responseCallback) {
        @strongify(self)
        if ([data containsObjectForKey:@"stop"]) {
            self.stop = [data[@"stop"] boolValue];
        }
        if ([data containsObjectForKey:@"title"]) {
            self.titleString = data[@"title"];
            self.navigationItem.title = self.titleString;
            self.setTitleCallBack = ^{
                responseCallback(@"go back!");
            };
        }
    }];
    // 刷新指定层级页面
    [self.bridge registerHandler:@"toRefresh" handler:^(id data, WVJBResponseCallback responseCallback) {
        if (data[@"refreshCount"]) {
            webRefresh = [data[@"refreshCount"] integerValue];
            if (webRefresh <= 0) {
                webRefresh = bigInteger;
            }
        }
    }];
    // 将webView提升至statusBar顶部
    [self.bridge registerHandler:@"hideStatusBar" handler:^(id data, WVJBResponseCallback responseCallback) {
        @strongify(self)
        NSInteger offset = iPhoneX?0:20;
        self.webView.top -= offset;
        self.webView.height += offset;
        
    }];
    // 保存图片至本地
    [self.bridge registerHandler:@"savePhoto" handler:^(id data, WVJBResponseCallback responseCallback) {
        @strongify(self)
        NSData *imgData;
        if (data[@"img"]){
            imgData = [NSData dataWithBase64EncodedString:data[@"img"]];
        }else if (data[@"imgUrl"]){
            imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:data[@"imgUrl"]]];
        }
        if (imgData) {
            UIImage *image = [UIImage imageWithData:imgData];
            UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), (__bridge void *)self);
            self.savePhotoCallback = ^{
                responseCallback(@"保存成功");
            };
        }
    }];
    // 分享弹窗
    [self.bridge registerHandler:@"showSharePanel" handler:^(id data, WVJBResponseCallback responseCallback) {
        @strongify(self)
        if ([data isKindOfClass:[NSDictionary class]]) {
            [self setupShareUI];
            
            kJWWeakSelf
            [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
                // 根据获取的platformType确定所选平台进行下一步操作
                [weakSelf shareWebPageToPlatformType:platformType shareData:data];
            }];
        }
    }];
    // 我要纠错
    [self.bridge registerHandler:@"issueReport" handler:^(id data, WVJBResponseCallback responseCallback) {
        
        @strongify(self)
        if ([data isKindOfClass:[NSDictionary class]]) {
//            PC_FeedbackViewController *vc = [[PC_FeedbackViewController alloc] init];
//            vc.selectedItem = 4;
//            vc.issueStr = [NSString stringWithFormat:@"【疾病百科】【%@】", self.titleString];
//            [self.navigationController pushViewController:vc animated:YES];
        }
    }];
    // 退回首页
    [self.bridge registerHandler:@"goToHome" handler:^(id data, WVJBResponseCallback responseCallback) {
        @strongify(self)
        //        [self.navigationController setNavigationBarHidden:NO animated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshRootVC" object:nil];
        [self.navigationController popToRootViewControllerAnimated:YES];
        self.tabBarController.selectedIndex = 0;
        //        [[NSNotificationCenter defaultCenter] postNotificationName:@"pretendClick" object:nil];
    }];
    // 退回首页
    [self.bridge registerHandler:@"goToVIP" handler:^(id data, WVJBResponseCallback responseCallback) {
        @strongify(self)
        //        [self.navigationController setNavigationBarHidden:NO animated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshRootVC" object:nil];
        [self.navigationController popToRootViewControllerAnimated:YES];
        self.tabBarController.selectedIndex = 0;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pretendClick" object:nil];
    }];
    // 关闭网页+1层原生
    [self.bridge registerHandler:@"closeReimbursement" handler:^(id data, WVJBResponseCallback responseCallback) {
        @strongify(self)
        NSMutableArray *controllers = self.navigationController.viewControllers.mutableCopy;
        [controllers removeObjectAtIndex:controllers.count-2];
        self.navigationController.viewControllers = controllers;
        [self closeNative];
    }];
    // 跳转我的钱包
    [self.bridge registerHandler:@"goToWallet" handler:^(id data, WVJBResponseCallback responseCallback) {
        @strongify(self)
//        NT_MeWalletViewController *vc = [[NT_MeWalletViewController alloc] init];
//        [self.navigationController pushViewController:vc animated:YES];
    }];
//    // 支付
//    [self.bridge registerHandler:@"comfirmPayment" handler:^(id data, WVJBResponseCallback responseCallback) {
//        //payWay applyNumber
//        @strongify(self)
//        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//        params[@"applyNumber"] = data[@"applyNumber"];
//        if ([[data[@"payWay"] stringValue] isEqualToString:@"1"]) {
////            [PC_HomeRequest getAppAlipayGetOrderParams:params success:^(id responseData) {
////                [AllPay payTreasurepayWithPayOrder:responseData orderNo:^(BOOL isSuccess) {
////                    responseCallback(@{@"payStatus" : isSuccess ? @"1" : @"2"});
////                }];
////            } failure:^(NSError *error) {
////                
////            }];
////            self.payCall = ^(NSString *code) {
////                responseCallback(@{@"payStatus" : [code isEqualToString:@"9000"] ? @"1" : @"2"});
////            };
//        }else{
////            [PC_HomeRequest getAppWechatpayUnifiedorderParams:params success:^(id responseData) {
////                [AllPay payWeChatPayWithAppid:responseData[@"appId"] AndNoncestr:responseData[@"nonceStr"] AndPackage:responseData[@"package"] AndPartnerid:responseData[@"partnerId"] AndPrepayid:responseData[@"prepayId"] AndTimestamp:responseData[@"timeStamp"] AndSign:responseData[@"sign"] AndPackagestr:responseData[@"appId"]];
////            } failure:^(NSError *error) {
////
////            }];
////            self.weixinCall = ^(NSString *code) {
////                responseCallback(@{@"payStatus" : [code isEqualToString:@"0"] ? @"1" : @"2"});
////            };
//        }
//    }];
}

- (void)recivePayStatus:(NSNotification *)notification
{
    NSDictionary * infoDic = [notification object];
    if ([infoDic[@"payWay"] isEqualToString:@"1"]) {
        if (_payCall) {
            _payCall(infoDic[@"orderState"]);
        }
    }else{
        if (_weixinCall) {
            _weixinCall(infoDic[@"orderState"]);
        }
    }
    
}

// 保存图片完成回调
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    
    if (error) {
        NSLog(@"%ld", (long)error.code);
        if (error.code == -3310) {
            [DYHUDView showAttentionHUD:@"请在设置中打开照片权限"];
        }else{
            [DYHUDView showErrorHUD:@"保存失败"];
        }
    }else{
        [DYHUDView showSuccessHUD:@"保存成功"];
        if (self.savePhotoCallback) {
            self.savePhotoCallback();
        }
    }
    
}

- (void)loginSeccessNotification:(NSNotification *)notification{
    [self deleteWebCache];
    NSString *url = self.webView.URL.absoluteString;
    NSString *append = [url containsString:@"?"] ? @"&userId=" : @"?userId=";
    NSString *newUrl = [NSString stringWithFormat:@"%@%@%@", url, append, [JW_UserModel sharedInstance].userId];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@""]]];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:newUrl]]];
}

#pragma mark - 图片选择控制器的代理

//点击取消
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    // 1.销毁picker控制器
    [picker dismissViewControllerAnimated:YES completion:nil];
}

//点击完成
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    // 1.销毁picker控制器
    [picker dismissViewControllerAnimated:YES completion:nil];
    // 2.返回的图片
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    // 3.回调照片
    if (_userCameraCallback) {
        _userCameraCallback(image);
    }
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    //    // 页面加载进度条
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        //        self.progressView.progress = self.webView.estimatedProgress;
        //        if (self.progressView.progress == 1) {
        //            /*
        //             *添加一个简单的动画，将progressView的Height变为1.4倍，在开始加载网页的代理中会恢复为1.5倍
        //             *动画时长0.25s，延时0.3s后开始动画
        //             *动画结束后将progressView隐藏
        //             */
        //            __weak typeof (self)weakSelf = self;
        //            [UIView animateWithDuration:0.25f delay:0.3f options:UIViewAnimationOptionCurveEaseOut animations:^{
        //                weakSelf.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.4f);
        //            } completion:^(BOOL finished) {mega网盘
        //                weakSelf.progressView.hidden = YES;
        //
        //            }];
        //        }
//        if (self.isDuiBa && self.webView.estimatedProgress == 1) {
//            if ([self.webView canGoBack]) {
//                self.navigationItem.leftBarButtonItem = self.backButton;
//            }else{
//                self.navigationItem.leftBarButtonItems = @[];
//            }
//        }
    }
    // 获取html的title
    else if ([keyPath isEqualToString:@"title"]){
        if (object == self.webView) {
            if (!self.titleString) {
                self.navigationItem.title = self.webView.title;
            }
        }else{
            [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        }
    }else{
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}
#pragma mark - WKNavigationDelegate
//WKWebView 开启跳转 App Store， 拨打电话等功能。。默认为禁止的，需在如下代理方法中处理
-(void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    //    NSLog(@"\n\n\nnavigationAction = %@", navigationAction);
    
    if(webView != self.webView) {
        decisionHandler(WKNavigationActionPolicyAllow);
        return;
    }
    //--------------fix xcode9 crash------------------
    WebViewJavascriptBridgeBase *base = [[WebViewJavascriptBridgeBase alloc] init];
    if ([base isWebViewJavascriptBridgeURL:navigationAction.request.URL]) {
        return;
    }
    //------------------------------------------------
    
    
    
    NSURL *url = navigationAction.request.URL;
    UIApplication *app = [UIApplication sharedApplication];
    //    NSLog(@"scheme::%@,url::%@", url.scheme, url.absoluteString);
    if ([url.scheme isEqualToString:@"tel"]){
        if ([app canOpenURL:url]){
            /// 解决iOS10及其以上系统弹出拨号框延迟的问题
            if (@available(iOS 10.0, *)) {
                [app openURL:url options:@{} completionHandler:nil];
            } else {
                [app openURL:url];
            }
            decisionHandler(WKNavigationActionPolicyCancel);
            return;
        }
    }else if ([url.absoluteString containsString:@"itunes.apple.com"] ||
              [url.absoluteString containsString:@"q.url.cn"] ||
              [url.absoluteString containsString:@"alipay://"] ||
              [url.absoluteString containsString:@"alipays://"] ||
              [url.absoluteString containsString:@"wechat://"] ||
              [url.absoluteString containsString:@"weixin://"] ||
              [url.absoluteString containsString:@"tbopen://"]) {
        
        if ([app canOpenURL:url]){
            /// 解决iOS10及其以上系统弹出拨号框延迟的问题
            if (@available(iOS 10.0, *)) {
                [app openURL:url options:@{} completionHandler:nil];
            } else {
                [app openURL:url];
            }
            decisionHandler(WKNavigationActionPolicyCancel);
            return;
        }
    }
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    [DYHUDView hiddenHud];
}

#pragma mark - WKUIDelegate
//WKWebView 支持新窗口页面跳转
- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures{
    WKFrameInfo *frameInfo = navigationAction.targetFrame;
    if (![frameInfo isMainFrame]) {
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}
//WKWebView 接受JS中的 alert, confirm 等弹窗，需在下面代理方法中处理
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          completionHandler();
                                                      }]];
    [self presentViewController:alertController animated:YES completion:^{}];
}
-(void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler{
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消"
                                                        style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction *action) {
                                                          completionHandler(NO);
                                                      }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          completionHandler(YES);
                                                      }]];
    [self presentViewController:alertController animated:YES completion:^{}];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 设置 返回、关闭 按钮
//点击返回的方法
- (void)backNative{
    if (self.setTitleCallBack) {
        self.setTitleCallBack();
        self.setTitleCallBack = nil;
    }
    if ([self shouldStop]) {
        return;
    }
    if ([self isAllFinish]) {
        NSInteger count = self.allFinishCount;
        if (count>1) {
            NSMutableArray *controllers = self.navigationController.viewControllers.mutableCopy;
            for (; count>1; count--) {
                if (controllers.count <= 2) {
                    break;
                }
                [controllers removeObjectAtIndex:controllers.count-2];
            }
            self.navigationController.viewControllers = controllers;
        }
        [self closeNative];
    }else{
        //判断是否有上一层H5页面
        if ([self.webView canGoBack]) {
            //如果有则返回
            [self.webView goBack];
            //同时设置返回按钮和关闭按钮为导航栏左边的按钮
//            if (_isDuiBa) {
//                self.navigationItem.leftBarButtonItems = @[];
//            }else{
                self.navigationItem.leftBarButtonItems = @[self.backButton];
                self.navigationItem.rightBarButtonItem = nil;
//            }
        } else {
            [self closeNative];
        }
    }
    
}

//关闭H5页面，直接回到原生页面
- (void)closeNative{
    webRefresh --;
    [self.navigationController popViewControllerAnimated:YES];
    if (!self.cancelDismiss) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (UIBarButtonItem *)backButton{
    if (!_backButton) {
        _backButton = [[UIBarButtonItem alloc] init];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *image = [UIImage imageNamed:@"nav_btn_back"];
        [btn setImage:image forState:UIControlStateNormal];
        [btn setTitle:@" 返回" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(backNative) forControlEvents:UIControlEventTouchUpInside];
        [btn.titleLabel setFont:KJWFont(17)];
        //字体的多少为btn的大小
        [btn sizeToFit];
        //左对齐
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btn.contentEdgeInsets = UIEdgeInsetsMake(0, -8, 0, 0);
        btn.frame = CGRectMake(0, 0, 45, 40);
        _backButton.customView = btn;
    }
    return _backButton;
}

- (UIBarButtonItem *)closeButton{
    if (!_closeButton) {
        _closeButton = [[UIBarButtonItem alloc] initWithTitle:@"关闭" style:UIBarButtonItemStylePlain target:self action:@selector(closeNative)];
    }
    return _closeButton;
}

- (void)shareDisease{
   
//    NSMutableDictionary *param = @{}.mutableCopy;
//    param[@"diseaseId"] = @(self.diseaseId);
//    [NT_CollegeHandler KnowledgeDiseaseModelShareParams:param success:^(id responseData) {
//        self.sharePanel.title = [NSString stringWithFormat:@"百科·%@", responseData[@"diseaseName"]];
//        self.sharePanel.content = [NSString stringWithFormat:@"%@\n多发人群：%@", responseData[@"description"], responseData[@"sufferer"]];
//        self.sharePanel.qrcodeUrl = responseData[@"downloadUrl"];
//        
//        
//        [self setupShareUI];
//        
//        kWeakSelf
//        [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
//            // 根据获取的platformType确定所选平台进行下一步操作
//            if (platformType == UMSocialPlatformType_WechatTimeLine) {
//                [DYCommonTool appUserTrackDataPage:@"012" event:@"012002"];
//            }else if (platformType == UMSocialPlatformType_WechatSession){
//                [DYCommonTool appUserTrackDataPage:@"012" event:@"012001"];
//            }
////            [weakSelf shareDiseaseToPlatformType:platformType];
//        }];
//    } failure:^(NSError *error) {
//    }];
}

- (void)shareVipCard{
    [self setupShareUI];
    @weakify(self)
    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
        // 根据获取的platformType确定所选平台进行下一步操作
        NSDictionary *dict = @{
                               @"title" : @"送您一张99甜橙卡",
                               @"detail": @"价值99元健康大礼包免费领\n最高可折49元现金红包喔~",
                               @"imgUrl": [UIImage imageNamed:@"share_vip"],
                               @"webUrl": [NSString stringWithFormat:@"https://web.jwdb.tech/plus/index.html#/sweetOrange?shareUnionId=%@", [JW_UserModel sharedInstance].unionId]
                               };
        @strongify(self)
        [self shareWebPageToPlatformType:platformType shareData:dict];
    }];
}

#pragma mark ------分享漫画---------
- (void)shareCartoon{

    [self configShareData];
    
    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
        
        [self shareWebPageToPlatformType:platformType];
    }];
    
}

- (void)configShareData{
    
//    self.cartoonSharePanel.scharePanelType = NT_SchoolSharePanelTypeCartoon;
//    self.cartoonSharePanel.cartoonModel = self.cartoonModel;
    
}

- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType{
    
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    UMShareImageObject *shareObject = [[UMShareImageObject alloc] init];
//    shareObject.shareImage = [self.cartoonSharePanel generateSharedImage];
    messageObject.shareObject = shareObject;
    
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType
                                        messageObject:messageObject
                                currentViewController:self
                                           completion:^(id data, NSError *error) {
                                               if (error) {
                                                   if (error.code == UMSocialPlatformErrorType_Cancel) {
                                                       [DYHUDView showOnlyTextHUD:@"取消分享"];
                                                   }else{
                                                       [DYHUDView showOnlyTextHUD:@"分享失败"];
                                                   }
                                                   UMSocialLogInfo(@"************Share fail with error %@*********",error);
                                               }else{
                                                   [DYHUDView showOnlyTextHUD:@"分享成功"];
                                                   
                                                   if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                                                       UMSocialShareResponse *resp = data;
                                                       //分享结果消息
                                                       UMSocialLogInfo(@"response message is %@",resp.message);
                                                       //第三方原始返回的数据
                                                       UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                                                   }else{
                                                       UMSocialLogInfo(@"response data is %@",data);
                                                   }
                                               }
                                           }];
    
    
}

- (void)setupShareUI{
    
    [UMSocialUIManager setShareMenuViewDelegate:self];
    
    [self configSharePanelUI];
    
    [UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_WechatSession),@(UMSocialPlatformType_WechatTimeLine),]];
    
    [UMSocialGlobal shareInstance].isUsingHttpsWhenShareContent = NO;
}
- (void)configSharePanelUI {
    [UMSocialShareUIConfig shareInstance].shareTitleViewConfig.isShow = NO;
    [UMSocialShareUIConfig shareInstance].sharePageScrollViewConfig.shareScrollViewPageMaxColumnCountForPortraitAndBottom = 2;
    [UMSocialShareUIConfig shareInstance].sharePageScrollViewConfig.shareScrollViewBackgroundColor = [UIColor whiteColor];
    [UMSocialShareUIConfig shareInstance].sharePageScrollViewConfig.shareScrollViewPageBGColor = [UIColor whiteColor];
    [UMSocialShareUIConfig shareInstance].sharePageScrollViewConfig.shareScrollViewPageMaxItemIconWidth = 50;
    [UMSocialShareUIConfig shareInstance].sharePageScrollViewConfig.shareScrollViewPageMaxItemIconHeight = 50;
    [UMSocialShareUIConfig shareInstance].sharePageScrollViewConfig.shareScrollViewPageMaxItemSpaceBetweenIconAndName = 5;
    [UMSocialShareUIConfig shareInstance].shareCancelControlConfig.shareCancelControlBackgroundColor = [UIColor whiteColor];
    [UMSocialShareUIConfig shareInstance].shareCancelControlConfig.shareCancelControlBackgroundColorPressed = [UIColor whiteColor];
    [UMSocialShareUIConfig shareInstance].shareCancelControlConfig.shareCancelControlTextColor = KJWHexRGB(0xFF8824);
    [UMSocialShareUIConfig shareInstance].shareCancelControlConfig.shareCancelControlText = @"取  消";
    [UMSocialShareUIConfig shareInstance].shareCancelControlConfig.shareCancelControlTextFont = KJWFont(16);
    
    [UMSocialShareUIConfig shareInstance].sharePlatformItemViewConfig.sharePlatformItemViewPlatformNameColor = KJWHexRGB(0x4A4A4A);
    
    //去掉毛玻璃效果
    [UMSocialShareUIConfig shareInstance].shareContainerConfig.isShareContainerHaveGradient = NO;
}

- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType shareData:(NSDictionary *)shareData{

    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    if (shareData[@"title"]) {
        //创建网页内容对象
        UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:shareData[@"title"] descr:shareData[@"detail"]  thumImage:shareData[@"imgUrl"]];
        //设置网页地址
        shareObject.webpageUrl = shareData[@"webUrl"];
        messageObject.shareObject = shareObject;
    }
    else {
        UMShareImageObject *shareObject = [[UMShareImageObject alloc] init];
        shareObject.shareImage = shareData[@"imgUrl"];
    }
    
    //调用分享接口
    [self shareObject:messageObject platformType:platformType];
  
    
}


- (void)shareDiseaseToPlatformType:(UMSocialPlatformType)platformType{
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    UMShareImageObject *shareObject = [[UMShareImageObject alloc] init];
//    shareObject.shareImage = [self.sharePanel generateSharedImage];
    messageObject.shareObject = shareObject;
    
//    messageObject.shareObject = shareObject;
    //调用分享接口
    [self shareObject:messageObject platformType:platformType];
    
}

- (void)shareObject:(UMSocialMessageObject *)messageObject platformType:(UMSocialPlatformType)platformType {
    [[UMSocialManager defaultManager] shareToPlatform:platformType
                                        messageObject:messageObject
                                currentViewController:self
                                           completion:^(id data, NSError *error) {
                                               if (error) {
                                                   if (error.code == UMSocialPlatformErrorType_Cancel) {
                                                       [DYHUDView showOnlyTextHUD:@"取消分享"];
                                                   }else{
                                                       [DYHUDView showOnlyTextHUD:@"分享失败"];
                                                   }
                                                   UMSocialLogInfo(@"************Share fail with error %@*********",error);
                                               }else{
                                                   [DYHUDView showOnlyTextHUD:@"分享成功"];
                                                   
                                                   if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                                                       UMSocialShareResponse *resp = data;
                                                       //分享结果消息
                                                       UMSocialLogInfo(@"response message is %@",resp.message);
                                                       //第三方原始返回的数据
                                                       UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                                                   }else{
                                                       UMSocialLogInfo(@"response data is %@",data);
                                                   }
                                               }
                                           }];
    switch (platformType) {
        case UMSocialPlatformType_WechatSession:
            //            [DYCommonTool appUserTrackDataPage:@"039" event:@"000002"];
            break;
        case UMSocialPlatformType_WechatTimeLine:
            //            [DYCommonTool appUserTrackDataPage:@"039" event:@"000003"];
            break;
        default:
            break;
    }
}
- (void)UMSocialShareMenuViewDidDisappear{
    
    if (self.shareType == WebViewControllerShareTypeCartoon) {
        
//        [self.cartoonSharePanel removeFromSuperview];
        
    }else if (self.shareType == WebViewControllerShareTypeTips){
    
        
    }else{
    
        if (self.diseaseId == 0) {
            return;
        }
//        [self.sharePanel removeFromSuperview];
    }
    
}
- (void)UMSocialShareMenuViewDidAppear{
    
    if (self.shareType == WebViewControllerShareTypeCartoon) {
    
        CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
        animation.duration = 0.3;
        NSMutableArray *values = [NSMutableArray array];
        [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
        [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1, 1.1, 1.0)]];
        [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
        animation.values = values;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [self.view.window addSubview:self.cartoonSharePanel];
//            [self.cartoonSharePanel.layer addAnimation:animation forKey:nil];
        });
        
    }else if (self.shareType == WebViewControllerShareTypeTips){
        
        
    }else{
        
        if (self.diseaseId == 0) {
            return;
        }
        CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
        animation.duration = 0.3;
        NSMutableArray *values = [NSMutableArray array];
        [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
        [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1, 1.1, 1.0)]];
        [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
        animation.values = values;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [self.view.window addSubview:self.sharePanel];
//            [self.sharePanel.layer addAnimation:animation forKey:nil];
        });
    }
    
}



- (UIBarButtonItem *)shareDiseaseBtn{
    UIBarButtonItem *barBtn = [[UIBarButtonItem alloc] init];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *image = [UIImage imageNamed:@"btn_fenxiang"];
    [btn setImage:image forState:UIControlStateNormal];
    
    if (self.shareType == WebViewControllerShareTypeCartoon) { // 分享漫画
    
        [btn addTarget:self action:@selector(shareCartoon) forControlEvents:(UIControlEventTouchUpInside)];
        
    }else if (self.shareType == WebViewControllerShareTypeTips){ // 分享文章
        
        
    }else{
        
        if (self.isVipCard) {
            
            [btn addTarget:self action:@selector(shareVipCard) forControlEvents:UIControlEventTouchUpInside];
        }else {
            
            [btn addTarget:self action:@selector(shareDisease) forControlEvents:UIControlEventTouchUpInside];
        }
        
    }
    
    btn.frame = CGRectMake(0, 0, 44, 44);
    btn.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    barBtn.customView = btn;
    return barBtn;
}



//- (PC_SharePanel *)sharePanel{
//
//    if (!_sharePanel) {
//
//        self.sharePanel = [[PC_SharePanel alloc] initWithFrame:[UIScreen mainScreen].bounds];
//    }
//
//    return _sharePanel;
//}
//
//
//- (NT_MasterVideoSharePanel *)cartoonSharePanel{
//
//    if (_cartoonSharePanel == nil) {
//
//        _cartoonSharePanel = [[NT_MasterVideoSharePanel alloc] initWithFrame:[UIScreen mainScreen].bounds];
//
//    }
//
//    return _cartoonSharePanel;
//
//}





@end
