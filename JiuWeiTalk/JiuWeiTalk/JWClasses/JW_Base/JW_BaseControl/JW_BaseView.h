//
//  JW_BaseView.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol JW_BaseViewModelProtocol;
NS_ASSUME_NONNULL_BEGIN
@protocol JW_BaseViewProtocol <NSObject>

@optional

- (instancetype)initWithViewModel:(id<JW_BaseViewModelProtocol>)viewmodel;
- (void)jw_bindViewModel;
- (void)jw_addSubViews;

@end


@interface JW_BaseView : UIView<JW_BaseViewProtocol>

@end

NS_ASSUME_NONNULL_END
