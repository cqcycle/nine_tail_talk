//
//  JW_BaseTableViewHeaderFooterView.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/5.
//  Copyright © 2019 jiuwei. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol JW_BaseTableViewHeaderFooterViewProtocol <NSObject>

@optional
- (void)jw_addSubViews;
+ (NSString *)jw_headerFooterReuseIdentifier;

@end

@interface JW_BaseTableViewHeaderFooterView : UITableViewHeaderFooterView<JW_BaseTableViewHeaderFooterViewProtocol>

@end

NS_ASSUME_NONNULL_END
