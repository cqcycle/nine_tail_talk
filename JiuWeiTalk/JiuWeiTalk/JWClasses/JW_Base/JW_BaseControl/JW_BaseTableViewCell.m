//
//  JW_BaseTableViewCell.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "JW_BaseTableViewCell.h"

@implementation JW_BaseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self jw_addSubViews];
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self jw_addSubViews];
    }
    return self;
}
- (void)jw_addSubViews {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}
+ (NSString *)jw_cellReuseIdentifier {
    return [NSString stringWithFormat:@"%@reuseIdentifier",NSStringFromClass([self class])];
}

@end
