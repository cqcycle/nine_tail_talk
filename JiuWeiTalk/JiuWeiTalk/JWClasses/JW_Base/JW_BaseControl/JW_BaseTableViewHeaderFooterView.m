//
//  JW_BaseTableViewHeaderFooterView.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/5.
//  Copyright © 2019 jiuwei. All rights reserved.
//

#import "JW_BaseTableViewHeaderFooterView.h"

@implementation JW_BaseTableViewHeaderFooterView

-(void)awakeFromNib {
    [super awakeFromNib];
    [self jw_addSubViews];
}
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        [self jw_addSubViews];
    }
    return self;
}

- (void)jw_addSubViews {};
+ (NSString *)jw_headerFooterReuseIdentifier {    
    return [NSString stringWithFormat:@"%@reuseIdentifier",NSStringFromClass([self class])];
}
@end
