//
//  JW_BaseView.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "JW_BaseView.h"

@implementation JW_BaseView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self jw_addSubViews];
        [self jw_bindViewModel];
    }
    return self;
}
- (instancetype)initWithViewModel:(id<JW_BaseViewModelProtocol>)viewmodel {
    if (self = [super init]) {
        [self jw_addSubViews];
        [self jw_bindViewModel];
    }
    return self;
}
#pragma mark -- JW_BaseViewProtocol
/**
 绑定数据
 */
- (void)jw_bindViewModel {
    
}
/**
 添加子控件
 */
- (void)jw_addSubViews{
    
}
@end
