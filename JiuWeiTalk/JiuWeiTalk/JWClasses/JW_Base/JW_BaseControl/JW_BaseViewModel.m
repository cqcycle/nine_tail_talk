//
//  JW_BaseViewModel.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "JW_BaseViewModel.h"

@implementation JW_BaseViewModel

+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    JW_BaseViewModel *viewmodel = [super allocWithZone:zone];
    if (viewmodel) {
        [viewmodel jw_initialize];
    }
    return viewmodel;
}
/*
 初始化
 */
- (void)jw_initialize {
    
}

/*
 根据model生成viewModel
 */
- (instancetype)initWithModel:(id)model {
    if (self = [super init]) {
        
    }
    return self;
}
@end
