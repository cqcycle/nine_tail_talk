//
//  JW_BaseViewController.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "JW_BaseViewController.h"

@interface JW_BaseViewController ()

@end

@implementation JW_BaseViewController

+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    JW_BaseViewController *viewController = [super allocWithZone:zone];
    @weakify(viewController)
    [[viewController rac_signalForSelector:@selector(viewDidLoad)] subscribeNext:^(RACTuple * _Nullable x) {
        @strongify(viewController)
        [viewController jw_addSubViews];
        [viewController jw_layoutNavigation];
        [viewController jw_bindViewModel];
    }];
    [[viewController rac_signalForSelector:@selector(viewWillAppear:)] subscribeNext:^(RACTuple * _Nullable x) {
        @strongify(viewController)
        [viewController jw_getNewData];
    }];
//    [[viewController rac_signalForSelector:@selector(viewDidAppear:)] subscribeNext:^(id x) {
//        [[BaiduMobStat defaultStat] pageviewStartWithName:NSStringFromClass([self class])];
//
//    }];
//    [[viewController rac_signalForSelector:@selector(viewDidDisappear:)] subscribeNext:^(id x) {
//        [[BaiduMobStat defaultStat] pageviewEndWithName:NSStringFromClass([self class])];
//
//    }];

    return viewController;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [DYHUDView hiddenHud];
    [self.view endEditing:YES];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
}
/**
 检测是否应该登录，未登录则跳转到登录页面
 */
- (BOOL)jw_shouldLogin {
    BOOL status = NO;
    if ([JW_UserModel isLogin]) {
        status = YES;
    } else {
        //没登录
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"前往登陆");
        });
    }
    return status;
}
- (void)dealloc {
    NSLog(@"%@_______dealloc",self);
}
#pragma mark -- JW_BaseViewControllerProtocol
///添加子控件
- (void)jw_addSubViews{};
///设置navigation
- (void)jw_layoutNavigation{};
///初次获取数据
- (void)jw_getNewData{};
///绑定数据
- (void)jw_bindViewModel{};

@end
