//
//  JW_BaseViewModel.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol JW_BaseViewModelProtocol<NSObject>

@optional
- (instancetype)initWithModel:(id)model;

- (void)jw_initialize;

@end


@interface JW_BaseViewModel : NSObject<JW_BaseViewModelProtocol>

@end

