//
//  JW_BaseTableViewCell.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol JW_BaseTableViewCellProtocol <NSObject>

@optional
- (void)jw_addSubViews;
+ (NSString *)jw_cellReuseIdentifier;

@end

@interface JW_BaseTableViewCell : UITableViewCell<JW_BaseTableViewCellProtocol>

@end

NS_ASSUME_NONNULL_END
