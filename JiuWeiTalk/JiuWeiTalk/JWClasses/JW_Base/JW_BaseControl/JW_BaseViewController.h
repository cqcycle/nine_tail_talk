//
//  JW_BaseViewController.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JW_BaseViewModel.h"
@protocol JW_BaseViewModelProtocol;

NS_ASSUME_NONNULL_BEGIN

@protocol JW_BaseViewControllerProtocol <NSObject>

@optional
///添加子控件
- (void)jw_addSubViews;
///设置navigation
- (void)jw_layoutNavigation;
///初次获取数据
- (void)jw_getNewData;
///绑定数据
- (void)jw_bindViewModel;


@end



@interface JW_BaseViewController : UIViewController<JW_BaseViewControllerProtocol>

/**
 检测是否应该登录，未登录则跳转到登录页面
 */
- (BOOL)jw_shouldLogin;



@end

NS_ASSUME_NONNULL_END
