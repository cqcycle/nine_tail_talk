//
//  WebViewController.h
//  JiaBan
//
//  Created by 程睿 on 2017/9/4.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<WebKit/WebKit.h>


typedef NS_ENUM(NSInteger, WebViewControllerShareType){

    WebViewControllerShareTypeCartoon = 1,  // 分享漫画
    WebViewControllerShareTypeTips    = 2,  // 分享小知识
    
};



@class NT_CartoonModel;
@class NT_SubTipsModel;


typedef void (^EnterMainViewBlock)(void);
typedef void(^payCallBack)(NSString *code);
typedef void(^weixinCallBack)(NSString *code);

@interface WebViewController : UIViewController
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, copy) NSString *titleString;
@property (nonatomic, copy) NSString *webUrl;
@property (nonatomic, assign) BOOL isTabView; // 是否是tab
@property (nonatomic, assign) BOOL hideHeader; // 是否隐藏导航栏
@property (nonatomic, assign) BOOL beginFromTop;//是否顶到状态栏
@property (nonatomic, copy) EnterMainViewBlock enterMainViewBlock;
@property (nonatomic, copy) payCallBack payCall;
@property (nonatomic, copy) weixinCallBack weixinCall;
@property (nonatomic, assign) BOOL cancelDismiss;
@property (nonatomic, assign) NSInteger diseaseId; // 疾病百科
@property (nonatomic, assign) BOOL isDisease;
@property (nonatomic, assign) BOOL isVipCard; // VIP 活动页

/// 分享类型
@property(nonatomic, assign) WebViewControllerShareType shareType;

/// 漫画
@property(nonatomic, strong) NT_CartoonModel *cartoonModel;

/// 保险小知识
@property(nonatomic, strong) NT_SubTipsModel *tipsModel;

- (instancetype)initWithTitle:(NSString *)title webUrl:(NSString *)url;


@end
