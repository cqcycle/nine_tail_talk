//
//  NT_UMengShareHelper.m
//  NineTails
//
//  Created by 程睿 on 2019/7/26.
//  Copyright © 2019 dayou. All rights reserved.
//

#import "NT_UMengShareHelper.h"

@implementation NT_UMengShareHelper

+ (void)setupShareMenuWithUMDelegate:(id<UMSocialShareMenuViewDelegate>)delegate{
    [UMSocialUIManager setShareMenuViewDelegate:delegate];
    
    [self configSharePanelUI];
    
    [UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_WechatSession),@(UMSocialPlatformType_WechatTimeLine),]];
    
    [UMSocialGlobal shareInstance].isUsingHttpsWhenShareContent = NO;
}

+ (void)configSharePanelUI {
    [UMSocialShareUIConfig shareInstance].shareTitleViewConfig.isShow = NO;
    [UMSocialShareUIConfig shareInstance].sharePageScrollViewConfig.shareScrollViewPageMaxColumnCountForPortraitAndBottom = 2;
    [UMSocialShareUIConfig shareInstance].sharePageScrollViewConfig.shareScrollViewBackgroundColor = [UIColor whiteColor];
    [UMSocialShareUIConfig shareInstance].sharePageScrollViewConfig.shareScrollViewPageBGColor = [UIColor whiteColor];
    [UMSocialShareUIConfig shareInstance].sharePageScrollViewConfig.shareScrollViewPageMaxItemIconWidth = 50;
    [UMSocialShareUIConfig shareInstance].sharePageScrollViewConfig.shareScrollViewPageMaxItemIconHeight = 50;
    [UMSocialShareUIConfig shareInstance].sharePageScrollViewConfig.shareScrollViewPageMaxItemSpaceBetweenIconAndName = 5;
    [UMSocialShareUIConfig shareInstance].shareCancelControlConfig.shareCancelControlBackgroundColor = [UIColor whiteColor];
    [UMSocialShareUIConfig shareInstance].shareCancelControlConfig.shareCancelControlBackgroundColorPressed = [UIColor whiteColor];
    [UMSocialShareUIConfig shareInstance].shareCancelControlConfig.shareCancelControlTextColor = KJWHexRGB(0xFF8824);
    [UMSocialShareUIConfig shareInstance].shareCancelControlConfig.shareCancelControlText = @"取  消";
    [UMSocialShareUIConfig shareInstance].shareCancelControlConfig.shareCancelControlTextFont = KJWFont(16);
    
    [UMSocialShareUIConfig shareInstance].sharePlatformItemViewConfig.sharePlatformItemViewPlatformNameColor = KJWHexRGB(0x4A4A4A);
    
    //去掉毛玻璃效果
    [UMSocialShareUIConfig shareInstance].shareContainerConfig.isShareContainerHaveGradient = NO;
}

@end
