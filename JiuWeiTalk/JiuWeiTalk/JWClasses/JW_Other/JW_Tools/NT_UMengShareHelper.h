//
//  NT_UMengShareHelper.h
//  NineTails
//
//  Created by 程睿 on 2019/7/26.
//  Copyright © 2019 dayou. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "PC_SharePanel.h"
//#import "NT_CaseSharePanel.h"
#import <UShareUI/UShareUI.h>

NS_ASSUME_NONNULL_BEGIN

@interface NT_UMengShareHelper : NSObject

+ (void)setupShareMenuWithUMDelegate:(id<UMSocialShareMenuViewDelegate>)delegate;

@end

NS_ASSUME_NONNULL_END
