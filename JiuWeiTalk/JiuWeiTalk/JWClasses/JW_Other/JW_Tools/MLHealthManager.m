//
//  MLHealthManager.m
//  iphoneHealth
//
//  Created by LiuZhiwei on 16/4/12.
//  Copyright © 2016年 smartdot.mau. All rights reserved.
//

#import "MLHealthManager.h"
#import <HealthKit/HealthKit.h>

@interface MLHealthManager ()
{
    BOOL _stepOneSuccess;
    BOOL _stepTwoSuccess;
}

@property (nonatomic, strong) NSMutableArray *healthSteps;
@property (nonatomic, strong) NSMutableArray *healthCalories;
@property (nonatomic, strong) NSMutableArray *healthDistances;
@property (nonatomic, strong) HKHealthStore  *store;

@end

@implementation MLHealthManager

- (HKHealthStore *)store
{
    if (!_store) {
        _store = [[HKHealthStore alloc] init];
    }
    return _store;
}

+ (instancetype)sharedInstance
{
    
    static MLHealthManager *model;
    //    YYCache *cache = [YYCache cacheWithName:Cache];
    //    if ([cache containsObjectForKey:Cache_JW_UserModel] == NO) {
    //        return nil;
    //    }
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        model = [[MLHealthManager alloc]init];
    });
    return model;
}

- (void)getIphoneHealthDataCompletion:(void (^)(BOOL success))completion withAlwaysAlert:(BOOL)shouldAlert withController:(UIViewController *)ctrl{
    
    
    
    
    self.healthSteps = [NSMutableArray array];
    self.healthDistances = [NSMutableArray array];
    self.healthCalories = [NSMutableArray array];

    NSSet *getData;
    
    // 1.判断设备是否支持HealthKit框架
    if ([HKHealthStore isHealthDataAvailable]) {
        getData = [self getData];
    }
    else {
        NSLog(@"---------不支持 HealthKit 框架");
        [DYHUDView showAttentionHUD:@"您的设备不支持健康数据功能"];
        return;
    }
    
    // 2.请求苹果健康的认证
    [self.store requestAuthorizationToShareTypes:nil readTypes:getData completion:^(BOOL success, NSError * _Nullable error) {
        if (!success) {
            NSLog(@"--------请求苹果健康认证失败");
            if (completion) {
                completion(NO);
            }
            return ;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            // 3.获取苹果健康数据
            
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//            [PC_MeRequset getSportSportDataModelLastSportRecordParams:params success:^(id responseData) {
//                if ([responseData isEqualToString:@""]) {
//                    [self getHealthStepData:[[NSDate date] dateByAddingDays:-89] andCompletion:^(BOOL success) {
//                        if (completion) {
//                            completion(success);
//                        }
//                    }];
//                    [self getHealthDistanceData:[[NSDate date] dateByAddingDays:-89] andCompletion:^(BOOL success) {
//                        if (completion) {
//                            completion(success);
//                        }
//                    }];
//                }else{
//                    NSDateFormatter *format = [[NSDateFormatter alloc] init];
//                    [format setDateFormat:@"yyyy-MM-dd"];
//                    NSDate *beginDate = [format dateFromString:responseData];
//                    
//                    [self getHealthStepData:beginDate andCompletion:^(BOOL success) {
//                        if (completion) {
//                            completion(success);
//                        }
//                    }];
//                    [self getHealthDistanceData:beginDate andCompletion:^(BOOL success) {
//                        if (completion) {
//                            completion(success);
//                        }
//                    }];
//                }
//            } failure:^(NSError *error) {
//                
//            }];
            
        });
    }];
    
}

+ (NSInteger)calcDaysFromBegin:(NSDate *)beginDate end:(NSDate *)endDate
{
    // 创建日期格式化对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    // 取两个日期对象的时间间隔
    // 这里的 NSTimeInterval 并不是对象，是基本型，其实是double 类型，是由 C 定义的: typedef double NSTimeInterval;
    NSTimeInterval time = [endDate timeIntervalSinceDate:beginDate];
    int days = ((int)time)/(3600 * 24);
    return days;
}

- (NSSet *)getData{
    HKQuantityType  *step = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount];
    
    HKQuantityType *distance = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceWalkingRunning];
    
    return [NSSet setWithObjects:step,distance, nil];
}


//- (void)updateStep{
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    NSDate *now = [NSDate date];
//    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
//    NSDate *startDate = [calendar dateFromComponents:components];
//    NSDate *endDate = [calendar dateByAddingUnit:NSCalendarUnitDay value:1 toDate:startDate options:0];
//
//    HKQuantityType *sampleType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount];
//    NSPredicate *predicate = [HKQuery predicateForSamplesWithStartDate:startDate endDate:endDate options:HKQueryOptionStrictStartDate];
//
//    HKStatisticsQuery *query = [[HKStatisticsQuery alloc] initWithQuantityType:sampleType quantitySamplePredicate:predicate options:HKStatisticsOptionCumulativeSum completionHandler:^(HKStatisticsQuery *query, HKStatistics *result, NSError *error) {
//        //        if (!result) {
//        //            if (completionHandler) {
//        //                completionHandler(0.0f, error);
//        //            }
//        //            return;
//        //        }
//
//        double totalStep = [result.sumQuantity doubleValueForUnit:[HKUnit countUnit]];
//        NSLog(@" ***************步数统计是：%f",totalStep);
//    }];
//
//    [store executeQuery:query];
//}

//- (void)updateDistance{
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    NSDate *now = [NSDate date];
//    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
//    NSDate *startDate = [calendar dateFromComponents:components];
//    NSDate *endDate = [calendar dateByAddingUnit:NSCalendarUnitDay value:1 toDate:startDate options:0];
//    
//    HKQuantityType *sampleType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceWalkingRunning];
//    NSPredicate *predicate = [HKQuery predicateForSamplesWithStartDate:startDate endDate:endDate options:HKQueryOptionStrictStartDate];
//    
//    HKStatisticsQuery *query = [[HKStatisticsQuery alloc] initWithQuantityType:sampleType quantitySamplePredicate:predicate options:HKStatisticsOptionCumulativeSum completionHandler:^(HKStatisticsQuery *query, HKStatistics *result, NSError *error) {
//        //        if (!result) {
//        //            if (completionHandler) {
//        //                completionHandler(0.0f, error);
//        //            }
//        //            return;
//        //        }
//        
//        double totalStep = [result.sumQuantity doubleValueForUnit:[HKUnit meterUnit]];
//        NSLog(@" -------------距离统计是：%f",totalStep);
//    }];
//    
//    [store executeQuery:query];
//}

- (void)updateEnergy{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *now = [NSDate date];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
    NSDate *startDate = [calendar dateFromComponents:components];
    NSDate *endDate = [calendar dateByAddingUnit:NSCalendarUnitDay value:1 toDate:startDate options:0];
    
    HKQuantityType *sampleType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierDietaryEnergyConsumed];
    NSPredicate *predicate = [HKQuery predicateForSamplesWithStartDate:startDate endDate:endDate options:HKQueryOptionStrictStartDate];
    
    HKStatisticsQuery *query = [[HKStatisticsQuery alloc] initWithQuantityType:sampleType quantitySamplePredicate:predicate options:HKStatisticsOptionCumulativeSum completionHandler:^(HKStatisticsQuery *query, HKStatistics *result, NSError *error) {
        //        if (!result) {
        //            if (completionHandler) {
        //                completionHandler(0.0f, error);
        //            }
        //            return;
        //        }
        
//        double totalStep = [result.sumQuantity doubleValueForUnit:[HKUnit calorieUnit]];
//        NSLog(@" -------------卡路里统计是：%f",totalStep);
    }];
    
    [_store executeQuery:query];
}

- (void)getHealthStepData:(NSDate *)startDay andCompletion:(void (^)(BOOL success))completion{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    // 设置时间支持单位
    NSDateComponents *anchorComponents =
    [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth |
     NSCalendarUnitYear | NSCalendarUnitWeekday fromDate:[NSDate date]];
    
    NSDate *anchorDate = [calendar dateFromComponents:anchorComponents];
    // 获取数据的截止时间 今天
    NSDate *endDate = [NSDate date];
    // 获取数据的起始时间 此处取从今日往前推100天的数据
    NSDate *startDate = startDay;
    
    // 数据类型
    HKQuantityType *type = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount];
    // Your interval: sum by hour
    NSDateComponents *intervalComponents = [[NSDateComponents alloc] init];
    intervalComponents.day = 1;
    // Example predicate 用于获取设置时间段内的数据
    NSPredicate *predicate = [HKQuery predicateForSamplesWithStartDate:startDate endDate:endDate options:HKQueryOptionStrictStartDate];
    
    HKStatisticsCollectionQuery *query = [[HKStatisticsCollectionQuery alloc] initWithQuantityType:type quantitySamplePredicate:predicate options:HKStatisticsOptionCumulativeSum anchorDate:anchorDate intervalComponents:intervalComponents];
    
    query.initialResultsHandler = ^(HKStatisticsCollectionQuery *query, HKStatisticsCollection *result, NSError *error) {
        
        for (HKStatistics *sample in [result statistics]) {
//            NSLog(@"--------------%@ 至 %@ : %@", sample.startDate, sample.endDate, sample.sumQuantity);
            NSDate *date = [sample.endDate dateByAddingDays:-1];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd"];
            NSString *dateTime = [formatter stringFromDate:date];
            
            double totalStep = [sample.sumQuantity doubleValueForUnit:[HKUnit countUnit]];
            NSString *appleHealth = @"com.apple.Health";
            
            double editStep  = 0.0;
            for (HKSource *source in sample.sources) {
                if ([source.bundleIdentifier isEqualToString:appleHealth]) {
                    // 获取用户自己添加的数据 并减去，防止用户手动刷数据
                    HKSource *healthSource = source;
                    editStep  = [[sample sumQuantityForSource:healthSource] doubleValueForUnit:[HKUnit countUnit]];
                }
            }
            
            NSInteger step = (NSInteger)totalStep - (NSInteger)editStep;
            NSString *value = [NSString stringWithFormat:@"%ld",step];
            
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
                                 dateTime,@"dateTime",
                                 value,@"value",nil];
            [self.healthSteps addObject:dic];
//            NSLog(@"gaizaoDateStyle:%@  Dic = %@",self.healthSteps,dic);
        }
        self.healthCalories = self.healthSteps;
        
//        NSDictionary *healthSteps = [NSDictionary dictionaryWithObjectsAndKeys:
//                                     self.healthSteps,@"healthSteps",
//                                     self.healthCalories,@"healthCalories",nil];
//        NSLog(@"改造数据格式：%@",healthSteps);
        self->_stepOneSuccess = YES;
        if (self->_stepOneSuccess&&self->_stepTwoSuccess) {
            [self makeDataCompletion:^(BOOL success) {
                if (completion) {
                    completion(success);
                }
            }];
        }
    };
    
    [_store executeQuery:query];
}

- (void)getHealthDistanceData:(NSDate *)startDay andCompletion:(void (^)(BOOL success))completion{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *anchorComponents =
    [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth |
     NSCalendarUnitYear | NSCalendarUnitWeekday fromDate:[NSDate date]];
    NSDate *anchorDate = [calendar dateFromComponents:anchorComponents];
    NSDate *endDate = [NSDate date];
    NSDate *startDate = startDay;
    
    HKQuantityType *type = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceWalkingRunning];
    
    // Your interval: sum by hour
    NSDateComponents *intervalComponents = [[NSDateComponents alloc] init];
    intervalComponents.day = 1;
    
    // Example predicate
    NSPredicate *predicate = [HKQuery predicateForSamplesWithStartDate:startDate endDate:endDate options:HKQueryOptionStrictStartDate];
    
    HKStatisticsCollectionQuery *query = [[HKStatisticsCollectionQuery alloc] initWithQuantityType:type quantitySamplePredicate:predicate options:HKStatisticsOptionCumulativeSum anchorDate:anchorDate intervalComponents:intervalComponents];
    query.initialResultsHandler = ^(HKStatisticsCollectionQuery *query, HKStatisticsCollection *result, NSError *error) {
        for (HKStatistics *sample in [result statistics]) {
//            NSLog(@"+++++++++++++++%@ 至 %@ : %@", sample.startDate, sample.endDate, sample.sumQuantity);
            NSDate *date = [sample.endDate dateByAddingDays:-1];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd"];
            NSString *dateTime = [formatter stringFromDate:date];
            
            double totalDistance = [sample.sumQuantity doubleValueForUnit:[HKUnit meterUnit]];
            
            NSString *appleHealth = @"com.apple.Health";
            //            double floor = [sample.sumQuantity doubleValueForUnit:[HKUnit yardUnit]];
            double editDistance  = 0.0;
            for (HKSource *source in sample.sources) {
                if ([source.bundleIdentifier isEqualToString:appleHealth]) {
                    // 获取用户自己添加的数据 并减去，防止用户手动刷数据
                    HKSource *healthSource = source;
                    editDistance = [[sample sumQuantityForSource:healthSource] doubleValueForUnit:[HKUnit meterUnit]];
                }
            }
            
            double distance = totalDistance - editDistance;
            NSString *value = [NSString stringWithFormat:@"%0.f",distance];
            
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
                                 dateTime,@"dateTime",
                                 value,@"value",nil];
            [self.healthDistances addObject:dic];
        }
        
        NSLog(@"改造距离格式：%@",self.healthDistances);
        self->_stepTwoSuccess = YES;
        if (self->_stepOneSuccess&&self->_stepTwoSuccess) {
            [self makeDataCompletion:^(BOOL success) {
                if (completion) {
                    completion(success);
                }
            }];
        }
    };
    
    [_store executeQuery:query];
}

- (void)makeDataCompletion:(void (^)(BOOL success))completion
{
    _stepOneSuccess = NO;
    _stepTwoSuccess = NO;
    NSMutableArray *newArr = [NSMutableArray array];
    if (self.healthSteps.count && self.healthDistances.count && self.healthSteps.count == self.healthDistances.count) {
        for (int i = 0; i < self.healthSteps.count; i++) {
            NSDictionary *dict1 = self.healthSteps[i];
            NSDictionary *dict2 = self.healthDistances[i];
            NSDictionary *newDict = @{@"sportTime":dict1[@"dateTime"], @"stepNumber":dict1[@"value"], @"walkDistance":dict2[@"value"]};
            [newArr addObject:newDict];
        }
    }
    
    if (newArr.count == 0) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"健康数据权限尚未开启" message:@"去设置->隐私->健康->九尾多保中开启" preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"去设置" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//
//        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"好" style:UIAlertActionStyleCancel handler:nil]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[DYCommonTool topViewController] presentViewController:alert animated:YES completion:nil];
        });
    }else{
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        params[@"data"] = [newArr modelToJSONString];
//        [PC_MeRequset getSportSportDataModelSaveSportParams:params success:^(id responseData) {
//            kUserDef
//            [ud setObject:[NSDate date] forKey:@"SynchronizeTime"];
//            [ud setObject:[newArr lastObject] forKey:@"todayData"];
//            [ud setBool:YES forKey:@"hasAlertHealth"];
//            completion(YES);
//        } failure:^(NSError *error) {
//            
//        }];
    }
    
}

@end
