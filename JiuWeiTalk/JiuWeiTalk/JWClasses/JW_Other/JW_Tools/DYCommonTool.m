//
//  CommonTool.m
//  tongrenbao
//
//  Created by XuLi on 15/3/23.
//  Copyright (c) 2015年 XuLi. All rights reserved.
//

#import "DYCommonTool.h"
#import "DYRequestData.h"
#import <SDWebImage/SDImageCache.h>
#import <CommonCrypto/CommonDigest.h>
#import <YYKeychain.h>
#import <Accelerate/Accelerate.h>
#import <AddressBook/AddressBook.h>
//#import "UserContactModel.h"
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <SystemConfiguration/CaptiveNetwork.h>
#import "UserDataEventsModel.h"
#import "UserDataContentModel.h"
#import "UserDataOverAllModel.h"
#import "UserEvent.h"
//#import "WLAlertView.h"
//#import "FMDeviceManager.h"
//#import "PC_LoginViewController.h"
#import "NT_LoginLunchViewController.h"
#import "PC_LoginRequest.h"

@interface DYCommonTool ()
@end

static dispatch_queue_t _queue;

@implementation DYCommonTool
/**
 * 判断手机号是否有效
 */
+ (BOOL)isValidatePhone:(NSString *)mobileString {
    if (mobileString == nil) {
        return NO;
    }
    NSMutableString *testString=[NSMutableString stringWithString:mobileString];
    NSRegularExpression *regularexpression = [[NSRegularExpression alloc]
                                              initWithPattern:@"^(13[0-9]|15[0-3]|15[5-9]|145|147|177|18[0-9]|170)[0-9]{8}$"
                                              options:NSRegularExpressionCaseInsensitive
                                              error:nil];
    NSUInteger numberofMatch = [regularexpression numberOfMatchesInString:testString
                                                                  options:NSMatchingReportProgress
                                                                    range:NSMakeRange(0, testString.length)];
    
    if(numberofMatch > 0)
    {
        return YES;
    } else {
        return NO;
    }
}

/**
 *  验证邮箱是否合法
 */
+ (BOOL)validateEmail:(NSString *)candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:candidate];
}

//身份证号
+ (BOOL)validateIdentityCard:(NSString *)identityCard {
    BOOL flag;
    if (identityCard.length <= 0) {
        flag = NO;
        return flag;
    }
    NSString *regex2 = @"^(\\d{14}|\\d{17})(\\d|[xX])$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];
    return [identityCardPredicate evaluateWithObject:identityCard];
}

/**
 *  计算文本size
 *
 *  @param text    内容
 *  @param font    字体
 *  @param maxSize 最大长度
 *
 *  @return size
 */
+ (CGSize)getTextSizeWithText:(NSString *)text font:(UIFont *)font maxSize:(CGSize)maxSize
{
    CGSize size = CGSizeZero;
    
    NSDictionary *attribute = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    size = [text boundingRectWithSize:maxSize options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    return size;
}

+ (NSString *)md5HexDigest:(NSString*)input {
    const char *cStr = [input UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

/**
 图片高斯模糊处理
 
 @param image 原图片
 @param blur 模糊系数
 @return 处理后图片
 */
+(UIImage *)boxblurImage:(UIImage *)image withBlurNumber:(CGFloat)blur{
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage= [CIImage imageWithCGImage:image.CGImage];
    //设置filter
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:@(blur) forKey: @"inputRadius"];
    //模糊图片
    CIImage *result=[filter valueForKey:kCIOutputImageKey];
    CGImageRef outImage=[context createCGImage:result fromRect:[inputImage extent]];
    UIImage *blurImage=[UIImage imageWithCGImage:outImage];
    CGImageRelease(outImage);
    return blurImage;
    
}

+ (NSString *)getCarrier{
    CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = info.subscriberCellularProvider;
//    DYLog(@"运营商:%@", carrier.carrierName);
    return carrier.carrierName;
}

+ (NSDictionary *)currentWifiSSID{
    // Does not work on the simulator.
    NSArray *ifs = (id)CFBridgingRelease(CNCopySupportedInterfaces());
//    DYLog(@"ifs:%@",ifs);
    NSDictionary *info;
    for (NSString *ifnam in ifs) {
        info = (id)CFBridgingRelease(CNCopyCurrentNetworkInfo((CFStringRef)ifnam));
//        DYLog(@"dici：%@",[info allKeys]);
        if (info[@"SSID"])
            break;
    }
    return info;
}

+ (NSString *)getIpAddress{
    //alternative  http://2018.ip138.com/ic.asp
    NSURL *ipURL = [NSURL URLWithString:@"http://ip.taobao.com/service/getIpInfo.php?ip=myip"];
    NSData *data = [NSData dataWithContentsOfURL:ipURL];
    if (!data) {
        return @"";
    }
    NSDictionary *ipDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    NSString *ipStr = nil;
    if (ipDic && [ipDic[@"code"] integerValue] == 0) { //获取成功
        ipStr = ipDic[@"data"][@"ip"];
    }
    return (ipStr ? ipStr : @"");
}

+ (NSString *)getCurrentTime{
    return [[NSNumber numberWithLongLong:[[NSDate date] timeIntervalSince1970]] stringValue];
}

+ (void)appUserTrackDataPage:(NSString *)page event:(NSString *)event{
    
    dispatch_async(self.queue, ^{
        NSString *time = [DYCommonTool getCurrentTime];
        UserDataEventsModel *eventsModel = [[UserDataEventsModel alloc]initWithEvent:event page:page time:time];
        UserDataContentModel *contentModel = [[UserDataContentModel alloc]initWithEvents:eventsModel];
        UserDataOverAllModel *overallModel = [[UserDataOverAllModel alloc]initWithContent:contentModel];
        
        NSDictionary *param = @{@"u_vs":[overallModel modelToJSONString]};
        
        [DYRequestData getMobileAppUserTrackParams:param.mutableCopy success:nil failure:nil];
        //将当前事件保存，作为下一事件的前置事件，同时存入缓存，为下次启动使用
        [UserEvent sharedUserEvent].page = page;
        [UserEvent sharedUserEvent].event = event;
        [UserEvent sharedUserEvent].time = time;
        
        NSDictionary *cacheEvent = @{
                                     @"page" : page,
                                     @"event" : event,
                                     @"time" : time,
                                     @"uuid" : [UserEvent sharedUserEvent].uuid
                                     };
        [UserEvent saveLastEventInCache:cacheEvent];
    });
}

+ (dispatch_queue_t)queue{
    if (!_queue) {
        _queue = dispatch_queue_create("com.jbrj.usertrack", DISPATCH_QUEUE_SERIAL);
    }
    return _queue;
}

// 周末
+ (BOOL)dateIsWeekend:(NSDate *)date{
    if (date.weekday == 7 || date.weekday == 1) {
        return YES;
    }else{
        return NO;
    }
}

// 获取当前最顶层VC
+ (UIViewController *)topViewController {
    UIViewController *resultVC;
    resultVC = [self _topViewController:[[UIApplication sharedApplication].keyWindow rootViewController]];
    while (resultVC.presentedViewController) {
        resultVC = [self _topViewController:resultVC.presentedViewController];
    }
    return resultVC;
}

+ (UIViewController *)_topViewController:(UIViewController *)vc {
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [self _topViewController:[(UINavigationController *)vc topViewController]];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        return [self _topViewController:[(UITabBarController *)vc selectedViewController]];
    } else {
        return vc;
    }
    return nil;
}

// float 转换为金额格式
+ (NSString *)moneyFromFloat:(float)money{
    return [NSString stringWithFormat:@"%.2f", money];
}

// 计算小时工资
+ (NSString *)calculateHourlyRateWithBasicSalary:(NSString *)basicSalary{
    float basicSalaryFloat = [basicSalary floatValue];
    float hourlyRate = basicSalaryFloat / 21.75 / 8;
    return [self moneyFromFloat:hourlyRate];
}

// 登录后淡入淡出更换rootViewController
+ (void)restoreRootViewController:(UIViewController *)rootViewController
{
    typedef void (^Animation)(void);
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    
    rootViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    Animation animation = ^{
        BOOL oldState = [UIView areAnimationsEnabled];
        [UIView setAnimationsEnabled:NO];
        [UIApplication sharedApplication].keyWindow.rootViewController = rootViewController;
        [UIView setAnimationsEnabled:oldState];
    };
    
    [UIView transitionWithView:window
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:animation
                    completion:nil];
}

+ (BOOL)currentUserHasLogin{
    if ([JW_UserModel sharedInstance].userId) {
        return YES;
    }else{
        [self alertLoginVC];
        return NO;
    }
}

/**
 获取订制的 MJRefreshGifHeader
 */
+ (MJRefreshGifHeader *)PC_getRefreshGifHeader{
    MJRefreshGifHeader *header = [[MJRefreshGifHeader alloc]init];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *beginImgArray = [NSMutableArray array];
    NSMutableArray *refreshingImgArray = [NSMutableArray array];
    
    UIImage *resizedImg = [[UIImage imageNamed:@"50"] imageByResizeToSize:CGSizeMake(40, 40)];
    [beginImgArray addObject:resizedImg];
    
    for (int i = 50; i>0; i--) {
        UIImage *resizedImg = [[UIImage imageNamed:@(i).stringValue] imageByResizeToSize:CGSizeMake(40, 40)];
        [refreshingImgArray addObject:resizedImg];
    }
    for (int i = 1; i<=50; i++) {
        UIImage *resizedImg = [[UIImage imageNamed:@(i).stringValue] imageByResizeToSize:CGSizeMake(40, 40)];
        [refreshingImgArray addObject:resizedImg];
    }
    
    [header setImages:refreshingImgArray duration:2.0 forState:MJRefreshStateIdle];
    [header setImages:beginImgArray duration:2.0 forState:MJRefreshStatePulling];
    [header setImages:refreshingImgArray duration:2.0 forState:MJRefreshStateRefreshing];

    return header;
}


/**
 文字转表情
 */
+ (NSAttributedString *)replaceStrToExpression:(NSString *)contentStr AndLabel:(UILabel *)label
{
    if (contentStr == nil) {
        contentStr = @"";
    }
    NSMutableArray *dataArray = [NSMutableArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Expression.plist" ofType:nil]];
    NSMutableDictionary *expressionDict = [[NSMutableDictionary alloc] init];
    for (NSDictionary *dict in dataArray) {
        expressionDict[dict[@"title"]] = dict[@"image"];
    }
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:contentStr];
    NSRegularExpression * regularExpression = [NSRegularExpression regularExpressionWithPattern:@"\\[[\u4e00-\u9fa5]{2}\\]" options:NSRegularExpressionCaseInsensitive error:nil];
    NSArray * checkingResult = [regularExpression matchesInString:contentStr options:NSMatchingReportCompletion range:NSMakeRange(0, contentStr.length)];
    //遍历数组., 获取匹配成功的字符串的范围 NSRange (因为替换后, 字符串的长度在改变,所以使用倒序来进行替换,来避免范围越界问题)
    for (NSInteger i = checkingResult.count-1  ; i<checkingResult.count; i--) {
        
        //获取range
        NSTextCheckingResult *result = checkingResult[i];
        
        
        //创建一个文本附件
        NSTextAttachment * textAttachment = [[NSTextAttachment alloc]init];
        
        //设置NSTextAttachment 大小
        
        textAttachment.bounds = CGRectMake(0, 0, label.font.lineHeight, label.font.lineHeight);
        /*重写这个方法就能设置附件的大小
         - (CGRect)attachmentBoundsForTextContainer:(nullable NSTextContainer *)textContainer proposedLineFragment:(CGRect)lineFrag glyphPosition:(CGPoint)position characterIndex:(NSUInteger)charIndex NS_AVAILABLE(10_11, 7_0)
         {
         return (CGRect){0,-3,lineFrag.size.height,lineFrag.size.height};
         }*/
        
        //通过获取的NSRange 来截取子字符串
        NSString *str = [contentStr substringWithRange:NSMakeRange(result.range.location + 1, 2)];
        
        NSString *imgStr = expressionDict[str];
        
        textAttachment.image = [UIImage imageNamed:imgStr];
        
        NSAttributedString * att = [NSAttributedString attributedStringWithAttachment:textAttachment];
        
        //创建一个可变的属性字符串
        
        [attStr replaceCharactersInRange:result.range withAttributedString:att];
        
        
    }
    
    return attStr;
}

//+ (void)initFMDeviceInfo{
//
//    // 获取设备管理器实例
//    FMDeviceManager_t *manager = [FMDeviceManager sharedManager];
//
//    // 准备SDK初始化参数
//    NSMutableDictionary *options = [NSMutableDictionary dictionary];
//
//    /*
//     * SDK具有防调试功能，当使用xcode运行时(开发测试阶段),请取消下面代码注释，
//     * 开启调试模式,否则使用xcode运行会闪退。上架打包的时候需要删除或者注释掉这
//     * 行代码,如果检测到调试行为就会触发crash,起到对APP的保护作用
//     */
//#ifdef DEBUG
//    // 上线Appstore的版本，请记得删除此行，否则将失去防调试防护功能！
//    [options setValue:@"allowd" forKey:@"allowd"];
//
//    // 指定对接同盾的测试环境，正式上线时，请删除或者注释掉此行代码，切换到同盾生产环境
//    [options setValue:@"sandbox" forKey:@"env"];
//#endif
//    // 此处已经替换为您的合作方标识了
//    [options setValue:@"dykj" forKey:@"partner"];
//
////    void (^callbackdata)(NSString *blackBox) = ^(NSString *blackBox){
////
////        printf("同盾设备指纹,回调函数获取到的blackBox:%s\n",[blackBox UTF8String]);
////
////        completion(blackBox);
////
////    };
////    [options setObject:callbackdata forKey:@"callback"];
//
//    // 使用上述参数进行SDK初始化
//    manager->initWithOptions(options);
//}

//+ (NSString *)getFMDeviceInfo{
//    // 获取设备管理器实例
//    FMDeviceManager_t *manager = [FMDeviceManager sharedManager];
//
//    /*
//     * 获取设备指纹黑盒数据，请确保在应用开启时已经对SDK进行初始化，切勿在get的时候才初始化
//     * 如果此处获取到的blackBox特别长(超过400字节)，说明初始化尚未完成(一般需要1-3秒)，或者由于网络问题导致初始化失败，进入了降级处理
//     * 降级不影响正常设备信息的获取，只是会造成blackBox字段超长，且无法获取设备真实IP
//     * 降级数据平均长度在2KB以内,一般不超过3KB,数据的长度取决于采集到的设备信息的长度,无法100%确定最大长度
//     */
//    NSString *blackBox = manager->getDeviceInfo();
//    NSLog(@"同盾设备指纹数据: %@", blackBox);
//
//    return blackBox;
//}

+ (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font maxSize:(CGSize)maxSize
{
    NSDictionary *attri = @{NSFontAttributeName:font};
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attri context:nil].size;
}

+ (NSArray<NSValue *> *)generateRandomPointsWithRange:(NSInteger)max andCount:(NSInteger)count{
    NSMutableArray<NSValue *> *points = [NSMutableArray arrayWithCapacity:count];
    
    CGFloat spacing = 1.0;
    
    for (int i=0; i<count; i++) {
        CGFloat x = arc4random() % (max*100) / 100.0;
        CGFloat y = arc4random() % (max*100) / 100.0;
        if (![self _isPointOnTheCircleCenter:CGPointMake(max/2.0, max/2.0) radius:max/2.0 point:CGPointMake(x, y)]) {
            i--;
            continue;
        }
        BOOL exist = NO;
        for (int j=0; j<points.count; j++) {
            CGPoint temp = points[j].CGPointValue;
            if (fabs(x - temp.x) < spacing && fabs(y - temp.y) < spacing) {
                exist = YES;
                i--;
                break;
            }
        }
        if (!exist) {
            [points addObject:[NSValue valueWithCGPoint:CGPointMake(x, y)]];
        }
    }
    
    return points;
}
+ (BOOL)_isPointOnTheCircleCenter:(CGPoint)center radius:(CGFloat)radius point:(CGPoint)point{
    CGFloat centerX = center.x;
    CGFloat centerY = center.y;
    CGFloat pointX = point.x;
    CGFloat pointY = point.y;
    
    CGFloat pointRadius = sqrt(pow(pointX-centerX, 2) + pow(pointY-centerY, 2));
    
    if (pointRadius > radius - 0.5 && pointRadius <= radius + 0.5)
        return YES;
    else
        return NO;
    
}

+ (void)shakeAnimationForView:(UIView*)view {
    
    CALayer *layerOfView = [view layer];
    CGPoint positionOfView = [layerOfView position];
    CGPoint endPosition = CGPointMake(positionOfView.x-5, positionOfView.y);
    CGPoint startPosition = CGPointMake(positionOfView.x+5, positionOfView.y);
    CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [animation setFromValue:[NSValue valueWithCGPoint:startPosition]];
    [animation setToValue:[NSValue valueWithCGPoint:endPosition]];
    [animation setAutoreverses:YES];
    [animation setDuration:0.05];
    [animation setRepeatCount:3];
    [layerOfView addAnimation:animation forKey:nil];
}

+ (void)radiusAndShadow:(UIView *)view
{
    view.layer.cornerRadius = 8;
    [view jw_setLayerShadow:KJWHexRGBA(0x9B9B9B, 0.4) offset:CGSizeMake(0, 2) radius:4];
}

+ (void)alertLoginVC
{
    [DYHUDView showOnlyTextHUD:@"请先登录"];
    NT_LoginLunchViewController *vc = [[NT_LoginLunchViewController alloc] init];
    if ([DEVICE_NAME containsString:@"iPad"]) {
        vc.isPad = YES;
    }
    
    BaseNavController *navVC = [[BaseNavController alloc] initWithRootViewController:vc];
    [[self topViewController] presentViewController:navVC animated:YES completion:nil];
    
}

+ (NSString *)filterHTML:(NSString *)html
{
    NSScanner * scanner = [NSScanner scannerWithString:html];
    NSString * text = nil;
    while([scanner isAtEnd]==NO)
    {
        [scanner scanUpToString:@"<" intoString:nil];
        [scanner scanUpToString:@">" intoString:&text];
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>",text] withString:@""];
    }
    return html;
}

+ (NSInteger)calcDaysFromBegin:(NSDate *)beginDate end:(NSDate *)endDate
{
    // 创建日期格式化对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    // 取两个日期对象的时间间隔
    // 这里的 NSTimeInterval 并不是对象，是基本型，其实是double 类型，是由 C 定义的: typedef double NSTimeInterval;
    NSTimeInterval time = [endDate timeIntervalSinceDate:beginDate];
    int days = ((int)time)/(3600 * 24);
    return days;
}

//app阅读行为
+ (void)recordArticleWithEventType:(NSInteger)eventType andArticleId:(NSString *)articleId andArticleType:(NSInteger)articleType andLocation:(NSInteger)location
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"eventType"] = @(eventType);
    params[@"articleId"] = articleId;
    params[@"articleType"] = @(articleType);
    params[@"location"] = @(location);
    [PC_LoginRequest getAppAppReadModelAddParams:params success:^(id responseData) {
    } failure:^(NSError *error) {
    }];
}

//修改label行间距
+ (NSAttributedString *)lineSpacingWithLabel:(UILabel *)label andSpacing:(NSInteger)lineSpacing
{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = lineSpacing;
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    [attributes setObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
    [attributes setObject:KJWFont(14) forKey:NSFontAttributeName];
    [attributes setObject:KJWHexRGB(0x262626) forKey:NSForegroundColorAttributeName];
    return [[NSAttributedString alloc] initWithString:label.text attributes:attributes];
}


+ (NSString *)getSandBoxFilePathWithFileName:(NSString *)fileName{
    if (fileName) {
        return [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingString:fileName];
    }
    
    return @"";
    
}

+ (void)deleteWebCache{
    //all WebsiteDataTypes清除所有缓存
    NSSet *websiteDataTypes = [WKWebsiteDataStore allWebsiteDataTypes];
    
    NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
    
    [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
        
    }];
}
@end

