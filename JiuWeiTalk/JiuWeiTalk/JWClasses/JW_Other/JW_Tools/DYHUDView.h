//
//  DYHUDView.h
//  NineTails
//
//  Created by zp on 2017/3/31.
//  Copyright © 2017年 zp. All rights reserved.
//

#import <Foundation/Foundation.h>

static CGFloat MBDuration = 1.2;

@class MBProgressHUD;

@interface DYHUDView : NSObject

+(UIView *)window;
// 只有文字
+ (void)showOnlyTextHUD:(NSString *)labelText;
// 成功
+ (void)showSuccessHUD:(NSString *)labeltext;

+ (void)showErrorHUD:(NSString *)labeltext Duration:(CGFloat)duration;
// 失败
+ (void)showErrorHUD:(NSString *)labeltext;
// 网络请求失败
+ (void)showNetWorkError;

// 警告
+ (void)showAttentionHUD:(NSString *)labelText;

+ (void)showCustomHUD:(NSString *)labeltext imageview:(NSString *)imageName;

+ (MBProgressHUD*)showHUDWithStr:(NSString*)title dim:(BOOL)dim;
//
+ (void)hiddenHud;

+ (void)showCustomHUD:(NSString *)labeltext imageview:(NSString *)imageName toView:(UIView *)view;

//-订制-转菊花
/** 订制-转菊花 */
+ (MBProgressHUD *)showIndicatorView;

//转菊花
/** 默认转菊花*/
+ (MBProgressHUD *)showDefaultIndicatorView;

@end
