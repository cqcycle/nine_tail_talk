//
//  RegularTool.h
//  TimeRent
//
//  Created by Jiada on 17/1/19.
//  Copyright © 2017年 channce. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum {
    kTelephoneNumber = 0,
    kEmail,
} RegularType;


@interface RegularTool : NSObject


+ (BOOL)checkTelephoneNumber:(NSString *)telephoneNumber;//手机号码格式
+ (BOOL)checkIdentityCard: (NSString *)identityCard;     //身份证号格式
+ (BOOL)checkEmailStr:(NSString *)emailStr;              //检查邮箱
+ (BOOL)verifyIDCardNumber:(NSString *)IDCardNumber;     //身份证号全校验
+ (BOOL)checkCardNo:(NSString*)cardNo;                   //
+ (BOOL)isIncludeSpecialCharact: (NSString *)str;        //检查是否有特殊符号
+ (BOOL)checkUserName : (NSString *) userName;           //检查昵称
+ (BOOL)isNum:(NSString *)checkedNumString;              //是否为数字
+ (NSString *)imageBase64WithDataURL:(UIImage *)image;
+ (BOOL)judgePassWordLegal:(NSString *)pass;/**1，不能全部是数字
                                            2，不能全部是字母
                                            3，必须是数字或字母*/
/**
 *  支付宝返回字段解析
 *
 *  @param AllString            字段
 *  @param FirstSeparateString  第一个分离字段的词
 *  @param SecondSeparateString 第二个分离字段的词
 *
 *  @return 返回字典
 */
+(NSDictionary *)VEComponentsStringToDic:(NSString*)AllString withSeparateString:(NSString *)FirstSeparateString AndSeparateString:(NSString *)SecondSeparateString;


@end
