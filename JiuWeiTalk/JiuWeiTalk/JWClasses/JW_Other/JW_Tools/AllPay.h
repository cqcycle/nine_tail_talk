//
//  AllPay.h
//  beautifulDaDa
//
//  Created by Jiada on 17/6/30.
//  Copyright © 2017年 Jiada. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^OrederNoBlcok)(BOOL isSuccess);
@interface AllPay : NSObject

+ (void)payUnionpaypayWithTn:(NSString *)tn;
+ (void)payTreasurepayWithPayOrder:(NSString *)payOrder orderNo:(OrederNoBlcok)orderno;
+ (void)payWeChatPayWithAppid:(NSString *)appid AndNoncestr:(NSString *)noncestr AndPackage:(NSString *)package AndPartnerid:(NSString *)partnerid AndPrepayid:(NSString *)prepayid AndTimestamp:(NSString *)timestamp AndSign:(NSString *)sign AndPackagestr:(NSString *)packagestr;


@end
