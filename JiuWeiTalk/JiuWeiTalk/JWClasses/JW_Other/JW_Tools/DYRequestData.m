//
//  DYRequestData.m
//  NineTails
//
//  Created by zp on 2017/3/30.
//  Copyright © 2017年 zp. All rights reserved.
//

#import "DYRequestData.h"

@implementation DYRequestData

/**
 用户行为数据统计
 */
+ (void)getMobileAppUserTrackParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure{
    [DYNetworkHelper POST:MobileAppUserTrack parameters:params showFailureMsg:NO success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}
/**
 开屏广告、强制更新、首页弹窗
 
 */
+ (void)getMobileAppGetAdByTypeParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure{
    [DYNetworkHelper POST:MobileAppGetAdByType parameters:params showFailureMsg:NO success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}
/**
 上传文件，带标识tag
 
 @param params 入参
 @param name 图片参数名
 @param image 图片数据
 @param success 成功回调
 @param failure 失败回调
 */
+ (void)getMobileFileUploadFileSecParams:(NSMutableDictionary *)params name:(NSString *)name image:(UIImage *)image success:(SuccessBlock)success failure:(FailureBlock)failure{
    
    [DYNetworkHelper uploadImagesWithURL:MobileFileUploadFileSec
                              parameters:params
                                    name:name
                                  images:[NSArray arrayWithObject:image]
                               fileNames:nil
                              imageScale:0.5f
                               imageType:nil
                                progress:nil
                                 success:^(id responseObject) {
                                     if (success) {
                                         success(responseObject);
                                     }
                                 } failure:^(NSError *error) {
                                     if (failure) {
                                         failure(error);
                                     }
                                 }];
    
}
@end
