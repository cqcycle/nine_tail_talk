//
//  CommonTool.h
//  tongrenbao
//
//  Created by XuLi on 15/3/23.
//  Copyright (c) 2015年 XuLi. All rights reserved.
//

static NSInteger const bigInteger = 9999;
static NSInteger webRefresh = bigInteger;

@interface DYCommonTool : NSObject



+ (BOOL)isValidatePhone:(NSString *)mobileString;

/**
 *  验证邮箱是否合法
 */
+ (BOOL)validateEmail:(NSString *)candidate;

/**
 *  验证身份
 */
+ (BOOL)validateIdentityCard:(NSString *)identityCard;

/**
 *  计算文本size
 *
 *  @param text    内容
 *  @param font    字体大小
 *  @param maxSize 最大值
 *
 *  @return size
 */
+ (CGSize)getTextSizeWithText:(NSString *)text font:(UIFont *)font maxSize:(CGSize)maxSize;

/**
 *  md5加密
 *
 *  @param input 输入内容
 *
 *  @return 返回md5值
 */
+ (NSString *)md5HexDigest:(NSString*)input;
/**
 *  图片高斯模糊处理
 *
 *  @param image 原图片
 *  @param blur 模糊系数
 *  @return 处理后图片
 */
+ (UIImage *)boxblurImage:(UIImage *)image withBlurNumber:(CGFloat)blur;

/**
 获取运营商信息
 */
+ (NSString *)getCarrier;
/**
 获取WIFI名
 */
+ (NSDictionary *)currentWifiSSID;
/**
 获取IP地址
 */
+ (NSString *)getIpAddress;
/**
 获取当前时间戳
 */
+ (NSString *)getCurrentTime;
/**
 用户行为数据统计
 */
+ (void)appUserTrackDataPage:(NSString *)page event:(NSString *)event;
/**
 是否为周末
 */
+ (BOOL)dateIsWeekend:(NSDate *)date;
/**
 获取当前最顶层VC
 */
+ (UIViewController *)topViewController;
/**
 金额
 */
+ (NSString *)moneyFromFloat:(float)money;
/**
 计算基本工资
 */
+ (NSString *)calculateHourlyRateWithBasicSalary:(NSString *)basicSalary;
/**
 淡入淡出切换rootViewController
 */
+ (void)restoreRootViewController:(UIViewController *)rootViewController;

/**
 获取订制的 MJRefreshGifHeader
 */
+ (MJRefreshGifHeader *)PC_getRefreshGifHeader;

/**
 是否已登录
 */
+ (BOOL)currentUserHasLogin;

/**
 文字转表情
 */
+ (NSAttributedString *)replaceStrToExpression:(NSString *)contentStr AndLabel:(UILabel *)label;
//计算文字高度
+ (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font maxSize:(CGSize)maxSize;

//生成随机坐标位置数组 (去重)
+ (NSArray<NSValue *> *)generateRandomPointsWithRange:(NSInteger)max andCount:(NSInteger)count;

//为view执行抖动动画
+ (void)shakeAnimationForView:(UIView*)view;

//加圆角阴影
+ (void)radiusAndShadow:(UIView *)view;

//登录页
+ (void)alertLoginVC;

//去除html标签
+ (NSString *)filterHTML:(NSString *)html;

+ (NSInteger)calcDaysFromBegin:(NSDate *)beginDate end:(NSDate *)endDate;

//app阅读行为
+ (void)recordArticleWithEventType:(NSInteger)eventType andArticleId:(NSString *)articleId andArticleType:(NSInteger)articleType andLocation:(NSInteger)location;

//修改label行间距
+ (NSAttributedString *)lineSpacingWithLabel:(UILabel *)label andSpacing:(NSInteger)lineSpacing;

//沙盒文件路径，参数nil时返回空
+ (NSString *)getSandBoxFilePathWithFileName:(NSString *)fileName;

//清除wkwebview缓存
+ (void)deleteWebCache;
@end
