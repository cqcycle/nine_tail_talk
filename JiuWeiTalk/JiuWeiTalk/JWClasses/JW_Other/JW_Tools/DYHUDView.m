//
//  DYHUDView.m
//  NineTails
//
//  Created by zp on 2017/3/31.
//  Copyright © 2017年 zp. All rights reserved.
//

#import "DYHUDView.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import <UIImage+GIF.h>

#define Kerror     @"hud_error"
#define Ksuccess   @"hud_success"
#define KAttention @"hud_attention"

@implementation DYHUDView

+ (UIView *)window {
    return [AppDelegate sharedAppDelegate].window;
}

MBProgressHUD *HUD;

// 只有文字
+ (void)showOnlyTextHUD:(NSString *)labelText {
    [self showCustomHUD:labelText imageview:nil];
}

+ (void)showAttentionHUD:(NSString *)labelText
{
    [self showCustomHUD:labelText imageview:KAttention];
}

+ (void)showSuccessHUD:(NSString *)labeltext
{
    [self showCustomHUD:labeltext imageview:Ksuccess];
}

+ (void)showErrorHUD:(NSString *)labeltext
{
    [self showCustomHUD:labeltext imageview:Kerror];
}

+ (void)showNetWorkError{
    [self showErrorHUD:@"网络请求失败"];
}

+ (void)showErrorHUD:(NSString *)labeltext Duration:(CGFloat)Duration
{
    [self showCustomHUD:labeltext imageview:Kerror time:Duration];
}

+ (void)showCustomHUD:(NSString *)labeltext imageview:(NSString *)imageName
{
    [self showCustomHUD:labeltext imageview:imageName time:MBDuration];
}

+ (void)showCustomHUD:(NSString *)labeltext imageview:(NSString *)imageName time:(CGFloat)time
{
    [self showHUDWithStr:labeltext dim:NO];
    if (imageName) {
        HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        HUD.mode = MBProgressHUDModeCustomView;
    }else{
        HUD.mode = MBProgressHUDModeText;
    }
    [HUD hide:YES afterDelay:time];
}


+ (void)showCustomHUD:(NSString *)labeltext imageview:(NSString *)imageName toView:(UIView *)view {
    [self showHUDWithStr:labeltext toView:view dim:NO];
    if (imageName) {
        HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        HUD.mode = MBProgressHUDModeCustomView;
    }else{
        HUD.mode = MBProgressHUDModeText;
    }
    [HUD hide:YES afterDelay:MBDuration];
}


+ (MBProgressHUD*)showHUDWithStr:(NSString*)title dim:(BOOL)dim
{
    if (!title) {
        title = @"未返回报错信息";
    }
    [self hiddenHud];
    HUD = [MBProgressHUD showHUDAddedTo:[self window] animated:YES];
    [HUD setUserInteractionEnabled:dim];
    if (title.length > 15) {
        [HUD setDetailsLabelFont:KJWFont(14)];
        [HUD setDetailsLabelText:title];
    }else{
        [HUD setLabelText:title];
    }
    
    return HUD;
}

+ (void)hiddenHud
{
    [MBProgressHUD hideHUDForView:[self window] animated:NO];
}

+ (MBProgressHUD*)showHUDWithStr:(NSString*)title toView:(UIView *)view dim:(BOOL)dim
{
    [self hiddenHud];
    HUD = [MBProgressHUD showHUDAddedTo:view animated:YES];
    [HUD setUserInteractionEnabled:dim];
    if (title.length > 15) {
        [HUD setDetailsLabelFont:KJWFont(14)];
        [HUD setDetailsLabelText:title];
    }else{
        [HUD setLabelText:title];
    }
    
    return HUD;
}

+ (MBProgressHUD *)showIndicatorView{
    [self hiddenHud];
    HUD = [MBProgressHUD showHUDAddedTo:[self window] animated:YES];
    HUD.mode = MBProgressHUDModeCustomView;
    UIImageView *customIndecator = [[UIImageView alloc]initWithImage:[UIImage sd_animatedGIFNamed:@"loading"]];
    customIndecator.size = CGSizeMake(80, 80);
    [customIndecator jw_setCornerRadius:5];
    HUD.customView = customIndecator;
    HUD.color = [UIColor clearColor];
    [HUD setUserInteractionEnabled:NO];
    return HUD;
}

+ (MBProgressHUD *)showDefaultIndicatorView{
    [self hiddenHud];
    HUD.mode = MBProgressHUDModeIndeterminate;
    HUD = [MBProgressHUD showHUDAddedTo:[self window] animated:YES];
    [HUD setUserInteractionEnabled:NO];
    return HUD;
}
@end
