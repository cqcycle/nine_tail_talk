//
//  DYRequestData.h
//  NineTails
//
//  Created by zp on 2017/3/30.
//  Copyright © 2017年 zp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DYNetworkHelper.h"
#import "DYNetworkCache.h"

//----用户行为数据----
#define MobileAppUserTrack                  KJWJoinString(@"app/app-point-model/add")
// 开屏广告、强制更新、首页弹窗
#define MobileAppGetAdByType                KJWJoinString(@"mobile/app/getAdByType")
// 上传文件，带标识
#define MobileFileUploadFileSec             KJWJoinString(@"mobile/file/uploadFileSec")


typedef void(^FailureBlock)(NSError *error);                // 请求失败的block
typedef void(^SuccessBlock)(id responseData);               // 请求成功的block
typedef void(^ResponseCacheBlock)(id responseCache);        // 缓存的block

@interface DYRequestData : NSObject

/**
 用户行为数据统计
 
 */
+ (void)getMobileAppUserTrackParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure;
/**
 开屏广告、强制更新、首页弹窗
 
 */
+ (void)getMobileAppGetAdByTypeParams:(NSMutableDictionary *)params success:(SuccessBlock)success failure:(FailureBlock)failure;

/**
 上传文件，带标识tag
 
 @param params 入参
 @param name 图片参数名
 @param image 图片数据
 @param success 成功回调
 @param failure 失败回调
 */
+ (void)getMobileFileUploadFileSecParams:(NSMutableDictionary *)params name:(NSString *)name image:(UIImage *)image success:(SuccessBlock)success failure:(FailureBlock)failure;
@end
