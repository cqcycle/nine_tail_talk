//
//  DYUUID.h
//  NineTails
//
//  Created by zp on 2017/4/5.
//  Copyright © 2017年 zp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DYUUID : NSObject
+(NSString *)getUUID;
+(NSString *)getRandomUUID;
@end
