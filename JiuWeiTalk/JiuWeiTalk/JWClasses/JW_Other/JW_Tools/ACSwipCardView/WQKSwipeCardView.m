//
//  WQKSwipeCardView.m
//  OilReading
//
//  Created by AlexCorleone on 2018/6/19.
//  Copyright © 2018年 Magic. All rights reserved.
//

#import "WQKSwipeCardView.h"


#import "NT_HomeCycleModel.h"

#import <UIImageView+WebCache.h>

@interface WQKSwipeCardView ()
<UIGestureRecognizerDelegate>

@property (nonatomic, assign) BOOL isSwipeAction;

@property(nonatomic, strong) UIImageView *playImageView;

@end

@implementation WQKSwipeCardView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setBackgroundColor:UIColor.clearColor];
        [self configSwipCardSubviews];
//        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureAction:)];
//        [self addGestureRecognizer:tapGesture];
//        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureAction:)];
//        [self addGestureRecognizer:panGesture];
    }
    return self;
}

#pragma mark - setter && getter

#pragma mark -Public Method
- (void)configSwipeCardSubviewsSizeWithViewSize:(CGSize)viewSize
{
    [_bgImageView setFrame:CGRectMake(0, 0, viewSize.width, viewSize.height)];
    
}

#pragma mark - Private Method
- (void)configSwipCardSubviews
{
    [self.layer setCornerRadius:10.];
    self.layer.masksToBounds = YES;
    self.bgImageView = [UIImageView new];
    [self addSubview:_bgImageView];
    
    UIImageView *playImageView = [[UIImageView alloc] initWithImage:KJWImageNamed(@"playlistbofang")];
    [_bgImageView addSubview:playImageView];
    [playImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.bottom.mas_equalTo(-15);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        
    }];
    
    self.playImageView = playImageView;
    
}

- (void)setCycleModel:(NT_HomeCycleModel *)cycleModel{
    
    _cycleModel= cycleModel;
    
    [self.bgImageView sd_setImageWithURL:[NSURL URLWithString:cycleModel.indexPicUrl]];
    
    if ([cycleModel.type integerValue] == 3) {
        
        self.playImageView.hidden = NO;
        
    }else{
        
        self.playImageView.hidden = YES;
    }
}



#pragma mark - UIGestureRecognizerDelegate


- (void)swipeGestureAction:(UIGestureRecognizer *)gesture
{
    CGPoint currentPoint = [gesture locationInView:self];

    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
            {
//                NSLog(@"滑动开始");
                self.isSwipeAction = YES;
                if (   self.swipeViewDelegate
                    && [self.swipeViewDelegate respondsToSelector:@selector(swipeCard:stateDidBeginWith:)])
                {
                    [self.swipeViewDelegate swipeCard:self stateDidBeginWith:currentPoint];
                }
            }
            break;
        case UIGestureRecognizerStateChanged:
            {
//                NSLog(@"滑动ing");
                if (   self.swipeViewDelegate
                    && [self.swipeViewDelegate respondsToSelector:@selector(swipeCard:stateDidChangeWith:)])
                {
                    [self.swipeViewDelegate swipeCard:self stateDidChangeWith:currentPoint];
                }
            }
            break;
        case UIGestureRecognizerStateEnded:
            {
//                NSLog(@"滑动结束");
                if (self.isSwipeAction)
                {
                    if (   self.swipeViewDelegate
                        && [self.swipeViewDelegate respondsToSelector:@selector(swipeCard:stateDidEndWith:)])
                    {
                        [self.swipeViewDelegate swipeCard:self stateDidEndWith:currentPoint];
                    }
                }else
                {
                    if (   self.swipeViewDelegate
                        && [self.swipeViewDelegate respondsToSelector:@selector(swipeCardDidClickCardView:)])
                    {
                        [self.swipeViewDelegate swipeCardDidClickCardView:self];
                        self.isSwipeAction = NO;
                    }

                }
            }
            break;
        case UIGestureRecognizerStatePossible:
            {
                NSLog(@"滑动未知");
            }
            break;
        case UIGestureRecognizerStateCancelled:
            {
                NSLog(@"滑动取消");
            }
            break;
        case UIGestureRecognizerStateFailed:
            {
                NSLog(@"滑动失败");
            }
            break;
    }
//    NSLog(@"---%@", NSStringFromCGPoint([gesture locationInView:self]));
}

@end
