//
//  CR_DashLineView.m
//  NineTails
//
//  Created by 程睿 on 2019/10/25.
//  Copyright © 2019 dayou. All rights reserved.
//

#import "CR_DashLineView.h"

@implementation CR_DashLineView

//// Only override drawRect: if you perform custom drawing.
//// An empty implementation adversely affects performance during animation.
//- (void)drawRect:(CGRect)rect {
//    // Drawing code
//    CGContextRef context = UIGraphicsGetCurrentContext(); //获取图形上下文
////    CGContextSetFillColorWithColor(context, [UIColor greenColor].CGColor); //填充色
////    CGContextFillRect(context, rect );//把整个空间用刚设置的颜色填充
//
//    CGFloat lengths[] = {3, 5}; // 实，虚，实，虚... 交替的长度
//    CGContextSetLineDash(context, 0, lengths, 2); // 第二个参数phase:起始画时跳过几个像素；第四个参数是lengths的数组长度
//    CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);//设置线的颜色
//    CGContextSetLineCap(context, kCGLineCapRound);
//
//    CGContextMoveToPoint(context, 0, 0); //画线的起始点位置
//    CGContextAddLineToPoint(context, 0, rect.size.height); //画第一条线的终点位置
//
//    CGContextStrokePath(context); //把线在界面上绘制出来
//}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
//        self.backgroundColor = [UIColor redColor];
        
        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
        [shapeLayer setFrame:self.bounds];
        [shapeLayer setFillColor:[UIColor clearColor].CGColor];
         
        //  设置虚线颜色为blackColor
        [shapeLayer setStrokeColor:KJWHexRGB(0xff8824).CGColor];
         
        //  设置虚线宽度
        [shapeLayer setLineWidth:1];
//        [shapeLayer setLineJoin:kCALineJoinRound];
        [shapeLayer setLineCap:kCALineCapRound];
         
        //  设置线宽，线间距
        [shapeLayer setLineDashPattern:@[@(2), @(5)]];
         
        //  设置路径
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathMoveToPoint(path, nil, 0, 0);
        CGPathAddLineToPoint(path, nil, 0, frame.size.height);
         
        [shapeLayer setPath:path];
        CGPathRelease(path);
         
        //  把绘制好的虚线添加上来
        [self.layer addSublayer:shapeLayer];
    }
    return self;
}

@end
