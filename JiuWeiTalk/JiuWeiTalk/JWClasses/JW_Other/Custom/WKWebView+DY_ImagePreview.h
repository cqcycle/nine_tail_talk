//
//  WKWebView+DY_ImagePreview.h
//  NineTails
//
//  Created by 程睿 on 2019/5/8.
//  Copyright © 2019 dayou. All rights reserved.
//

#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WKWebView (DY_ImagePreview)

- (void)setImgUrlArray:(NSArray *)imgUrlArray;
- (NSArray *)getImgUrlArray;
- (NSArray *)getImageUrlByJS:(WKWebView *)webView;

@end

NS_ASSUME_NONNULL_END
