//
//  UICollectionViewCellAlignmentFlowLayout.m
//  NineTails
//
//  Created by 程睿 on 2019/6/10.
//  Copyright © 2019 dayou. All rights reserved.
//

#import "UICollectionViewCellAlignmentFlowLayout.h"

@implementation UICollectionViewCellAlignmentFlowLayout

- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSArray<UICollectionViewLayoutAttributes *> *superArray = [super layoutAttributesForElementsInRect:rect];
    
    if (self.cellAlignmentType == UICollectionViewCellAlignmentTypeLeft){
        [superArray enumerateObjectsUsingBlock:^(UICollectionViewLayoutAttributes * _Nonnull layoutAttributes, NSUInteger idx, BOOL * _Nonnull stop) {
            
            if (idx > 0){
                UICollectionViewLayoutAttributes *prevLayoutAttributes = superArray[idx - 1];
                if (CGRectGetMaxX(prevLayoutAttributes.frame) + CGRectGetWidth(layoutAttributes.frame) + self.cellAlignmentSpace <= self.collectionViewContentSize.width - self.sectionInset.right){
                    CGRect currentFrame = layoutAttributes.frame;
                    currentFrame.origin.x = CGRectGetMaxX(prevLayoutAttributes.frame) + self.cellAlignmentSpace;
                    layoutAttributes.frame = currentFrame;
                }else{
//                    CGRect currentFrame = layoutAttributes.frame;
//                    currentFrame.origin.x = self.sectionInset.left;
//                    layoutAttributes.frame = currentFrame;
                }
            }else{
//                CGRect currentFrame = layoutAttributes.frame;
//                currentFrame.origin.x = self.sectionInset.left;
//                layoutAttributes.frame = currentFrame;
            }
        }];
        
    }
    else if (self.cellAlignmentType == UICollectionViewCellAlignmentTypeRight)//右对齐
    {
        [superArray enumerateObjectsUsingBlock:^(UICollectionViewLayoutAttributes * _Nonnull layoutAttributes, NSUInteger idx, BOOL * _Nonnull stop) {
            
            if (idx > 0){
                UICollectionViewLayoutAttributes *prevLayoutAttributes = superArray[idx - 1];
                
                if (CGRectGetWidth(layoutAttributes.frame) + self.minimumInteritemSpacing + self.sectionInset.left < prevLayoutAttributes.frame.origin.x){
                    CGRect currentFrame = layoutAttributes.frame;
                    currentFrame.origin.x = prevLayoutAttributes.frame.origin.x - self.minimumInteritemSpacing - CGRectGetWidth(currentFrame);
                    layoutAttributes.frame = currentFrame;
                }else{
                    CGRect currentFrame = layoutAttributes.frame;
                    currentFrame.origin.x = self.collectionViewContentSize.width - CGRectGetWidth(currentFrame) - self.sectionInset.right;
                    layoutAttributes.frame = currentFrame;
                }
            }else{
                CGRect currentFrame = layoutAttributes.frame;
                currentFrame.origin.x = self.collectionViewContentSize.width - CGRectGetWidth(currentFrame) - self.sectionInset.right;
                layoutAttributes.frame = currentFrame;
            }
        }];
    }
    return superArray;
}

@end
