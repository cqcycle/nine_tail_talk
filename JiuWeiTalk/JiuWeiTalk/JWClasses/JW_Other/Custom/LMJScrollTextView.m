//
//  LMJScrollTextView2.m
//  LMJScrollText
//
//  Created by MajorLi on 15/5/4.
//  Copyright (c) 2015年 iOS开发者公会. All rights reserved.
//
//  iOS开发者公会-技术1群 QQ群号：87440292
//  iOS开发者公会-技术2群 QQ群号：232702419
//  iOS开发者公会-议事区  QQ群号：413102158
//


#import "LMJScrollTextView.h"

#define ScrollTime 1.f

@implementation LMJScrollTextView
{
    UILabel * _scrollLabel;
    
    NSInteger _index;
    
    BOOL _needStop;
    bool _isRunning;
}

- (id)init{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20); // 设置一个初始的frame
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.clipsToBounds = YES;
        
        _index = 0;
        _needStop  = NO;
        _isRunning = NO;
        
//        _textDataArr = @[@"您好"];
        _textFont    = [UIFont systemFontOfSize:12];
        _textColor   = [UIColor blackColor];
        _scrollLabel = nil;
    }
    return self;
}

- (void)setTextFont:(UIFont *)textFont{
    _textFont = textFont;
    _scrollLabel.font = textFont;
}
- (void)setTextColor:(UIColor *)textColor{
    _textColor = textColor;
    _scrollLabel.textColor = textColor;
}


- (void)createScrollLabel{
    _scrollLabel = [[UILabel alloc] initWithFrame:CGRectMake(93, 23, self.frame.size.width - 110, self.frame.size.height)];
//    _scrollLabel.text          = @"";
    _scrollLabel.textAlignment = NSTextAlignmentLeft;
    _scrollLabel.textColor     = _textColor;
    _scrollLabel.font          = _textFont;
    [self addSubview:_scrollLabel];
    
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 70, 25)];
//    label.backgroundColor = [UIColor colorWithHexString:@"#AE894C"];
//    label.layer.cornerRadius = 3.0f;
//    label.layer.masksToBounds = YES;
//    label.text = @"公积金头条";
//    label.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
//    label.font = [UIFont systemFontOfSize:12];
//    label.textAlignment = NSTextAlignmentCenter;
//    [self addSubview:label];
    UIImageView *imgView = [[UIImageView alloc] init];
    imgView.image = [UIImage imageNamed:@"kuaixun"];
    [self addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(14);
        make.centerY.equalTo(self);
    }];
    UIImageView *dianView = [[UIImageView alloc] init];
    dianView.image = [UIImage imageNamed:@"yuandian"];
    [self addSubview:dianView];
    [dianView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_scrollLabel.mas_left).offset(-2);
        make.centerY.equalTo(self);
    }];
}


- (void)startScrollBottomToTop{
    
    if (_isRunning) return;
    if (_scrollLabel == nil) [self createScrollLabel];
    
    _index     = 0;
    _needStop  = NO;
    _isRunning = YES;
    [self scrollBottomToTop];
}

- (void)startScrollTopToBottom{
    
    if (_isRunning) return;
    if (_scrollLabel == nil) [self createScrollLabel];
    
    _index     = 0;
    _needStop  = NO;
    _isRunning = YES;
    [self scrollTopToBottom];
}

- (void)stop{
    _needStop = YES;
}



- (void)scrollBottomToTop{
    
    if (![self isCurrentViewControllerVisible:[self viewController]]) {  // 处于非当前页面
        
        [self performSelector:@selector(scrollBottomToTop) withObject:nil afterDelay:ScrollTime*3];
        
    }else{                                                               // 处于当前页面
        
        if ([self.delegate respondsToSelector:@selector(scrollTextView:currentTextIndex:)]) { // 代理回调
            [self.delegate scrollTextView:self currentTextIndex:_index];
        }
        
        _scrollLabel.frame = CGRectMake(93, self.frame.size.height, _scrollLabel.frame.size.width, _scrollLabel.frame.size.height);
        if (_textDataArr.count != 0) {
            _scrollLabel.text  = _textDataArr[_index];
        }
        
        [UIView animateWithDuration:ScrollTime animations:^{
            
            _scrollLabel.frame = CGRectMake(93, 0, _scrollLabel.frame.size.width, _scrollLabel.frame.size.height);
            
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:ScrollTime delay:ScrollTime+3 options:0 animations:^{
                
                _scrollLabel.frame = CGRectMake(93, -self.frame.size.height, _scrollLabel.frame.size.width, _scrollLabel.frame.size.height);
                
            } completion:^(BOOL finished) {
                
                _index ++;
                if (_index == _textDataArr.count) {
                    _index = 0;
                }
                
                if (_needStop == NO) {
                    [self scrollBottomToTop];
                }else{
                    _isRunning = NO;
                }
            }];
        }];
    }
}

- (void)scrollTopToBottom{
    
    if (![self isCurrentViewControllerVisible:[self viewController]]) { // 处于非当前页面
        
        [self performSelector:@selector(scrollTopToBottom) withObject:nil afterDelay:ScrollTime*3];
        
    }else{                                                              // 处于当前页面
    
        if ([self.delegate respondsToSelector:@selector(scrollTextView:currentTextIndex:)]) { // 代理回调
            [self.delegate scrollTextView:self currentTextIndex:_index];
        }
        
        _scrollLabel.frame = CGRectMake(0, -self.frame.size.height, _scrollLabel.frame.size.width, _scrollLabel.frame.size.height);
        _scrollLabel.text  = _textDataArr[_index];
        
        
        
        [UIView animateWithDuration:ScrollTime animations:^{
            
            _scrollLabel.frame = CGRectMake(0, 0, _scrollLabel.frame.size.width, _scrollLabel.frame.size.height);
            
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:ScrollTime delay:ScrollTime+3 options:0 animations:^{
                
                _scrollLabel.frame = CGRectMake(0, self.frame.size.height, _scrollLabel.frame.size.width, _scrollLabel.frame.size.height);
                
            } completion:^(BOOL finished) {
                
                _index ++;
                if (_index == _textDataArr.count) {
                    _index = 0;
                }
                
                if (_needStop == NO) {
                    [self scrollTopToBottom];
                }else{
                    _isRunning = NO;
                }
            }];
        }];
    }
}


-(BOOL)isCurrentViewControllerVisible:(UIViewController *)viewController{
    return (viewController.isViewLoaded && viewController.view.window && [UIApplication sharedApplication].applicationState == UIApplicationStateActive);
}

- (UIViewController *)viewController {
    for (UIView * next = [self superview]; next; next = next.superview) {
        UIResponder * nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}

@end
