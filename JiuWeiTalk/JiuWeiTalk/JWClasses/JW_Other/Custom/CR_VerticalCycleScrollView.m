//
//  CR_VerticalCycleScrollView.m
//  NineTails
//
//  Created by 程睿 on 2019/8/19.
//  Copyright © 2019 dayou. All rights reserved.
//

#import "CR_VerticalCycleScrollView.h"

@interface CR_VerticalCycleScrollView () <UITableViewDelegate, UITableViewDataSource> {
    NSInteger _currentRowIndex;
    NSTimeInterval _interval;
    NSString *_nibName;
}
@property (nonatomic, strong) NSArray *dataList;
@property (nonatomic, strong) NSTimer *timer;
@end

@implementation CR_VerticalCycleScrollView

+ (instancetype)cycleScrollViewWithFrame:(CGRect)frame dataSource:(NSArray *)dataSource timeInterval:(NSTimeInterval)timeInterval nibName:(nonnull NSString *)nibName{
    CR_VerticalCycleScrollView *view = [[CR_VerticalCycleScrollView alloc] initWithFrame:frame];
    view.dataSource = view;
    view.delegate = view;
    view.dataList = dataSource;
    view.rowHeight = frame.size.height;
    [view setInterval: timeInterval ? : 1.f];
    view->_nibName = nibName;
    [view registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
    view.separatorStyle = UITableViewCellSeparatorStyleNone;
    view.showsVerticalScrollIndicator = NO;
    view.scrollEnabled = NO;
    return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.dataList.count;
    } else {
        if (self.dataList.count == 0) {
            return 0;
        }
    }
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:_nibName];
    [cell setValue:_dataList[indexPath.row] forKey:@"data"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.rowClickedBlock) {
        self.rowClickedBlock(indexPath.row, self.dataList[indexPath.row]);
    }
}
#pragma mark - scrollViewDelegate
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    // 以无动画的形式跳到第1组的第0行
    if (_currentRowIndex == _dataList.count) {
        _currentRowIndex = 0;
        [self setContentOffset:CGPointZero];
    }
}

- (void)stop{
    if (_timer) {
        if (_timer) {
            [_timer invalidate];
            _timer = nil;
        }
    }
}
- (void)restart{
    [self setInterval:_interval];
}
#pragma mark - priviate method
- (void)setInterval:(NSTimeInterval)interval {
    _interval = interval;
    
    [self stop];
    // 定时器
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(scroll) userInfo:nil repeats:YES];
    _timer = timer;
}
- (void)scroll {
    _currentRowIndex++;
//    NSLog(@"CR_VerticalCycleScrollView scrolls to row: %ld", _currentRowIndex);
    [self setContentOffset:CGPointMake(0, _currentRowIndex * self.rowHeight) animated:YES];
}

- (void)dealloc{
    
}
@end



