//
//  DXQButton.h
//  NineTails
//
//  Created by zp on 2017/3/27.
//  Copyright © 2017年 zp. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ButtonImageLocation) {
    ButtonImageLeft,
    ButtonImageRight,
    ButtonImageTop,
    ButtonImageBottom
};
typedef NS_ENUM(NSInteger, FollowButtonStatus) {
    FollowButtonStatusFollow = 0,           // 关注
    FollowButtonStatusFollowed,             // 已关注，可取关
    FollowButtonStatusFollowedDisabled,     // 已关注，且不可取关
    FollowButtonStatusMutual,               // 相互关注，可取关
    FollowButtonStatusMutualDisabled        // 相互关注，且不可取关
};


@interface DXQButton : UIButton

@property (assign, nonatomic) CGFloat spacing;///图片与文字的间距 默认为0
@property (assign, nonatomic) FollowButtonStatus followBtnStatus;

/**
 *  初始化
 *
 *  @param type Image方向类型
 */
-(instancetype)initWithButtonType:(ButtonImageLocation)type;


/**
 *  返回内部button右上角Point
 */
-(CGPoint)buttonRightPoint;


@end
