//
//  DXQButton.m
//  NineTails
//
//  Created by zp on 2017/3/27.
//  Copyright © 2017年 zp. All rights reserved.
//

#import "DXQButton.h"

@interface DXQButton ()

@property(nonatomic , assign) ButtonImageLocation type;//Image方向类型

@end


@implementation DXQButton

-(instancetype)initWithButtonType:(ButtonImageLocation)type {
    if (self = [super init] ) {
        self.type = type;
        self.spacing = 0;
    }
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    //Center text
    CGRect newFrame = [self titleLabel].frame;
    CGFloat w = newFrame.size.width + self.imageView.width;
    switch (self.type) {
        case ButtonImageLeft:
            
            self.imageView.centerY = self.height / 2.f;
            self.imageView.left = (self.width - (newFrame.size.width + self.imageView.width)) / 2.f ;
            self.titleLabel.centerY = self.height / 2.f;
            self.titleLabel.left = self.imageView.right + self.spacing;
            self.titleLabel.textAlignment = NSTextAlignmentLeft;
            
            break;
            
        case ButtonImageRight:
            
            self.imageView.centerY = self.height / 2.f;
            self.imageView.right = (self.width - w)/2.f + w;
            self.titleLabel.centerY = self.height / 2.f;
            self.titleLabel.right = self.imageView.left - self.spacing;
            self.titleLabel.textAlignment = NSTextAlignmentRight;
            
            break;
            
        case ButtonImageTop:
            
            self.imageView.centerX = self.width / 2.f;
            self.imageView.bottom = self.height / 2.f + 5;
            newFrame.origin.x = 0;
            newFrame.origin.y = self.imageView.bottom +  self.spacing;
            newFrame.size.width = self.frame.size.width;
            self.titleLabel.frame = newFrame;
            self.titleLabel.textAlignment = NSTextAlignmentCenter;
            
            break;
            
        case ButtonImageBottom:
            
            self.imageView.centerX = self.width / 2.f;
            self.imageView.top = self.height / 2.f;
            newFrame.origin.x = 0;
            newFrame.origin.y = self.imageView.top - newFrame.size.height - self.spacing;
            newFrame.size.width = self.frame.size.width;
            self.titleLabel.frame = newFrame;
            self.titleLabel.textAlignment = NSTextAlignmentCenter;
            
            break;
        default:
            break;
    }
    
}

-(CGPoint)buttonRightPoint {
    [self layoutSubviews];
    CGFloat pointX = 0;
    CGFloat pointY = 0;
    switch (self.type) {
        case ButtonImageLeft:
        case ButtonImageBottom:
            pointX = self.titleLabel.right;
            pointY = self.titleLabel.top;
            break;
            
        case ButtonImageRight:
        case ButtonImageTop:
            pointX = self.imageView.right;
            pointY = self.imageView.top;
            
            break;
            
        default:
            break;
    }
    CGPoint point = {pointX,pointY};
    return point;
}

@end
