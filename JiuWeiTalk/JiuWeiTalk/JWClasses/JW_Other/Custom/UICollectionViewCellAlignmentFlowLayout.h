//
//  UICollectionViewCellAlignmentFlowLayout.h
//  NineTails
//
//  Created by 程睿 on 2019/6/10.
//  Copyright © 2019 dayou. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSUInteger,UICollectionViewCellAlignmentType) {
    UICollectionViewCellAlignmentTypeDefault = 0,//默认两边对齐
    UICollectionViewCellAlignmentTypeLeft,//左对齐
    UICollectionViewCellAlignmentTypeRight//右对齐
};

NS_ASSUME_NONNULL_BEGIN

@interface UICollectionViewCellAlignmentFlowLayout : UICollectionViewFlowLayout

@property(nonatomic ,assign ,readwrite) UICollectionViewCellAlignmentType cellAlignmentType;
@property(nonatomic ,assign ,readwrite) CGFloat cellAlignmentSpace;

@end

NS_ASSUME_NONNULL_END
