//
//  CR_VerticalCycleScrollView.h
//  NineTails
//
//  Created by 程睿 on 2019/8/19.
//  Copyright © 2019 dayou. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CR_VerticalCycleScrollView : UITableView

@property (nonatomic, copy) void (^rowClickedBlock) (NSInteger index, id rowData);

+ (instancetype)cycleScrollViewWithFrame:(CGRect)frame
                              dataSource:(NSArray *)dataSource
                            timeInterval:(NSTimeInterval)timeInterval
                                 nibName:(NSString *)nibName;

- (void)stop;
- (void)restart;
@end

NS_ASSUME_NONNULL_END
