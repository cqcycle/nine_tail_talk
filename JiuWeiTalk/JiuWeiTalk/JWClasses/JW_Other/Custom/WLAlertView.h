//
//  WLAlertView.h
//
//  Created by zp on 16/8/5.
//  Copyright © 2016年 .All rights reserved.
//  自定义的alertView 

#import <UIKit/UIKit.h>
@class WLAlertView;
//动画style （可扩展）
typedef NS_ENUM(NSInteger , WLShowAnimationStyle) {
    WLAnimationDefault    = 0, //默认动画

};


@protocol WLAlertViewDelegate <NSObject>

- (void)alertView:(WLAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

@end


@interface WLAlertView : UIView

@property (weak, nonatomic) id<WLAlertViewDelegate> delegate;

@property (assign, nonatomic) BOOL shouldHideTouchingBackground;

/**
 *  @author zhaopu, 16-08-05 11:08:41
 *
 *  默认样式（类似系统alert） 初始化
 *
 *  @param title             主标题
 *  @param message           内容 (如果只显示一个的话 请用message)
 *  @param delegate          代理
 *  @param buttonTitles      button数组
 *
 *  @return 1
 *
 *  @since 2.8.3.3
 */
- (instancetype) initWithTitle:(NSString *)title
                          message:(NSString *)message
                         delegate:(id<WLAlertViewDelegate>)delegate
                     buttonTitles:(NSString *)buttonTitles, ... NS_REQUIRES_NIL_TERMINATION;

/**
 *  @author zhaopu, 16-08-05 11:08:26
 *
 *  自定义样式（自定义View）
 *
 *  @param customView        自定义UIView
 *  @param isShow  是否展示控件按钮 yes 展示  no 不展示
 *  @param delegate          代理
 *  @param buttonTitles      button数组
 *
 *  @return 1
 *
 *  @since 2.8.3.3
 */
- (instancetype) initWithCustomView:(UIView *)customView
               isShowControlsButton:(BOOL)isShow
                              delegate:(id<WLAlertViewDelegate>)delegate
                          buttonTitles:(NSString *)buttonTitles, ... NS_REQUIRES_NIL_TERMINATION;


/**
 * 显示
 */
- (void)show:(WLShowAnimationStyle)animationStyle;
/**
 * 隐藏
 */
- (void)hide;


/**
 *  @author zhaopu, 16-08-05 14:08:58
 *
 *  设置标题的颜色 字体大小
 *
 *  @param color 颜色 default is black
 *  @param size  字体大小 default is 14
 *
 *  @since 2.8.3.3
 */
- (void)setTitleColor:(UIColor *)color fontSize:(CGFloat)size;

/**
 *  @author zhaopu, 16-08-05 14:08:58
 *
 *  设置内容的颜色 字体大小
 *
 *  @param color 颜色 default is black
 *  @param size  字体大小 default is 12
 *
 *  @since 2.8.3.3
 */
- (void)setMessageColor:(UIColor *)color fontSize:(CGFloat)size;

/**
 *  @author zhaopu, 16-08-05 14:08:03
 *
 *  按钮的颜色字体
 *
 *  @param color 颜色 default is black
 *  @param size  字体 default is 16
 *  @param index button Index
 *
 *  @since 2.8.3.3
 */
- (void)setButtonTitleColor:(UIColor *)color fontSize:(CGFloat)size atIndex:(NSInteger)index;



@end



