//
//  WKWebView+DY_ImagePreview.m
//  NineTails
//
//  Created by 程睿 on 2019/5/8.
//  Copyright © 2019 dayou. All rights reserved.
//

#import "WKWebView+DY_ImagePreview.h"
#import <objc/runtime.h>

static char imgUrlArrayKey;

@implementation WKWebView (DY_ImagePreview)


- (void)setImgUrlArray:(NSArray *)imgUrlArray {
    objc_setAssociatedObject(self, &imgUrlArrayKey, imgUrlArray, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSArray *)getImgUrlArray {
    return objc_getAssociatedObject(self, &imgUrlArrayKey);
}

- (NSArray *)getImageUrlByJS:(WKWebView *)webView {
    //js方法遍历图片添加点击事件返回图片个数 这个看你咋改
    //document.getElementById(\"content\").getElementsByTagName(\"img\");\
    //document.getElementsByTagName(\"img\");\
    
    static  NSString * const jsGetImages =
    @"function getImages(){\
    var objs = document.getElementsByTagName(\"img\");\
    var imgUrlStr='';\
    for(var i=0;i<objs.length;i++){\
    if(i==0){\
    imgUrlStr=objs[i].src;\
    }else{\
    imgUrlStr+='#'+objs[i].src;\
    }\
    objs[i].onclick=function(){\
    document.location=\"myweb:imageClick:\"+this.src;\
    };\
    };\
    return imgUrlStr;\
    };";
    
    //用js获取全部图片
    [webView evaluateJavaScript:jsGetImages completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        NSLog(@"%@",result);
    }];
    
    NSString *js2 = @"getImages()";
    __block NSArray *array = [NSArray array];
    [webView evaluateJavaScript:js2 completionHandler:^(id Result, NSError * error) {
        NSString *resurlt = [NSString stringWithFormat:@"%@",Result];
        if([resurlt hasPrefix:@"#"]){
            resurlt = [resurlt substringFromIndex:1];
        }
        array = [resurlt componentsSeparatedByString:@"#"];
        [webView setImgUrlArray:array];
    }];
    
    return array;
}

@end
