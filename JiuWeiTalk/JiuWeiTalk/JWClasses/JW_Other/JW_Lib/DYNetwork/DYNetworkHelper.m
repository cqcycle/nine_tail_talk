//
//  DYNetworkHelper.m
//  NineTails
//
//  Created by zp on 2017/3/25.
//  Copyright © 2017年 zp. All rights reserved.
//

#import "DYNetworkHelper.h"
#import "DYUUID.h"
#import <AFNetworking.h>
#import <AFNetworkActivityIndicatorManager.h>

#ifdef DEBUG
#define DYLog(...) printf("[%s] %s [第%d行]: %s\n", __TIME__ ,__PRETTY_FUNCTION__ ,__LINE__, [[NSString stringWithFormat:__VA_ARGS__] UTF8String])
#else
#define DYLog(...)
#endif

#define NSStringFormat(format,...) [NSString stringWithFormat:format,##__VA_ARGS__]

@implementation DYNetworkHelper

static BOOL _isOpenLog;   // 是否已开启日志打印
static NSMutableArray *_allSessionTask;
static AFHTTPSessionManager *_sessionManager;

#pragma mark - 开始监听网络
+ (void)networkStatusWithBlock:(DYNetworkStatus)networkStatus {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            switch (status) {
                case AFNetworkReachabilityStatusUnknown:
                    networkStatus ? networkStatus(DYNetworkStatusUnknown) : nil;
                    DYLog(@"未知网络");
                    break;
                case AFNetworkReachabilityStatusNotReachable:
                    networkStatus ? networkStatus(DYNetworkStatusNotReachable) : nil;
                    DYLog(@"无网络");
                    break;
                case AFNetworkReachabilityStatusReachableViaWWAN:
                    networkStatus ? networkStatus(DYNetworkStatusReachableViaWWAN) : nil;
                    DYLog(@"手机自带网络");
                    break;
                case AFNetworkReachabilityStatusReachableViaWiFi:
                    networkStatus ? networkStatus(DYNetworkStatusReachableViaWiFi) : nil;
                    DYLog(@"WIFI");
                    break;
            }
        }];
    });
}

+ (BOOL)isNetwork {
    return [AFNetworkReachabilityManager sharedManager].reachable;
}

+ (BOOL)isWWANNetwork {
    return [AFNetworkReachabilityManager sharedManager].reachableViaWWAN;
}

+ (BOOL)isWiFiNetwork {
    return [AFNetworkReachabilityManager sharedManager].reachableViaWiFi;
}

+ (void)openLog {
    _isOpenLog = YES;
}

+ (void)closeLog {
    _isOpenLog = NO;
}

+ (void)cancelAllRequest {
    // 锁操作
    @synchronized(self) {
        [[self allSessionTask] enumerateObjectsUsingBlock:^(NSURLSessionTask  *_Nonnull task, NSUInteger idx, BOOL * _Nonnull stop) {
            [task cancel];
        }];
        [[self allSessionTask] removeAllObjects];
    }
}

+ (void)cancelRequestWithURL:(NSString *)URL {
    if (!URL) { return; }
    @synchronized (self) {
        [[self allSessionTask] enumerateObjectsUsingBlock:^(NSURLSessionTask  *_Nonnull task, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([task.currentRequest.URL.absoluteString hasPrefix:URL]) {
                [task cancel];
                [[self allSessionTask] removeObject:task];
                *stop = YES;
            }
        }];
    }
}
#pragma mark - GET请求无缓存

+ (NSURLSessionTask *)GET:(NSString *)URL
               parameters:(NSDictionary *)parameters
                  success:(DYHttpRequestSuccess)success
                  failure:(DYHttpRequestFailed)failure {
    return [self GET:URL parameters:parameters responseCache:nil success:success failure:failure];
}


#pragma mark - POST请求无缓存

+ (NSURLSessionTask *)POST:(NSString *)URL
                parameters:(NSDictionary *)parameters
                   success:(DYHttpRequestSuccess)success
                   failure:(DYHttpRequestFailed)failure {
    return [self POST:URL parameters:parameters responseCache:nil success:success failure:failure];
}

#pragma mark - POST请求无缓存-可关闭错误信息

+ (NSURLSessionTask *)POST:(NSString *)URL
                parameters:(NSDictionary *)parameters
            showFailureMsg:(BOOL)showFailureMsg
                   success:(DYHttpRequestSuccess)success
                   failure:(DYHttpRequestFailed)failure {
    return [self POST:URL parameters:parameters responseCache:nil showFailureMsg:showFailureMsg success:success failure:failure];
}


#pragma mark - GET请求自动缓存

+ (NSURLSessionTask *)GET:(NSString *)URL
               parameters:(NSDictionary *)parameters
            responseCache:(DYHttpRequestCache)responseCache
                  success:(DYHttpRequestSuccess)success
                  failure:(DYHttpRequestFailed)failure {
    //读取缓存
    responseCache ? responseCache([DYNetworkCache httpCacheForURL:URL parameters:parameters]) : nil;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setDictionary:parameters];
    params[@"appSecret"] = APPSECRET;
    params[@"timestamp"] = [[NSNumber numberWithLongLong:[[NSDate date] timeIntervalSince1970]*1000] stringValue];
    params[@"platform"] = PLATFORM;
    params[@"appName"] = APPNAME;
    params[@"innerVersion"] = INNER_VER;
    params[@"outerVersion"] = APP_VERSION;
    params[@"systemVersion"] = SYS_VER;
    params[@"device"] = [DYUUID getUUID];
    params[@"channel"] = CHANNEL;
    params[@"userNum"] = [JW_UserModel sharedInstance].userId;
    params[@"wifi"] = kIsWiFiNetwork ? @"true":@"false";
    NSDictionary *wifi = [DYCommonTool currentWifiSSID];
    if (wifi) {
        params[@"wifiSsid"] = wifi[@"SSID"];
        params[@"wifiBssid"] = wifi[@"BSSID"];
    }
    //排序
    NSArray *keyArray = [params allKeys];
    NSArray *sortArray = [keyArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    
    NSMutableArray *valueArray = [NSMutableArray array];
    for (NSString *sortString in sortArray) {
        [valueArray addObject:[params objectForKey:sortString]];
    }
    
    params[@"sign"] = [[valueArray componentsJoinedByString:@""] md5String];
    [params removeObjectForKey:@"appSecret"];
    DYLog(@"%@",_isOpenLog ? NSStringFormat(@"\nURL = %@\nparams = %@",URL,[self jsonToString:params]) : @"DYNetworkHelper已关闭日志打印");
    
    NSURLSessionTask *sessionTask = [_sessionManager GET:URL parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        DYLog(@"%@",_isOpenLog ? NSStringFormat(@"\nURL = %@\nresponseObject = %@",URL,[self jsonToString:responseObject]) : @"DYNetworkHelper已关闭日志打印");
        
        [[self allSessionTask] removeObject:task];
        NSNumber *flag = [responseObject objectForKey:@"flag"];
        if ([[NSString stringWithFormat:@"%@",flag] isEqualToString:@"1"]) {
            if ([responseObject containsObjectForKey:@"advertisement"]) {
                success ? success(responseObject) : nil;
            }else {
                success ? success([responseObject objectForKey:@"data"]) : nil;
            }
            //对数据进行异步缓存
            responseCache ? [DYNetworkCache setHttpCache:[responseObject objectForKey:@"data"] URL:URL parameters:parameters] : nil;
        }else {
            failure ? failure(nil) : nil;
            if ([responseObject containsObjectForKey:@"code"]) {
                [DYHUDView hiddenHud];
            }else{
                if ([responseObject objectForKey:@"msg"] != nil) {
                    [DYHUDView showErrorHUD:[responseObject objectForKey:@"msg"]];
                }
            }
            
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        DYLog(@"%@",_isOpenLog ? NSStringFormat(@"\nURL = %@\nerror = %@",URL,error) : @"DYNetworkHelper已关闭日志打印");
        
        [[self allSessionTask] removeObject:task];
        failure ? failure(error) : nil;
    }];
    
    // 添加最新的sessionTask到数组
    sessionTask ? [[self allSessionTask] addObject:sessionTask] : nil ;
    return sessionTask;
}

#pragma mark - POST请求自动缓存-提示错误信息
+ (NSURLSessionTask *)POST:(NSString *)URL
                parameters:(NSDictionary *)parameters
             responseCache:(DYHttpRequestCache)responseCache
                   success:(DYHttpRequestSuccess)success
                   failure:(DYHttpRequestFailed)failure{
    
    return [self POST:URL parameters:parameters responseCache:responseCache showFailureMsg:YES success:success failure:failure];
}
#pragma mark - POST请求自动缓存-可关闭错误信息

+ (NSURLSessionTask *)POST:(NSString *)URL
                parameters:(NSDictionary *)parameters
             responseCache:(DYHttpRequestCache)responseCache
            showFailureMsg:(BOOL)showFailureMsg
                   success:(DYHttpRequestSuccess)success
                   failure:(DYHttpRequestFailed)failure {
    //读取缓存
    responseCache ? responseCache([DYNetworkCache httpCacheForURL:URL parameters:parameters]) : nil;

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setDictionary:parameters];
    params[@"appSecret"] = APPSECRET;
    params[@"timestamp"] = [[NSNumber numberWithLongLong:[[NSDate date] timeIntervalSince1970]*1000] stringValue];
    params[@"appPlatform"] = PLATFORM;
    params[@"appName"] = APPNAME;
    params[@"innerVersion"] = INNER_VER;
    params[@"outerVersion"] = APP_VERSION;
    params[@"systemVersion"] = SYS_VER;
    params[@"deviceNum"] = [DYUUID getUUID];
    params[@"appChannel"] = CHANNEL;
    params[@"userNum"] = [JW_UserModel sharedInstance].userId.length?[JW_UserModel sharedInstance].userId:@"0";
    params[@"wifi"] = kIsWiFiNetwork ? @"true":@"false";
    NSDictionary *wifi = [DYCommonTool currentWifiSSID];
    if (wifi) {
        params[@"wifiSsid"] = wifi[@"SSID"];
        params[@"wifiBssid"] = wifi[@"BSSID"];
    }
    //排序
    NSArray *keyArray = [params allKeys];
    NSArray *sortArray = [keyArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    
    NSMutableArray *valueArray = [NSMutableArray array];
    for (NSString *sortString in sortArray) {
        [valueArray addObject:[params objectForKey:sortString]];
    }
    
    params[@"sign"] = [[valueArray componentsJoinedByString:@""] md5String];
    [params removeObjectForKey:@"appSecret"];
    if (![URL containsString:@"app-point-model"]) {
        DYLog(@"%@",_isOpenLog ? NSStringFormat(@"\nURL = %@\nparams = %@",URL,[self jsonToString:params]) : @"DYNetworkHelper已关闭日志打印");
    }

    NSURLSessionTask *sessionTask = [_sessionManager POST:URL parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (![URL containsString:@"app-point-model"]) {
            DYLog(@"%@",_isOpenLog ? NSStringFormat(@"\nURL = %@\nresponseObject = %@",URL,[self jsonToString:responseObject]) : @"DYNetworkHelper已关闭日志打印");
        }
        [[self allSessionTask] removeObject:task];
        NSNumber *flag = [responseObject objectForKey:@"flag"];
        if ([[NSString stringWithFormat:@"%@",flag] isEqualToString:@"1"]) {
            if ([responseObject containsObjectForKey:@"advertisement"]) {
                success ? success(responseObject) : nil;
            }else {
                success ? success([responseObject objectForKey:@"data"]) : nil;
            }
            //对数据进行异步缓存
            responseCache ? [DYNetworkCache setHttpCache:[responseObject objectForKey:@"data"] URL:URL parameters:parameters] : nil;
        }else {
            failure ? failure(nil) : nil;
            if ([responseObject containsObjectForKey:@"code"]) {
                [DYHUDView hiddenHud];
            }else{
                if (showFailureMsg && [responseObject objectForKey:@"msg"] != nil) {
                    [DYHUDView showErrorHUD:[responseObject objectForKey:@"msg"]];
                }
            }
            
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        DYLog(@"%@",_isOpenLog ? NSStringFormat(@"\nURL = %@\nerror = %@",URL,error) : @"DYNetworkHelper已关闭日志打印");
        
        [[self allSessionTask] removeObject:task];
        failure ? failure(error) : nil;
        if (showFailureMsg) {
            [DYHUDView showErrorHUD:kRequestFailureMsg];
        }
    }];
    
    // 添加最新的sessionTask到数组
    sessionTask ? [[self allSessionTask] addObject:sessionTask] : nil ;
    return sessionTask;
}
#pragma mark - 上传文件

+ (NSURLSessionTask *)uploadFileWithURL:(NSString *)URL
                             parameters:(NSDictionary *)parameters
                                   name:(NSString *)name
                               filePath:(NSString *)filePath
                               progress:(DYHttpProgress)progress
                                success:(DYHttpRequestSuccess)success
                                failure:(DYHttpRequestFailed)failure {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setDictionary:parameters];
    params[@"appSecret"] = APPSECRET;
    params[@"timestamp"] = [[NSNumber numberWithLongLong:[[NSDate date] timeIntervalSince1970]*1000] stringValue];
    params[@"platform"] = PLATFORM;
    params[@"appName"] = APPNAME;
    params[@"innerVersion"] = INNER_VER;
    params[@"outerVersion"] = APP_VERSION;
    params[@"systemVersion"] = SYS_VER;
    params[@"device"] = [DYUUID getUUID];
    params[@"channel"] = CHANNEL;
    params[@"userNum"] = [JW_UserModel sharedInstance].userId;
    params[@"wifi"] = kIsWiFiNetwork ? @"true":@"false";
    NSDictionary *wifi = [DYCommonTool currentWifiSSID];
    if (wifi) {
        params[@"wifiSsid"] = wifi[@"SSID"];
        params[@"wifiBssid"] = wifi[@"BSSID"];
    }
    //排序
    NSArray *keyArray = [params allKeys];
    NSArray *sortArray = [keyArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    
    NSMutableArray *valueArray = [NSMutableArray array];
    for (NSString *sortString in sortArray) {
        [valueArray addObject:[params objectForKey:sortString]];
    }
    
    params[@"sign"] = [[valueArray componentsJoinedByString:@""] md5String];
    [params removeObjectForKey:@"appSecret"];
    
    NSURLSessionTask *sessionTask = [_sessionManager POST:URL parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSError *error = nil;
        NSURL *fileUrl = [NSURL fileURLWithPath:filePath isDirectory:NO];
        [formData appendPartWithFileURL:fileUrl name:name error:&error];
        

        (failure && error) ? failure(error) : nil;
        
        DYLog(@"%@",_isOpenLog ? NSStringFormat(@"\nURL = %@\nparams = %@\nformData = %@:%@",URL,[self jsonToString:params],name, formData) : @"DYNetworkHelper已关闭日志打印");
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        //上传进度
        dispatch_sync(dispatch_get_main_queue(), ^{
            progress ? progress(uploadProgress) : nil;
        });
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        DYLog(@"%@",_isOpenLog ? NSStringFormat(@"\nURL = %@\nresponseObject = %@",URL,[self jsonToString:responseObject]) : @"DYNetworkHelper已关闭日志打印");
        
        [[self allSessionTask] removeObject:task];
        NSNumber *flag = [responseObject objectForKey:@"flag"];
        if ([[NSString stringWithFormat:@"%@",flag] isEqualToString:@"1"]) {
            success ? success([responseObject objectForKey:@"data"]) : nil;
        }else {
            if ([responseObject objectForKey:@"msg"] != nil) {
                [DYHUDView showErrorHUD:[responseObject objectForKey:@"msg"]];
            }
            failure ? failure(nil) : nil;
            [DYHUDView hiddenHud];
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        DYLog(@"%@",_isOpenLog ? NSStringFormat(@"\nURL = %@\nerror = %@",URL,error) : @"DYNetworkHelper已关闭日志打印");
        
        [[self allSessionTask] removeObject:task];
        failure ? failure(error) : nil;
        [DYHUDView showErrorHUD:kRequestFailureMsg];
    }];
    
    // 添加sessionTask到数组
    sessionTask ? [[self allSessionTask] addObject:sessionTask] : nil ;
    
    return sessionTask;
}

#pragma mark - 上传多张图片

+ (NSURLSessionTask *)uploadImagesWithURL:(NSString *)URL
                               parameters:(NSDictionary *)parameters
                                     name:(NSString *)name
                                   images:(NSArray<UIImage *> *)images
                                fileNames:(NSArray<NSString *> *)fileNames
                               imageScale:(CGFloat)imageScale
                                imageType:(NSString *)imageType
                                 progress:(DYHttpProgress)progress
                                  success:(DYHttpRequestSuccess)success
                                  failure:(DYHttpRequestFailed)failure {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setDictionary:parameters];
    params[@"appSecret"] = APPSECRET;
    params[@"timestamp"] = [[NSNumber numberWithLongLong:[[NSDate date] timeIntervalSince1970]*1000] stringValue];
    params[@"platform"] = PLATFORM;
    params[@"appName"] = APPNAME;
    params[@"innerVersion"] = INNER_VER;
    params[@"outerVersion"] = APP_VERSION;
    params[@"systemVersion"] = SYS_VER;
    params[@"device"] = [DYUUID getUUID];
    params[@"channel"] = CHANNEL;
    params[@"userNum"] = [JW_UserModel sharedInstance].userId.length ? [JW_UserModel sharedInstance].userId : @"0";
    params[@"wifi"] = kIsWiFiNetwork ? @"true":@"false";
    NSDictionary *wifi = [DYCommonTool currentWifiSSID];
    if (wifi) {
        params[@"wifiSsid"] = wifi[@"SSID"];
        params[@"wifiBssid"] = wifi[@"BSSID"];
    }
    //排序
    NSArray *keyArray = [params allKeys];
    NSArray *sortArray = [keyArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    
    NSMutableArray *valueArray = [NSMutableArray array];
    for (NSString *sortString in sortArray) {
        [valueArray addObject:[params objectForKey:sortString]];
    }
    
    params[@"sign"] = [[valueArray componentsJoinedByString:@""] md5String];
    [params removeObjectForKey:@"appSecret"];
    
    NSURLSessionTask *sessionTask = [_sessionManager POST:URL parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        for (NSUInteger i = 0; i < images.count; i++) {
            // 图片经过等比压缩后得到的二进制文件
            NSData *imageData = UIImageJPEGRepresentation(images[i], imageScale ?: 1.f);
            // 默认图片的文件名, 若fileNames为nil就使用
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.dateFormat = @"yyyyMMddHHmmss";
            NSString *str = [formatter stringFromDate:[NSDate date]];
            NSString *imageFileName = NSStringFormat(@"%@%ld.%@",str,(unsigned long)i,imageType?:@"jpg");
            
            [formData appendPartWithFileData:imageData
                                        name:name
                                    fileName:fileNames ? NSStringFormat(@"%@.%@",fileNames[i],imageType?:@"jpg") : imageFileName
                                    mimeType:NSStringFormat(@"image/%@",imageType ?: @"jpg")];
        }
        DYLog(@"%@",_isOpenLog ? NSStringFormat(@"\nURL = %@\nparams = %@\nformData = %@:%@",URL,[self jsonToString:params],name, formData) : @"DYNetworkHelper已关闭日志打印");
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        //上传进度
        dispatch_sync(dispatch_get_main_queue(), ^{
            progress ? progress(uploadProgress) : nil;
        });
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        DYLog(@"%@",_isOpenLog ? NSStringFormat(@"\nURL = %@\nresponseObject = %@",URL,[self jsonToString:responseObject]) : @"DYNetworkHelper已关闭日志打印");
        
        [[self allSessionTask] removeObject:task];
        NSNumber *flag = [responseObject objectForKey:@"flag"];
        if ([[NSString stringWithFormat:@"%@",flag] isEqualToString:@"1"]) {
            success ? success([responseObject objectForKey:@"data"]) : nil;
        }else {
            if ([responseObject objectForKey:@"msg"] != nil) {
                [DYHUDView showErrorHUD:[responseObject objectForKey:@"msg"]];
            }
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        DYLog(@"%@",_isOpenLog ? NSStringFormat(@"\nURL = %@\nerror = %@",URL,error) : @"DYNetworkHelper已关闭日志打印");
        
        [[self allSessionTask] removeObject:task];
        failure ? failure(error) : nil;
        [DYHUDView showErrorHUD:kRequestFailureMsg];
    }];
    
    // 添加sessionTask到数组
    sessionTask ? [[self allSessionTask] addObject:sessionTask] : nil ;
    
    return sessionTask;
}

#pragma mark - 下载文件
+ (NSURLSessionTask *)downloadWithURL:(NSString *)URL
                              fileDir:(NSString *)fileDir
                             progress:(DYHttpProgress)progress
                              success:(void(^)(NSString *))success
                              failure:(DYHttpRequestFailed)failure {
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URL]];
    NSURLSessionDownloadTask *downloadTask = [_sessionManager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        //下载进度
        dispatch_sync(dispatch_get_main_queue(), ^{
            progress ? progress(downloadProgress) : nil;
        });
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        //拼接缓存目录
        NSString *downloadDir = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:fileDir ? fileDir : @"Download"];
        //打开文件管理器
        NSFileManager *fileManager = [NSFileManager defaultManager];
        //创建Download目录
        [fileManager createDirectoryAtPath:downloadDir withIntermediateDirectories:YES attributes:nil error:nil];
        //拼接文件路径
        NSString *filePath = [downloadDir stringByAppendingPathComponent:response.suggestedFilename];
        
        //返回文件位置的URL路径
        return [NSURL fileURLWithPath:filePath];
        
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        
        [[self allSessionTask] removeObject:downloadTask];
        if(failure && error) {failure(error) ; return ;};
        success ? success(filePath.absoluteString /** NSURL->NSString*/) : nil;
        
    }];
    //开始下载
    [downloadTask resume];
    // 添加sessionTask到数组
    downloadTask ? [[self allSessionTask] addObject:downloadTask] : nil ;
    return downloadTask;
}

/**
 *  json转字符串
 */
+ (NSString *)jsonToString:(id)data {
    if(!data) { return nil; }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

/**
 存储着所有的请求task数组
 */
+ (NSMutableArray *)allSessionTask {
    if (!_allSessionTask) {
        _allSessionTask = [NSMutableArray array];
    }
    return _allSessionTask;
}
#pragma mark - 初始化AFHTTPSessionManager相关属性
/**
 开始监测网络状态
 */
+ (void)load {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}
/**
 *  所有的HTTP请求共享一个AFHTTPSessionManager
 *  原理参考地址:http://www.jianshu.com/p/5969bbb4af9f
 */
+ (void)initialize {
    _sessionManager = [AFHTTPSessionManager manager];
    // 设置请求的超时时间
    _sessionManager.requestSerializer.timeoutInterval = 30.f;
    // 设置服务器返回结果的类型:JSON (AFJSONResponseSerializer,AFHTTPResponseSerializer)
    _sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
    _sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"aDYlication/json", @"text/html", @"text/json", @"text/plain", @"application/json",@"text/javascript", @"text/xml", @"image/*", nil];
    // 打开状态栏的等待菊花
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
}

#pragma mark - 重置AFHTTPSessionManager相关属性
+ (void)setRequestSerializer:(DYRequestSerializer)requestSerializer {
    _sessionManager.requestSerializer = requestSerializer==DYRequestSerializerHTTP ? [AFHTTPRequestSerializer serializer] : [AFJSONRequestSerializer serializer];
}

+ (void)setResponseSerializer:(DYResponseSerializer)responseSerializer {
    _sessionManager.responseSerializer = responseSerializer==DYResponseSerializerHTTP ? [AFHTTPResponseSerializer serializer] : [AFJSONResponseSerializer serializer];
}

+ (void)setRequestTimeoutInterval:(NSTimeInterval)time {
    _sessionManager.requestSerializer.timeoutInterval = time;
}

+ (void)setValue:(NSString *)value forHTTPHeaderField:(NSString *)field {
    [_sessionManager.requestSerializer setValue:value forHTTPHeaderField:field];
}

+ (void)openNetworkActivityIndicator:(BOOL)open {
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:open];
}

+ (void)setSecurityPolicyWithCerPath:(NSString *)cerPath validatesDomainName:(BOOL)validatesDomainName {
    NSData *cerData = [NSData dataWithContentsOfFile:cerPath];
    // 使用证书验证模式
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    // 如果需要验证自建证书(无效证书)，需要设置为YES
    securityPolicy.allowInvalidCertificates = YES;
    // 是否需要验证域名，默认为YES;
    securityPolicy.validatesDomainName = validatesDomainName;
    securityPolicy.pinnedCertificates = [[NSSet alloc] initWithObjects:cerData, nil];
    
    [_sessionManager setSecurityPolicy:securityPolicy];
}

@end


#pragma mark - NSDictionary,NSArray的分类
/*
 ************************************************************************************
 *新建NSDictionary与NSArray的分类, 控制台打印json数据中的中文
 ************************************************************************************
 */

#ifdef DEBUG
@implementation NSArray (DY)

- (NSString *)descriptionWithLocale:(id)locale {
    NSMutableString *strM = [NSMutableString stringWithString:@"(\n"];
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [strM appendFormat:@"\t%@,\n", obj];
    }];
    [strM appendString:@")"];
    
    return strM;
}

@end

@implementation NSDictionary (DY)

- (NSString *)descriptionWithLocale:(id)locale {
    NSMutableString *strM = [NSMutableString stringWithString:@"{\n"];
    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [strM appendFormat:@"\t%@ = %@;\n", key, obj];
    }];
    
    [strM appendString:@"}\n"];
    
    return strM;
}
@end
#endif

