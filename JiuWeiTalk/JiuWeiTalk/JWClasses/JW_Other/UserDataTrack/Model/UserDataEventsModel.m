//
//  UserDataEventsModel.m
//  JiaBan
//
//  Created by 程睿 on 2017/11/20.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import "UserDataEventsModel.h"
#import "UserEvent.h"
@implementation UserDataEventsModel
- (instancetype)init{
    if (self = [super init]) {
        _event_value = @"";
        _event_remark = @"";
        _msg = @"";
        if ([UserEvent sharedUserEvent].page) {
            _previous_page = [UserEvent sharedUserEvent].page;
            _previous_uuid = [UserEvent sharedUserEvent].uuid;
            _previous_event = [UserEvent sharedUserEvent].event;
            _start_time = [UserEvent sharedUserEvent].time;
        }else if ([UserEvent getLastEventFromCache]){
            NSDictionary *lastEvent = [UserEvent getLastEventFromCache];
            _previous_page = lastEvent[@"page"]?:@"";
            _previous_uuid = lastEvent[@"uuid"]?:@"";
            _previous_event = lastEvent[@"event"]?:@"";
            _start_time = lastEvent[@"time"]?:@"";
            [UserEvent sharedUserEvent].page = lastEvent[@"page"]?:@"";
            [UserEvent sharedUserEvent].uuid = lastEvent[@"uuid"]?:@"";
            [UserEvent sharedUserEvent].event = lastEvent[@"event"]?:@"";
            [UserEvent sharedUserEvent].time = lastEvent[@"time"]?:@"";
        }
    }
    return self;
}

- (instancetype)initWithEvent:(NSString *)event page:(NSString *)page time:(NSString *)time{
    if (self = [self init]){
        _current_page = page;
        _current_event = event;
        _end_time = time;
    }
    return self;
}
@end
