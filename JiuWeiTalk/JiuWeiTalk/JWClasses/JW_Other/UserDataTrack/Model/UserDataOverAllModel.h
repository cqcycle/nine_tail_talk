//
//  UserDataOverAllModel.h
//  JiaBan
//
//  Created by 程睿 on 2017/11/20.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDataContentModel.h"
@interface UserDataOverAllModel : NSObject
@property (nonatomic, copy) UserDataContentModel *content;
@property (nonatomic, copy) NSString *seq_id;
@property (nonatomic, copy) NSString *service;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *uuid;

- (instancetype)initWithContent:(UserDataContentModel *)content;
@end
