//
//  UserDataDeviceModel.m
//  JiaBan
//
//  Created by 程睿 on 2017/11/20.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import "UserDataDeviceModel.h"
#import "DYUUID.h"

@implementation UserDataDeviceModel
- (instancetype)init{
    if (self = [super init]) {
        _device_key = [DYUUID getUUID];
        _dgf = @"";
        _manufacturer = @"apple";
        _model = DEVICE_NAME;
        _os = @"iOS";
        _os_version = SYS_VER;
        _screen_width = [NSString stringWithFormat:@"%f", KJWSCREEN_WIDTH];
        _screen_height = [NSString stringWithFormat:@"%f", KJWSCREEN_HEIGHT];
    }
    return self;
}
@end
