//
//  UserDataUserModel.h
//  JiaBan
//
//  Created by 程睿 on 2017/11/20.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDataUserModel : NSObject
//@property (nonatomic, copy) NSString *id_card;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *original_channel;
@property (nonatomic, copy) NSString *user_id;
//@property (nonatomic, copy) NSString *user_name;
@end
