//
//  UserDataContentModel.h
//  JiaBan
//
//  Created by 程睿 on 2017/11/20.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDataAppModel.h"
#import "UserDataUserModel.h"
#import "UserDataEventsModel.h"
#import "UserDataDeviceModel.h"
#import "UserDataNetworkModel.h"

@interface UserDataContentModel : NSObject
@property (nonatomic, copy) UserDataAppModel *app;
@property (nonatomic, copy) UserDataDeviceModel *device;
@property (nonatomic, copy) UserDataNetworkModel *network;
@property (nonatomic, copy) UserDataUserModel *user;
@property (nonatomic, copy) UserDataEventsModel *events;

- (instancetype)initWithEvents:(UserDataEventsModel *)events;
@end
