//
//  UserDataNetworkModel.h
//  JiaBan
//
//  Created by 程睿 on 2017/11/20.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDataNetworkModel : NSObject
@property (nonatomic, copy) NSString *carrier;
//@property (nonatomic, copy) NSString *carrier_rat;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *city_code;
@property (nonatomic, copy) NSString *ip;
@property (nonatomic, copy) NSString *lat;
@property (nonatomic, copy) NSString *lng;
@property (nonatomic, copy) NSString *network_type;
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *province_code;
//@property (nonatomic, copy) NSString *wifi;
//@property (nonatomic, copy) NSString *wifi_bssid;
//@property (nonatomic, copy) NSString *wifi_ssid;
@end
