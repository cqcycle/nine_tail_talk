//
//  UserDataAppModel.h
//  JiaBan
//
//  Created by 程睿 on 2017/11/20.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDataAppModel : NSObject
@property (nonatomic, copy) NSString *app_id;
@property (nonatomic, copy) NSString *big_app_id;
@property (nonatomic, copy) NSString *channel;
@property (nonatomic, copy) NSString *gap;
@property (nonatomic, copy) NSString *plat;
@property (nonatomic, copy) NSString *user_agent;
@property (nonatomic, copy) NSString *version;
@end
