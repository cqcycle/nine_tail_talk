//
//  UserDataAppModel.m
//  JiaBan
//
//  Created by 程睿 on 2017/11/20.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import "UserDataAppModel.h"

@implementation UserDataAppModel

- (instancetype)init{
    if (self = [super init]) {
        _app_id = APPNAME;
        _big_app_id = [APP_VERSION substringToIndex:1];
        _channel = APP_CURNAME;
        _gap = @"1";
        _plat = PLATFORM;
        _user_agent = [NSString stringWithFormat:@"%@-iOS%@", DEVICE_NAME, SYS_VER];
        _version = APP_VERSION;
    }
    return self;
}
@end
