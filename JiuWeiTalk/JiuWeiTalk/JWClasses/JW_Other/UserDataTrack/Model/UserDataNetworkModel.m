//
//  UserDataNetworkModel.m
//  JiaBan
//
//  Created by 程睿 on 2017/11/20.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import "UserDataNetworkModel.h"
#import "UserLocation.h"

@implementation UserDataNetworkModel
- (instancetype)init{
    if (self = [super init]) {
        _carrier = [DYCommonTool getCarrier];
//        _carrier_rat = @"";
        _city = [UserLocation sharedUserLocation].city;
        _city_code = [UserLocation sharedUserLocation].cityCode;
        _lat = [UserLocation sharedUserLocation].latitude;
        _lng = [UserLocation sharedUserLocation].longitude;
        _province = [UserLocation sharedUserLocation].province;
        _province_code = @"";
//        _wifi = kIsWiFiNetwork ? @"true":@"false";
        _ip = [DYCommonTool getIpAddress];
        _network_type = kIsWiFiNetwork ? @"2":@"1";
//        NSDictionary *wifi = [DYCommonTool currentWifiSSID];
//        if (wifi) {
//            _wifi_ssid = wifi[@"SSID"];
//            _wifi_bssid = wifi[@"BSSID"];
//        }
        
    }
    return self;
}
@end
