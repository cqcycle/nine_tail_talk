//
//  UserDataDeviceModel.h
//  JiaBan
//
//  Created by 程睿 on 2017/11/20.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDataDeviceModel : NSObject
@property (nonatomic, copy) NSString *device_key;
@property (nonatomic, copy) NSString *dgf; 
@property (nonatomic, copy) NSString *manufacturer;
@property (nonatomic, copy) NSString *model;
@property (nonatomic, copy) NSString *os;
@property (nonatomic, copy) NSString *os_version;
@property (nonatomic, copy) NSString *screen_height;
@property (nonatomic, copy) NSString *screen_width;
@end
