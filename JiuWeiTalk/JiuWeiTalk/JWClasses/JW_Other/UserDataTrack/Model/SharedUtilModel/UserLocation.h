//
//  UserLocation.h
//  JiaBan
//
//  Created by 程睿 on 2017/11/21.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserLocation : NSObject
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *cityCode;
@property (nonatomic, copy) NSString *province;
//@property (nonatomic, copy) NSString *provinceCode;

single_interface(UserLocation)
@end
