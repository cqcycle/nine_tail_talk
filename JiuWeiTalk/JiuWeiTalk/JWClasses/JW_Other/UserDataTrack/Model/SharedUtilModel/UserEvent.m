//
//  UserEvent.m
//  JiaBan
//
//  Created by 程睿 on 2017/11/21.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import "UserEvent.h"

@implementation UserEvent
single_implementation(UserEvent)

+ (void)saveLastEventInCache:(id)event{
    YYCache *cache = [YYCache cacheWithName:Cache];
    [cache setObject:event forKey:Cache_LastEvent];
}

+ (NSDictionary *)getLastEventFromCache{
    YYCache *cache = [YYCache cacheWithName:Cache];
    if (![cache containsObjectForKey:Cache_LastEvent]) {
        return nil;
    }
    NSDictionary *event = (NSDictionary *)[cache objectForKey:Cache_LastEvent];
    return event;
}
@end
