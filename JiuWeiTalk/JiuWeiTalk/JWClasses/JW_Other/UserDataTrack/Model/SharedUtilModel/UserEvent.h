//
//  UserEvent.h
//  JiaBan
//
//  Created by 程睿 on 2017/11/21.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserEvent : NSObject
@property (nonatomic, copy) NSString *event;
@property (nonatomic, copy) NSString *page;
@property (nonatomic, copy) NSString *uuid;
@property (nonatomic, copy) NSString *time;

single_interface(UserEvent)

+ (void)saveLastEventInCache:(id)event;
+ (NSDictionary *)getLastEventFromCache;
@end
