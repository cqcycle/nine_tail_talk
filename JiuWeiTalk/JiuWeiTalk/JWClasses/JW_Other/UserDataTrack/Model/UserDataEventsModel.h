//
//  UserDataEventsModel.h
//  JiaBan
//
//  Created by 程睿 on 2017/11/20.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDataEventsModel : NSObject
@property (nonatomic, copy) NSString *current_event;
@property (nonatomic, copy) NSString *current_page;
@property (nonatomic, copy) NSString *end_time;
@property (nonatomic, copy) NSString *event_remark;
@property (nonatomic, copy) NSString *event_value;
@property (nonatomic, copy) NSString *msg;
@property (nonatomic, copy) NSString *previous_event;
@property (nonatomic, copy) NSString *previous_page;
@property (nonatomic, copy) NSString *previous_uuid;
@property (nonatomic, copy) NSString *start_time;

- (instancetype)initWithEvent:(NSString *)event page:(NSString *)page time:(NSString *)time;
@end
