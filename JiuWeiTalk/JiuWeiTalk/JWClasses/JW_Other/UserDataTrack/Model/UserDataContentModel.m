//
//  UserDataContentModel.m
//  JiaBan
//
//  Created by 程睿 on 2017/11/20.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import "UserDataContentModel.h"
#import "UserDataDeviceModel.h"
#import "UserDataUserModel.h"
#import "UserDataAppModel.h"
#import "UserDataNetworkModel.h"

@implementation UserDataContentModel
- (instancetype)initWithEvents:(UserDataEventsModel *)events{
    if (self = [super init]) {
        _app = [[UserDataAppModel alloc]init];
        _device = [[UserDataDeviceModel alloc]init];
        _events = events;
        _network = [[UserDataNetworkModel alloc]init];
        _user = [[UserDataUserModel alloc]init];
    }
    return self;
}
@end
