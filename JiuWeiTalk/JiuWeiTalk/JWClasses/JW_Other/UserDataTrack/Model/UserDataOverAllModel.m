//
//  UserDataOverAllModel.m
//  JiaBan
//
//  Created by 程睿 on 2017/11/20.
//  Copyright © 2017年 Dayou. All rights reserved.
//

#import "UserDataOverAllModel.h"
#import "DYUUID.h"
#import "UserEvent.h"

@implementation UserDataOverAllModel
- (instancetype)initWithContent:(UserDataContentModel *)content{
    if (self = [super init]) {
        _content = content;
//        _seq_id = @"";
//        _service = @"";
        _time = [[NSNumber numberWithLongLong:[[NSDate date] timeIntervalSince1970]] stringValue];
//        _token = @"";
        [UserEvent sharedUserEvent].uuid = [DYUUID getRandomUUID];
//        _uuid = [UserEvent sharedUserEvent].uuid;
    }
    return self;
}
@end
