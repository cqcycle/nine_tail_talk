//
//  AppDelegate.m
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import "AppDelegate.h"
#import "JW_WelcomeViewController.h"
///友盟统计
#import <UMMobClick/MobClick.h>
///友盟分享
#import <UMSocialCore/UMSocialCore.h>
///极光推送
#import <JPUSHService.h>
#import "NT_MessageManager.h"
///iOS10注册APNs所需头文件
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif
#import <CoreTelephony/CTCellularData.h>
#import <AFNetworkReachabilityManager.h>
#import <AVFoundation/AVFoundation.h>
#import <Bagel.h>
@interface AppDelegate ()<JPUSHRegisterDelegate>

@end

@implementation AppDelegate

single_implementation(AppDelegate)


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //创建window
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    [UIApplication sharedApplication].keyWindow.backgroundColor = [UIColor whiteColor];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [self jw_initMyKeyBoard];
//
//    NSLog(@"%@---%zd---%@",[NSObject jw_getBaseUrl],[NSObject jw_getEnvironmentType],KJWJoinString(@"addparama"));
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
        
        //1.获取网络权限 根绝权限进行人机交互
        if (iOS(10)) {
            [self networkStatus:application didFinishLaunchingWithOptions:launchOptions];
        }else {
            //2.2已经开启网络权限 监听网络状态
            [self addReachabilityManager:application didFinishLaunchingWithOptions:launchOptions];
        }
        
        self.launchOption = launchOptions;
        // 载入缓存数据
        YYCache *cache = [YYCache cacheWithName:Cache];
        if ([cache containsObjectForKey:Cache_JW_UserModel]) {
            JW_UserModel *model = (JW_UserModel *)[cache objectForKey:Cache_JW_UserModel];
            [JW_UserModel setupSharedInstance:model];
        }
        
        /*********************极光*************************
         *                                               *
         *                                               *
         *************************************************/
        //Required
        //notice: 3.0.0及以后版本注册可以这样写，也可以继续用之前的注册方式
        JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
        entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
            // 可以添加自定义categories
            // NSSet<UNNotificationCategory *> *categories for iOS10 or later
            // NSSet<UIUserNotificationCategory *> *categories for iOS8 and iOS9
        }
        [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
        
        // Optional
        // 获取IDFA
        // 如需使用IDFA功能请添加此代码并在初始化方法的advertisingIdentifier参数中填写对应值
        //    NSString *advertisingId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
        
        // Required
        // init Push
        // notice: 2.1.5版本的SDK新增的注册方法，改成可上报IDFA，如果没有使用IDFA直接传nil
        // 如需继续使用pushConfig.plist文件声明appKey等配置内容，请依旧使用[JPUSHService setupWithOption:launchOptions]方式初始化。
        
        BOOL isProduction = 1;
        //0 (默认值)表示采用的是开发证书，1 表示采用生产证书发布应用。
    #ifdef DEBUG
        [Bagel start];
        isProduction = 0;
    #else
        isProduction = 1;
    #endif
        
        [JPUSHService setupWithOption:launchOptions appKey:kJPUSHAppKey
                              channel:@"App Store"
                     apsForProduction:isProduction
                advertisingIdentifier:nil];
    ///******************************************************************************///
    
    
    ///网络请求日志
    [DYNetworkHelper openLog];
    
    self.welcomeVC = [JW_WelcomeViewController new];
    [self.window setRootViewController:[[BaseNavController alloc]initWithRootViewController:self.welcomeVC]];
    [self.window makeKeyAndVisible];
    
    return YES;
}


/*
 CTCellularData在iOS9之前是私有类，权限设置是iOS10开始的，所以App Store审核没有问题
 获取网络权限状态
 */
- (void)networkStatus:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //2.根据权限执行相应的交互
    CTCellularData *cellularData = [[CTCellularData alloc] init];
    
    /*
     此函数会在网络权限改变时再次调用
     */
    cellularData.cellularDataRestrictionDidUpdateNotifier = ^(CTCellularDataRestrictedState state) {
        switch (state) {
            case kCTCellularDataRestricted:
                
                NSLog(@"Restricted");
                //2.1权限关闭的情况下 再次请求网络数据会弹出设置网络提示
                [self addReachabilityManager:application didFinishLaunchingWithOptions:launchOptions];
                break;
            case kCTCellularDataNotRestricted:
                
                NSLog(@"NotRestricted");
                //2.2已经开启网络权限 监听网络状态
                [self addReachabilityManager:application didFinishLaunchingWithOptions:launchOptions];
                
                break;
            case kCTCellularDataRestrictedStateUnknown:
                
                NSLog(@"Unknown");
                //2.3未知情况 （还没有遇到推测是有网络但是连接不正常的情况下）
                
                break;
                
            default:
                break;
        }
    };
}

/**
 实时检查当前网络状态
 */
- (void)addReachabilityManager:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    AFNetworkReachabilityManager *afNetworkReachabilityManager = [AFNetworkReachabilityManager sharedManager];
    
    //这个可以放在需要侦听的页面
    //    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(afNetworkStatusChanged:) name:AFNetworkingReachabilityDidChangeNotification object:nil];
    [afNetworkReachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:{
                NSLog(@"网络不通：%@",@(status) );
                [self getInfo_application:application didFinishLaunchingWithOptions:launchOptions];
                break;
            }
            case AFNetworkReachabilityStatusReachableViaWiFi:{
                NSLog(@"网络通过WIFI连接：%@",@(status));
//                if (self.mallConfigModel == nil) {
                    [self getInfo_application:application didFinishLaunchingWithOptions:launchOptions];
//                }
                break;
            }
            case AFNetworkReachabilityStatusReachableViaWWAN:{
                NSLog(@"网络通过4G连接：%@",@(status) );
//                if (self.mallConfigModel == nil) {
                    [self getInfo_application:application didFinishLaunchingWithOptions:launchOptions];
//                }
                break;
            }
            default:
                break;
        }
    }];
    
    [afNetworkReachabilityManager startMonitoring];  //开启网络监视器；
}

- (void)getInfo_application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    /*********************友盟*************************
     *                                               *
     *                                               *
     *************************************************/
    UMConfigInstance.appKey = kUmengAppKey;
    UMConfigInstance.channelId = @"App Store";
    [MobClick startWithConfigure:UMConfigInstance];//配置以上参数后调用此方法初始化SDK！
    [MobClick setLogEnabled:YES];
    
    
    /*********************友盟分享**********************
     *                                               *
     *                                               *
     *************************************************/
    /* 打开调试日志 */
    [[UMSocialManager defaultManager] openLog:NO];
    
    /* 设置友盟appkey */
    [[UMSocialManager defaultManager] setUmSocialAppkey:kUmengAppKey];
    
    [self configUSharePlatforms];
    
    //微信支付
//    [WXApi registerApp:kWechatAppkey];
    
    
#if TARGET_IPHONE_SIMULATOR
#else
    // face++SDK初始化
//    [FaceIDTool initFaceSDKLicense];
#endif
    //-----------------------初始化第三方sdk结束--------------------------
    
    [self notiBackgroundWithType:1];
    
}

- (void)notiBackgroundWithType:(NSInteger)type
{
    //1打开，2退到后台，3关闭app，4变活跃
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"type"] = @(type);
    params[@"model"] = DEVICE_NAME;
    [PC_LoginRequest getAppAppUsageModelAddParams:params success:^(id responseData) {
        
    } failure:^(NSError *error) {
    }];
}

- (void)configUSharePlatforms
{
    /*
     设置微信的appKey和appSecret
     [微信平台从U-Share 4/5升级说明]http://dev.umeng.com/social/ios/%E8%BF%9B%E9%98%B6%E6%96%87%E6%A1%A3#1_1
     */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:kWechatAppkey appSecret:kWechatAppSecret redirectURL:nil];
    /*
     * 移除相应平台的分享，如微信收藏
     */
    //[[UMSocialManager defaultManager] removePlatformProviderWithPlatformTypes:@[@(UMSocialPlatformType_WechatFavorite)]];
    
    /* 设置分享到QQ互联的appID
     * U-Share SDK为了兼容大部分平台命名，统一用appKey和appSecret进行参数设置，而QQ平台仅需将appID作为U-Share的appKey参数传进即可。
     100424468.no permission of union id
     [QQ/QZone平台集成说明]http://dev.umeng.com/social/ios/%E8%BF%9B%E9%98%B6%E6%96%87%E6%A1%A3#1_3
     */
//    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:kQQAppkey appSecret:nil redirectURL:nil];
    
    /*
     设置新浪的appKey和appSecret
     [新浪微博集成说明]http://dev.umeng.com/social/ios/%E8%BF%9B%E9%98%B6%E6%96%87%E6%A1%A3#1_2
     */
//    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_Sina appKey:kSinaAppkey  appSecret:kSinaAppSecret redirectURL:kSinaRedirectURL];
    
}

// 支持所有iOS系统
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options
{
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
    BOOL result = [[UMSocialManager defaultManager]  handleOpenURL:url options:options];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    
//    [WXApi handleOpenURL:url delegate:self];
    
//    __weak typeof(self) weakSelf = self;
    if ([url.host isEqualToString:@"safepay"]) {
        //跳转支付宝钱包进行支付，处理支付结果
//        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
//            NSLog(@"result = %@",resultDic);
//            NSString * orderState=resultDic[@"resultStatus"];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"payStatusPost" object:@{@"orderState":orderState , @"payWay":@"1"}];
//        }];
    }
    
    return result;
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    [DYCommonTool appUserTrackDataPage:@"000" event:@"000000"];
}

//用户从app返回到后台桌面
- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [self notiBackgroundWithType:2];
    [JW_UserModel saveDataToCache];
    [[NT_MessageManager sharedInstance] disconnect];
//    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^(){
//
//           //程序在10分钟内未被系统关闭或者强制关闭，则程序会调用此代码块，可以在这里做一些保存或者清理工作
//    }];
}

//用户从后台回到app
- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
//    [[MoxieSDK shared] applicationWillEnterForeground:application];
    [self notiBackgroundWithType:4];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    application.applicationIconBadgeNumber = 0;
    [JPUSHService resetBadge];
    if ([JW_UserModel sharedInstance].userId.length) {
        [[NT_MessageManager sharedInstance] start];
    }
}

///关闭app，即杀掉App进程
- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [JW_UserModel sharedInstance].redPoint = NO;
    [JW_UserModel saveDataToCache];
    [self notiBackgroundWithType:3];
    [[NT_MessageManager sharedInstance] disconnect];
}

//注册APNs成功并上报DeviceToken
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    /// Required - 注册 DeviceToken
    [JPUSHService registerDeviceToken:deviceToken];
}

#pragma mark - JPUSHRegisterDelegate
//添加处理APNs通知回调方法
// iOS 10 以后，应用在前台
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler  API_AVAILABLE(ios(10.0)){
    // Required
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }completionHandler(UNNotificationPresentationOptionSound | UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以选择设置
//    [[NSNotificationCenter defaultCenter] postNotificationName:kNewMsgReceivedNotification object:nil]; // 通知显示红点
    if ([userInfo containsObjectForKey:@"wallet"]){ // 钱包
        
    }
}

- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler
{
    // Required
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    
    completionHandler();  // 系统要求执行这个方法
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        NT_MessageViewController *vc = [[NT_MessageViewController alloc] init];
//        [[DYCommonTool topViewController].navigationController pushViewController:vc animated:YES];
    });
}

// iOS 10 以后，应用在后台
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler API_AVAILABLE(ios(10.0)){
    // Required
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    
    completionHandler();  // 系统要求执行这个方法
    //TODO
//    if(!self.launchOption){ //已经在App内
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            if ([userInfo containsObjectForKey:@"wallet"]){ // 钱包
            
//            }else{
//                NT_MessageViewController *vc = [[NT_MessageViewController alloc] init];
//                [[DYCommonTool topViewController].navigationController pushViewController:vc animated:YES];
//                self.launchOption = nil;
//            }
        });
//    }
    
}

// iOS 10 以下
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    // Required, iOS 7 Support
    [JPUSHService handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);

    // TODO
//    [[NSNotificationCenter defaultCenter] postNotificationName:kNewMsgReceivedNotification object:nil]; // 通知显示红点
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        NT_MessageViewController *vc = [[NT_MessageViewController alloc] init];
//        [[DYCommonTool topViewController].navigationController pushViewController:vc animated:YES];
//    });
}

- (void)jw_initMyKeyBoard {
    
    IQKeyboardManager *keyboardManager = [IQKeyboardManager sharedManager];
    keyboardManager.enable = YES;
    
    keyboardManager.shouldResignOnTouchOutside = YES;
    
    keyboardManager.shouldToolbarUsesTextFieldTintColor = YES;
    
    keyboardManager.toolbarManageBehaviour = IQAutoToolbarBySubviews;
    
    keyboardManager.placeholderFont = KJWFont(16.f);
    
    keyboardManager.keyboardDistanceFromTextField = 10.f;
    
}
@end
