//
//  AppDelegate.h
//  JiuWeiTalk
//
//  Created by apple on 2019/12/4.
//  Copyright © 2019 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JW_WelcomeViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

single_interface(AppDelegate)

@property (strong, nonatomic) UIWindow * window;
@property (nonatomic, strong)  NSDictionary *launchOption;
@property (nonatomic, strong)  JW_WelcomeViewController *welcomeVC;

@end

